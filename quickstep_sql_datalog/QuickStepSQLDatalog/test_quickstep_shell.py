import os
import json
import subprocess

config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
    config = json.load(config_json_file)

if __name__ == '__main__':
    quickstep_shell_path = config['QuickStep_Shell_Path']
    os.system(quickstep_shell_path + '/quickstep_cli_shell -printing_enabled=false -visualize_plan -visualize_execution_dag')
    
