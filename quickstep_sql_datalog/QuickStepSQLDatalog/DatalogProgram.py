from antlr4 import *
from DatalogLexer import DatalogLexer
from DatalogParser import DatalogParser
import sys
import collections


class DatalogProgram:
    def __init__(self, datalog_program, edb_decl, idb_decl, dependency_graph,scc):
        self.datalog_program = datalog_program
        self.edb_decl = edb_decl
        self.idb_decl = idb_decl
        self.dependency_graph = dependency_graph
        self.scc = scc


def iterate_datalog_edb_idb_decl(relations):
    """
    Iterate each specific component in the edb/idb declaration and print the complete declaration
    """
    for relation in relations:
        relation_str = relation['name'] + '('
        attributes = relation['attributes']
        for attribute in attributes:
            relation_str += attribute.name + ' ' + attribute.type + ', '
        relation_str = relation_str[:len(relation_str)-2]
        relation_str += ')'
        print(relation_str)


def iterate_datalog_rule(datalog_rule):
    """
    Iterate each specific component in a datalog rule (head and body)
    """
    head = datalog_rule['head']
    body = datalog_rule['body']

    head_name = head['name']
    head_arg_list = head['arg_list']

    try:
        body_atom_list = body['atoms']
        body_compare_list = body['compares']
        body_assign_list = body['assigns']
        body_negation_list = body['negations']
    except TypeError:
        # if the rule of the body is empty, then the body will be 'NoneType'
        body_atom_list = []
        body_compare_list = []
        body_assign_list = []
        body_negation_list = []


    head_str = head_name + '('
    for arg in head_arg_list:
        if arg.type != 'aggregation':
            head_str += arg.name + ', '
            #print('arg.name')
            #print(arg.name)
        else:
            head_str += arg.name['agg_op'] + '(' + arg.name['agg_arg'] + ')' + ', '
    head_str = head_str[:len(head_str)-2] + ')'

    body_str = ''
    for atom in body_atom_list:
        body_str += atom['name'] + '('
        arg_list = atom['arg_list']
        for arg in arg_list:
            body_str += arg.name + ', '
        body_str = body_str[:len(body_str)-2] + '), '

    for compare in body_compare_list:
        body_str += compare['lhs'][0] + ' ' + compare['op'] + ' ' + compare['rhs'][0] + ', '

    for negation in body_negation_list:
        body_str += '!' + negation['name'] + '('
        arg_list = negation['arg_list']
        for arg in arg_list:
            body_str += arg.name + ', '
        body_str = body_str[:len(body_str)-2] + '), '

    for assign in body_assign_list:
        body_str += assign['lhs'] + ' ' + ' = ' + assign['rhs']['lhs'] + ' ' +  \
                    assign['rhs']['op'] + ' ' + assign['rhs']['rhs'] + ', '

    body_str = body_str[:len(body_str)-2] + '.'

    #print(head_str + ' :- ' + body_str)
    return head_str + ' :- ' + body_str


def iterate_datalog_program(datalog_program):
    """
    Iterate each rule in the AST generated from the datalog program and print the complete datalog program
    """
    for datalog_rule in datalog_program:
        iterate_datalog_rule(datalog_rule)


def correctness_checking():
    """
    TODO:
    1. range-restricted: every variable in the head of the rule appears in some positive relation occurrence in the body
    """
    print('Check the correctness of the input datalog program')


def construct_dependency_graph(datalog_program):
    rule_number = len(datalog_program)
    dependency_map = collections.OrderedDict({})
    head_rule_map = collections.OrderedDict({})
    # Preprocessing: create a dictionary storing <head_atom, rule_index_list> key-value pair
    for rule_index in range(rule_number):
        cur_rule = datalog_program[rule_index]
        head_atom_name = cur_rule['head']['name']
        if head_atom_name not in head_rule_map:
            head_rule_map[head_atom_name] = set([])
            head_rule_map[head_atom_name].add(rule_index)
        else:
            head_rule_map[head_atom_name].add(rule_index)

    # Construct dependency graph
    for rule_index in range(rule_number):
        dependency_map[rule_index] = set([])
        cur_rule = datalog_program[rule_index]
        try:
            body_atoms = cur_rule['body']['atoms']
        except TypeError:
            # if the rule of the body is empty, then the body will be 'NoneType'
            body_atoms = []
        #body_negations = cur_rule['body']['negations']
        for atom in body_atoms:
            # Check whether the atom is in head of any IDB rules
            if atom['name'] in head_rule_map:
                dependent_rule_list = head_rule_map[atom['name']]
                for dependent_rule_index in dependent_rule_list:
                    dependency_map[rule_index].add(dependent_rule_index)

    for rule_index in dependency_map:
        dependency_str = 'rule_' + str(rule_index) + ': '
        dependent_rule_indices = dependency_map[rule_index]
        for dependent_rule_index in dependent_rule_indices:
            dependency_str += 'rule_' + str(dependent_rule_index) + ', '
        print(dependency_str[:len(dependency_str)-2])

    return dependency_map


def group_rules(dependency_map):
    # compute strongly connected components by Kosaraju's algorithm
    rule_visited_map = [0] * len(dependency_map)
    # map to check whether a rule has already been assigned to one of strongly connected components
    rule_assigned_map = [0] * len(dependency_map)
    dfs_order_list = []
    # strongly connected components
    scc = collections.OrderedDict({})

    # depth-first search to compute the transponse graph order
    def visit(rule):
        if rule_visited_map[rule] == 0:
            rule_visited_map[rule] = 1
            dependent_rules = dependency_map[rule]
            for dependent_rule in dependent_rules:
                visit(dependent_rule)
            dfs_order_list.append(rule)

    for rule in dependency_map:
        visit(rule)

    dfs_order_list.reverse()
    # construct transpose dependency graph
    transpose_dependency_graph = collections.OrderedDict({})

    #print('dependency_map:')
    #print(dependency_map)
    for rule in dependency_map:
        dependent_rules = dependency_map[rule]
        for dependent_rule in dependent_rules:
            if dependent_rule not in transpose_dependency_graph:
                transpose_dependency_graph[dependent_rule] = set([])
            transpose_dependency_graph[dependent_rule].add(rule)

    def assign(rule, root):
        if rule_assigned_map[rule] == 0:
            if root not in scc:
                scc[root] = []
            scc[root].append(rule)
            rule_assigned_map[rule] = 1
            # check if the rule is depended on by any other rules
            # corner case: a rule could be totally independent
            if rule in transpose_dependency_graph:
                reverse_dependent_rules = transpose_dependency_graph[rule]
                for reverse_dependent_rule in reverse_dependent_rules:
                    assign(reverse_dependent_rule, root)

    for rule in dfs_order_list:
        assign(rule, rule)

    # rule evaluation order: the reverse order of sccs
    return scc


def datalog_program(input_file_name):
    """
    try:
        input_file_name = sys.argv[1]
    except:
        raise Exception('No input file is given')
    """

    try:
        input = FileStream(input_file_name)
    except IOError:
        raise Exception('An error occured when tryting to read file \'' + input_file_name + '\'')

    lexer = DatalogLexer(input)
    stream = CommonTokenStream(lexer)
    parser = DatalogParser(stream)
    parser.buildParseTrees = False
    # AST of the given datalog program
    edb_decl_tree = parser.datalog_edb_declare().r
    idb_decl_tree = parser.datalog_idb_declare().r
    rules_tree = parser.datalog_rule_declare().r

    print('EDB_DECL:')
    iterate_datalog_edb_idb_decl(edb_decl_tree)
    print('\n')
    print('IDB_DECL:')
    iterate_datalog_edb_idb_decl(idb_decl_tree)
    print('\n')
    print('RULE_DECL:')
    iterate_datalog_program(rules_tree)
    print('\n\n')
    print('DEPENDENCY_GRAPH: ')
    dependency_graph = construct_dependency_graph(rules_tree)
    print('\n\n')
    print('SCC: ')
    scc = group_rules(dependency_graph)
    # reverse scc ordered dictionary and the new order of sccs would be the scc evaluation order
    scc = collections.OrderedDict(reversed(list(scc.items())))
    print(scc)
    for root in scc:
        print('size: ' + str(len(scc[root])))
    datalog_program_instance = DatalogProgram(rules_tree, edb_decl_tree, idb_decl_tree, dependency_graph, scc)
    return datalog_program_instance

