from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
    config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
AddressOf = Table('AddressOf')
AddressOf.add_attribute('y', 'int')
AddressOf.add_attribute('x', 'int')
quickstep_shell_instance.create_table(AddressOf)
Assign = Table('Assign')
Assign.add_attribute('y', 'int')
Assign.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Assign)
Load = Table('Load')
Load.add_attribute('y', 'int')
Load.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Load)
Store = Table('Store')
Store.add_attribute('y', 'int')
Store.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Store)
# create idb tables
PointsTo = Table('PointsTo')
PointsTo.add_attribute('y', 'int')
PointsTo.add_attribute('x', 'int')
quickstep_shell_instance.create_table(PointsTo)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(AddressOf, './Input/AddressOf.tbl', ',')
quickstep_shell_instance.load_data_from_file(Assign, './Input/Assign.tbl', ',')
quickstep_shell_instance.load_data_from_file(Load, './Input/Load.tbl', ',')
quickstep_shell_instance.load_data_from_file(Store, './Input/Store.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('PointsTo(y, x) :- AddressOf(y, x).')
time_monitor.update()
# PointsTo(y, x) :- AddressOf(y, x).
PointsTo_count=quickstep_shell_instance.count_rows('PointsTo')
lpa_logger.info('Number of tuples in PointsTo: ' + str(PointsTo_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('y', 'int')
tmp_res_table.add_attribute('x', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.y as y, a0.x as x from AddressOf a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['y', 'x'], PointsTo, ['y', 'x'])
quickstep_shell_instance.sql_command('\\analyzecount PointsTo\n')
quickstep_shell_instance.drop_table('tmp_res_table')
PointsTo_count=quickstep_shell_instance.count_rows('PointsTo')
lpa_logger.info('Number of tuples in PointsTo: ' + str(PointsTo_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# recursive rules evaluation
# PointsTo(y, x) :- Assign(y, z), PointsTo(z, x).
# PointsTo(y, w) :- Load(y, x), PointsTo(x, z), PointsTo(z, w).
# PointsTo(z, w) :- Store(y, x), PointsTo(y, z), PointsTo(x, w).
lpa_logger.info('Start creating delta, prev, common-delta tables for semi-naive evaluation')
time_monitor.update()
PointsTo_Delta = Table('PointsTo_Delta0')
PointsTo_Delta.add_attribute('y', 'int')
PointsTo_Delta.add_attribute('x', 'int')
quickstep_shell_instance.create_table(PointsTo_Delta)
PointsTo_Common_Delta = Table('PointsTo_Common_Delta')
PointsTo_Common_Delta.add_attribute('y', 'int')
PointsTo_Common_Delta.add_attribute('x', 'int')
quickstep_shell_instance.create_table(PointsTo_Common_Delta)
PointsToPrev = Table('PointsToPrev')
PointsToPrev.add_attribute('y', 'int')
PointsToPrev.add_attribute('x', 'int')
quickstep_shell_instance.create_table(PointsToPrev)
quickstep_shell_instance.sql_command('\\analyze \n')
quickstep_shell_instance.load_data_from_table(PointsTo, ['y', 'x'], PointsTo_Delta, ['y', 'x'])
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
is_delta_empty = quickstep_shell_instance.is_table_empty('PointsTo_Delta0')
iter_num = 0
while(not is_delta_empty):
    lpa_logger.info('#####Start Iteration ' + str(iter_num + 1) + '#####')
    time_monitor.update()
    cur_iter_start_time = time_monitor.local_start_time
    PointsTo_delta_name = 'PointsTo_Delta' + str(iter_num)

    iter_num += 1

    new_PointsTo_delta_name = 'PointsTo_Delta' + str(iter_num)

    PointsTo_Delta.rename(new_PointsTo_delta_name)

    quickstep_shell_instance.create_table(PointsTo_Delta)

    # PointsTo
    PointsTo_mDelta = Table('PointsTo_mDelta')
    PointsTo_mDelta.add_attribute('y', 'int')
    PointsTo_mDelta.add_attribute('x', 'int')
    quickstep_shell_instance.create_table(PointsTo_mDelta)

    lpa_logger.info('Evaluate PointsTo')
    # PointsTo(y, x) :- Assign(y, z), PointsTo(z, x).
    # PointsTo(y, w) :- Load(y, x), PointsTo(x, z), PointsTo(z, w).
    # PointsTo(z, w) :- Store(y, x), PointsTo(y, z), PointsTo(x, w).
    PointsTo_tmp_mDelta = Table('PointsTo_tmp_mDelta')
    PointsTo_tmp_mDelta.add_attribute('y', 'int')
    PointsTo_tmp_mDelta.add_attribute('x', 'int')
    quickstep_shell_instance.create_table(PointsTo_tmp_mDelta)
    quickstep_shell_instance.sql_command('insert into PointsTo_tmp_mDelta select * from (' + 'select a0.y as y, p1.x as x from Assign a0, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p1 where a0.x = p1.y' + ' union all ' + 'select l0.y as y, p2.x as x from Load l0, ' + 'PointsToPrev' + ' p1, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p2 where p1.x = p2.y AND l0.x = p1.y AND p1.x != -1' + ' union all ' + 'select l0.y as y, p2.x as x from Load l0, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p1, ' + 'PointsToPrev' + ' p2 where p1.x = p2.y AND l0.x = p1.y AND p1.x != -1' + ' union all ' + 'select l0.y as y, p2.x as x from Load l0, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p1, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p2 where p1.x = p2.y AND l0.x = p1.y AND p1.x != -1' + ' union all ' + 'select p1.x as y, p2.x as x from Store s0, ' + 'PointsToPrev' + ' p1, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p2 where s0.x = p2.y AND s0.y = p1.y' + ' union all ' + 'select p1.x as y, p2.x as x from Store s0, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p1, ' + 'PointsToPrev' + ' p2 where s0.x = p2.y AND s0.y = p1.y' + ' union all ' + 'select p1.x as y, p2.x as x from Store s0, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p1, ' + 'PointsTo_Delta' + str(iter_num-1) + ' p2 where s0.x = p2.y AND s0.y = p1.y' + ') t;')
    quickstep_shell_instance.sql_command('\\analyzecount PointsTo_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into PointsTo_mDelta select * from PointsTo_tmp_mDelta group by y, x;')
    quickstep_shell_instance.drop_table('PointsTo_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount PointsTo_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()
    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into PointsTo_Common_Delta select PointsTo_mDelta.y, PointsTo_mDelta.x from PointsTo_mDelta, PointsTo where PointsTo_mDelta.y = PointsTo.y and PointsTo_mDelta.x = PointsTo.x;')
    quickstep_shell_instance.sql_command('\\analyzecount PointsTo_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'PointsTo_Delta' + str(iter_num) + ' select * from PointsTo_mDelta where not exists (select PointsTo_Common_Delta.y, PointsTo_Common_Delta.x from PointsTo_Common_Delta where PointsTo_Common_Delta.y = PointsTo_mDelta.y and PointsTo_Common_Delta.x = PointsTo_mDelta.x);')
    quickstep_shell_instance.sql_command('\\analyze PointsTo_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    # drop mDelta table
    quickstep_shell_instance.drop_table('PointsTo_mDelta')
    quickstep_shell_instance.drop_table('PointsTo_Common_Delta')
    quickstep_shell_instance.create_table(PointsTo_Common_Delta)
    lpa_logger.info('Save the current IDB')
    time_monitor.update()
    # save the current idb
    quickstep_shell_instance.drop_table('PointsToPrev')
    quickstep_shell_instance.create_table(PointsToPrev)
    quickstep_shell_instance.sql_command('insert into PointsToPrev select * from PointsTo;')
    quickstep_shell_instance.sql_command('\\analyzecount PointsToPrev\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update PointsTo
    quickstep_shell_instance.sql_command('insert into PointsTo select * from ' + new_PointsTo_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount PointsTo\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # drop all old intermediate tables 
    quickstep_shell_instance.drop_table(PointsTo_delta_name)

    is_delta_empty = quickstep_shell_instance.is_table_empty(new_PointsTo_delta_name)
    PointsTo_Delta_count=quickstep_shell_instance.count_rows(new_PointsTo_delta_name)
    lpa_logger.info('Number of tuples in PointsTo_Delta: ' + str(PointsTo_Delta_count))
    PointsTo_count=quickstep_shell_instance.count_rows('PointsTo')
    lpa_logger.info('Number of tuples in PointsTo: ' + str(PointsTo_count))
    time_monitor.update()
    cur_iter_end_time = time_monitor.local_start_time
    lpa_logger.info('Iteration Time: ' + str(cur_iter_end_time - cur_iter_start_time) + 's')
    lpa_logger.info('')
    lpa_logger.info('')

# clear all prev tables
quickstep_shell_instance.drop_table('PointsToPrev')

# clear all  deltas
quickstep_shell_instance.drop_table('PointsTo_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('PointsTo_Common_Delta')
lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
