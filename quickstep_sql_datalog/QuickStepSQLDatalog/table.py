"""
Data Structures used for communication between database and logic flow
"""
import collections

class Table():
    
    def __init__(self, table_name):
        self.table_name = table_name
        self.attributes = collections.OrderedDict()
        self.attribute_num = 0
        
    def rename(self, new_table_name):
        self.table_name = new_table_name

    def add_attribute(self, attribute_name, attribute_type):
        if attribute_name in self.attributes:
            raise Exception(attribute_name + 'already exists in table ' + self.table_name)
        else:
            self.attributes[attribute_name] = attribute_type
            self.attribute_num += 1
