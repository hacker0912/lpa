import unittest
from table import Table


class TestTable(unittest.TestCase):
    """
    Tests for table.py
    """
    TEST_TABLE_NAME = 'TestTable'
    TEST_ATTRIBUTE_ONE = 'TestAttributeOne'
    TEST_ATTRIBUTE_ONE_TYPE = 'INTEGER'
    TEST_ATTRIBUTE_TWO = 'TestAttributeTwo'
    TEST_ATTRIBUTE_TWO_TYPE = 'VARCHAR'
    TEST_RENAME = 'TestRename'

    def setUp(self):
        self.table = Table(TestTable.TEST_TABLE_NAME)
        self.table.add_attribute(TestTable.TEST_ATTRIBUTE_ONE, TestTable.TEST_ATTRIBUTE_ONE_TYPE)
        self.table.add_attribute(TestTable.TEST_ATTRIBUTE_TWO, TestTable.TEST_ATTRIBUTE_TWO_TYPE)

    def test_rename(self):
        self.assertEqual(self.table.table_name, TestTable.TEST_TABLE_NAME)
        self.table.rename(TestTable.TEST_RENAME)
        self.assertEqual(self.table.table_name, TestTable.TEST_RENAME)

    def test_attributes(self):
        self.assertEqual(self.table.attributes[TestTable.TEST_ATTRIBUTE_ONE], TestTable.TEST_ATTRIBUTE_ONE_TYPE)
        self.assertEqual(self.table.attributes[TestTable.TEST_ATTRIBUTE_TWO], TestTable.TEST_ATTRIBUTE_TWO_TYPE)
        self.assertEqual(len(self.table.attributes), 2)


if __name__ == '__main__':
    unittest.main()
