from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
	config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
arc = Table('arc')
arc.add_attribute('x', 'int')
arc.add_attribute('y', 'int')
quickstep_shell_instance.create_table(arc)
# create idb tables
sg = Table('sg')
sg.add_attribute('x', 'int')
sg.add_attribute('y', 'int')
quickstep_shell_instance.create_table(sg)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(arc, './Input/arc.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('sg(x, y) :- arc(p, x), arc(p, y), x != y.')
time_monitor.update()
# sg(x, y) :- arc(p, x), arc(p, y), x != y.
sg_count=quickstep_shell_instance.count_rows('sg')
lpa_logger.info('Number of tuples in sg: ' + str(sg_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
tmp_res_table.add_attribute('y', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.y as x, a1.y as y from arc a0, arc a1 where a0.x = a1.x AND a0.x != -1 AND a0.y != a1.y;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x', 'y'], sg, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyzecount sg\n')
quickstep_shell_instance.drop_table('tmp_res_table')
sg_count=quickstep_shell_instance.count_rows('sg')
lpa_logger.info('Number of tuples in sg: ' + str(sg_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# recursive rules evaluation
# sg(x, y) :- arc(a, x), sg(a, b), arc(b, y).
lpa_logger.info('Start creating delta, prev, common-delta tables for semi-naive evaluation')
time_monitor.update()
sg_Delta = Table('sg_Delta0')
sg_Delta.add_attribute('x', 'int')
sg_Delta.add_attribute('y', 'int')
quickstep_shell_instance.create_table(sg_Delta)
sg_Common_Delta = Table('sg_Common_Delta')
sg_Common_Delta.add_attribute('x', 'int')
sg_Common_Delta.add_attribute('y', 'int')
quickstep_shell_instance.create_table(sg_Common_Delta)
quickstep_shell_instance.sql_command('\\analyze \n')
quickstep_shell_instance.load_data_from_table(sg, ['x', 'y'], sg_Delta, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
is_delta_empty = quickstep_shell_instance.is_table_empty('sg_Delta0')
iter_num = 0
while(not is_delta_empty):
	lpa_logger.info('#####Start Iteration ' + str(iter_num + 1) + '#####')
	time_monitor.update()
	cur_iter_start_time = time_monitor.local_start_time
	sg_delta_name = 'sg_Delta' + str(iter_num)

	iter_num += 1

	new_sg_delta_name = 'sg_Delta' + str(iter_num)

	sg_Delta.rename(new_sg_delta_name)

	quickstep_shell_instance.create_table(sg_Delta)

	# sg
	sg_mDelta = Table('sg_mDelta')
	sg_mDelta.add_attribute('x', 'int')
	sg_mDelta.add_attribute('y', 'int')
	quickstep_shell_instance.create_table(sg_mDelta)

	lpa_logger.info('Evaluate sg')
	# sg(x, y) :- arc(a, x), sg(a, b), arc(b, y).
	sg_tmp_mDelta = Table('sg_tmp_mDelta')
	sg_tmp_mDelta.add_attribute('x', 'int')
	sg_tmp_mDelta.add_attribute('y', 'int')
	quickstep_shell_instance.create_table(sg_tmp_mDelta)
	quickstep_shell_instance.sql_command('insert into sg_tmp_mDelta select * from (' + 'select a0.y as x, a2.y as y from arc a0, ' + 'sg_Delta' + str(iter_num-1) + ' s1, arc a2 where a0.x = s1.x AND s1.y = a2.x' + ') t;')
	quickstep_shell_instance.sql_command('\\analyzecount sg_tmp_mDelta\n')
	quickstep_shell_instance.sql_command('insert into sg_mDelta select * from sg_tmp_mDelta group by x, y;')
	quickstep_shell_instance.drop_table('sg_tmp_mDelta')
	quickstep_shell_instance.sql_command('\\analyzecount sg_mDelta\n')
	lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
	lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
	time_monitor.update()
	# compute intersection for anti-join (set-difference)
	quickstep_shell_instance.sql_command('insert into sg_Common_Delta select sg_mDelta.x, sg_mDelta.y from sg_mDelta, sg where sg_mDelta.x = sg.x and sg_mDelta.y = sg.y;')
	quickstep_shell_instance.sql_command('\\analyzecount sg_Common_Delta\n')
	lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
	lpa_logger.info('Compute set-difference')
	time_monitor.update()
	# compute set-difference
	# compute delta
	quickstep_shell_instance.sql_command('insert into ' +  'sg_Delta' + str(iter_num) + ' select * from sg_mDelta where not exists (select sg_Common_Delta.x, sg_Common_Delta.y from sg_Common_Delta where sg_Common_Delta.x = sg_mDelta.x and sg_Common_Delta.y = sg_mDelta.y);')
	quickstep_shell_instance.sql_command('\\analyze sg_Delta' + str(iter_num) + '\n')
	lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
	# drop mDelta table
	quickstep_shell_instance.drop_table('sg_mDelta')
	quickstep_shell_instance.drop_table('sg_Common_Delta')
	quickstep_shell_instance.create_table(sg_Common_Delta)
	lpa_logger.info('Update IDB (union delta)')
	time_monitor.update()
	# update sg
	quickstep_shell_instance.sql_command('insert into sg select * from ' + new_sg_delta_name + ';')
	quickstep_shell_instance.sql_command('\\analyzecount sg\n')
	lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

	# drop all old intermediate tables 
	quickstep_shell_instance.drop_table(sg_delta_name)

	is_delta_empty = quickstep_shell_instance.is_table_empty(new_sg_delta_name)
	sg_Delta_count=quickstep_shell_instance.count_rows(new_sg_delta_name)
	lpa_logger.info('Number of tuples in sg_Delta: ' + str(sg_Delta_count))
	sg_count=quickstep_shell_instance.count_rows('sg')
	lpa_logger.info('Number of tuples in sg: ' + str(sg_count))
	time_monitor.update()
	cur_iter_end_time = time_monitor.local_start_time
	lpa_logger.info('Iteration Time: ' + str(cur_iter_end_time - cur_iter_start_time) + 's')
	lpa_logger.info('')
	lpa_logger.info('')

# clear all prev tables

# clear all  deltas
quickstep_shell_instance.drop_table('sg_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('sg_Common_Delta')
lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
