from table import Table
import quickstep
import json
import sys
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
    config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
Assign = Table('Assign')
Assign.add_attribute('src', 'int')
Assign.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(Assign)
Dereference = Table('Dereference')
Dereference.add_attribute('src', 'int')
Dereference.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(Dereference)
# create idb tables
ValueFlow = Table('ValueFlow')
ValueFlow.add_attribute('src', 'int')
ValueFlow.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueFlow)
ValueAlias = Table('ValueAlias')
ValueAlias.add_attribute('src', 'int')
ValueAlias.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueAlias)
MemoryAlias = Table('MemoryAlias')
MemoryAlias.add_attribute('src', 'int')
MemoryAlias.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(MemoryAlias)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(Assign, './Input/Assign.tbl', ',')
quickstep_shell_instance.load_data_from_file(Dereference, './Input/Dereference.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('ValueFlow(y, x) :- Assign(y, x).')
time_monitor.update()
# ValueFlow(y, x) :- Assign(y, x).
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('src', 'int')
tmp_res_table.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.src as src, a0.dest as dest from Assign a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['src', 'dest'], ValueFlow, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyzecount ValueFlow\n')
quickstep_shell_instance.drop_table('tmp_res_table')
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('MemoryAlias(x, x) :- Assign(y, x).')
time_monitor.update()
# MemoryAlias(x, x) :- Assign(y, x).
MemoryAlias_count=quickstep_shell_instance.count_rows('MemoryAlias')
lpa_logger.info('Number of tuples in MemoryAlias: ' + str(MemoryAlias_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('src', 'int')
tmp_res_table.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.dest as src, a0.dest as dest from Assign a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['src', 'dest'], MemoryAlias, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias\n')
quickstep_shell_instance.drop_table('tmp_res_table')
MemoryAlias_count=quickstep_shell_instance.count_rows('MemoryAlias')
lpa_logger.info('Number of tuples in MemoryAlias: ' + str(MemoryAlias_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('MemoryAlias(x, x) :- Assign(x, y).')
time_monitor.update()
# MemoryAlias(x, x) :- Assign(x, y).
MemoryAlias_count=quickstep_shell_instance.count_rows('MemoryAlias')
lpa_logger.info('Number of tuples in MemoryAlias: ' + str(MemoryAlias_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('src', 'int')
tmp_res_table.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.src as src, a0.src as dest from Assign a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['src', 'dest'], MemoryAlias, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias\n')
quickstep_shell_instance.drop_table('tmp_res_table')
MemoryAlias_count=quickstep_shell_instance.count_rows('MemoryAlias')
lpa_logger.info('Number of tuples in MemoryAlias: ' + str(MemoryAlias_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('ValueFlow(x, x) :- Assign(x, y).')
time_monitor.update()
# ValueFlow(x, x) :- Assign(x, y).
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('src', 'int')
tmp_res_table.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.src as src, a0.src as dest from Assign a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['src', 'dest'], ValueFlow, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyzecount ValueFlow\n')
quickstep_shell_instance.drop_table('tmp_res_table')
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('ValueFlow(x, x) :- Assign(y, x).')
time_monitor.update()
# ValueFlow(x, x) :- Assign(y, x).
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('src', 'int')
tmp_res_table.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.dest as src, a0.dest as dest from Assign a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['src', 'dest'], ValueFlow, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyzecount ValueFlow\n')
quickstep_shell_instance.drop_table('tmp_res_table')
ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# recursive rules evaluation
# ValueFlow(x, y) :- Assign(x, z), MemoryAlias(z, y).
# ValueFlow(x, y) :- ValueFlow(x, z), ValueFlow(z, y).
# ValueAlias(x, y) :- ValueFlow(z, x), ValueFlow(z, y).
# MemoryAlias(x, w) :- Dereference(y, x), ValueAlias(y, z), Dereference(z, w).
# ValueAlias(x, y) :- ValueFlow(z, x), MemoryAlias(z, w), ValueFlow(w, y).
lpa_logger.info('Start creating delta, prev, common-delta tables for semi-naive evaluation')
time_monitor.update()
ValueFlow_Delta = Table('ValueFlow_Delta0')
ValueFlow_Delta.add_attribute('src', 'int')
ValueFlow_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueFlow_Delta)
ValueFlow_Common_Delta = Table('ValueFlow_Common_Delta')
ValueFlow_Common_Delta.add_attribute('src', 'int')
ValueFlow_Common_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueFlow_Common_Delta)
ValueAlias_Delta = Table('ValueAlias_Delta0')
ValueAlias_Delta.add_attribute('src', 'int')
ValueAlias_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueAlias_Delta)
ValueAlias_Common_Delta = Table('ValueAlias_Common_Delta')
ValueAlias_Common_Delta.add_attribute('src', 'int')
ValueAlias_Common_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueAlias_Common_Delta)
MemoryAlias_Delta = Table('MemoryAlias_Delta0')
MemoryAlias_Delta.add_attribute('src', 'int')
MemoryAlias_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(MemoryAlias_Delta)
MemoryAlias_Common_Delta = Table('MemoryAlias_Common_Delta')
MemoryAlias_Common_Delta.add_attribute('src', 'int')
MemoryAlias_Common_Delta.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(MemoryAlias_Common_Delta)
MemoryAliasPrev = Table('MemoryAliasPrev')
MemoryAliasPrev.add_attribute('src', 'int')
MemoryAliasPrev.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(MemoryAliasPrev)
ValueFlowPrev = Table('ValueFlowPrev')
ValueFlowPrev.add_attribute('src', 'int')
ValueFlowPrev.add_attribute('dest', 'int')
quickstep_shell_instance.create_table(ValueFlowPrev)
quickstep_shell_instance.sql_command('\\analyze \n')
quickstep_shell_instance.load_data_from_table(ValueFlow, ['src', 'dest'], ValueFlow_Delta, ['src', 'dest'])
quickstep_shell_instance.load_data_from_table(ValueAlias, ['src', 'dest'], ValueAlias_Delta, ['src', 'dest'])
quickstep_shell_instance.load_data_from_table(MemoryAlias, ['src', 'dest'], MemoryAlias_Delta, ['src', 'dest'])
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
is_delta_empty = quickstep_shell_instance.is_table_empty('ValueFlow_Delta0') and quickstep_shell_instance.is_table_empty('ValueAlias_Delta0') and quickstep_shell_instance.is_table_empty('MemoryAlias_Delta0')
iter_num = 0
while(not is_delta_empty):
    lpa_logger.info('#####Start Iteration ' + str(iter_num + 1) + '#####')
    time_monitor.update()

    cur_iter_start_time = time_monitor.local_start_time
    ValueFlow_delta_name = 'ValueFlow_Delta' + str(iter_num)
    ValueAlias_delta_name = 'ValueAlias_Delta' + str(iter_num)
    MemoryAlias_delta_name = 'MemoryAlias_Delta' + str(iter_num)

    iter_num += 1

    new_ValueFlow_delta_name = 'ValueFlow_Delta' + str(iter_num)
    new_ValueAlias_delta_name = 'ValueAlias_Delta' + str(iter_num)
    new_MemoryAlias_delta_name = 'MemoryAlias_Delta' + str(iter_num)

    ValueFlow_Delta.rename(new_ValueFlow_delta_name)
    ValueAlias_Delta.rename(new_ValueAlias_delta_name)
    MemoryAlias_Delta.rename(new_MemoryAlias_delta_name)

    quickstep_shell_instance.create_table(ValueFlow_Delta)
    quickstep_shell_instance.create_table(ValueAlias_Delta)
    quickstep_shell_instance.create_table(MemoryAlias_Delta)

    # ValueFlow
    ValueFlow_mDelta = Table('ValueFlow_mDelta')
    ValueFlow_mDelta.add_attribute('src', 'int')
    ValueFlow_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(ValueFlow_mDelta)

    lpa_logger.info('Evaluate ValueFlow')
    # ValueFlow(x, y) :- Assign(x, z), MemoryAlias(z, y).
    # ValueFlow(x, y) :- ValueFlow(x, z), ValueFlow(z, y).
    ValueFlow_tmp_mDelta = Table('ValueFlow_tmp_mDelta')
    ValueFlow_tmp_mDelta.add_attribute('src', 'int')
    ValueFlow_tmp_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(ValueFlow_tmp_mDelta)
    quickstep_shell_instance.sql_command('insert into ValueFlow_tmp_mDelta select * from (' + 'select a0.src as src, m1.dest as dest from Assign a0, ' + 'MemoryAlias_Delta' + str(iter_num-1) + ' m1 where a0.dest = m1.src' + ' union all ' + 'select v0.src as src, v1.dest as dest from ' + 'ValueFlowPrev' + ' v0, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v1 where v0.dest = v1.src AND v0.dest != -1' + ' union all ' + 'select v0.src as src, v1.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'ValueFlowPrev' + ' v1 where v0.dest = v1.src AND v0.dest != -1' + ' union all ' + 'select v0.src as src, v1.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v1 where v0.dest = v1.src AND v0.dest != -1' + ') t;')
    quickstep_shell_instance.sql_command('\\analyzecount ValueFlow_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into ValueFlow_mDelta select * from ValueFlow_tmp_mDelta group by src, dest;')
    quickstep_shell_instance.drop_table('ValueFlow_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount ValueFlow_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()
    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into ValueFlow_Common_Delta select ValueFlow_mDelta.src, ValueFlow_mDelta.dest from ValueFlow_mDelta, ValueFlow where ValueFlow_mDelta.src = ValueFlow.src and ValueFlow_mDelta.dest = ValueFlow.dest;')
    quickstep_shell_instance.sql_command('\\analyzecount ValueFlow_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'ValueFlow_Delta' + str(iter_num) + ' select * from ValueFlow_mDelta where not exists (select ValueFlow_Common_Delta.src, ValueFlow_Common_Delta.dest from ValueFlow_Common_Delta where ValueFlow_Common_Delta.src = ValueFlow_mDelta.src and ValueFlow_Common_Delta.dest = ValueFlow_mDelta.dest);')
    quickstep_shell_instance.sql_command('\\analyze ValueFlow_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    # drop mDelta table
    quickstep_shell_instance.drop_table('ValueFlow_mDelta')
    quickstep_shell_instance.drop_table('ValueFlow_Common_Delta')
    quickstep_shell_instance.create_table(ValueFlow_Common_Delta)
    lpa_logger.info('Save the current IDB')
    time_monitor.update()
    # save the current idb
    quickstep_shell_instance.drop_table('ValueFlowPrev')
    quickstep_shell_instance.create_table(ValueFlowPrev)
    quickstep_shell_instance.sql_command('insert into ValueFlowPrev select * from ValueFlow;')
    quickstep_shell_instance.sql_command('\\analyzecount ValueFlowPrev\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update ValueFlow
    quickstep_shell_instance.sql_command('insert into ValueFlow select * from ' + new_ValueFlow_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount ValueFlow\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # ValueAlias
    ValueAlias_mDelta = Table('ValueAlias_mDelta')
    ValueAlias_mDelta.add_attribute('src', 'int')
    ValueAlias_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(ValueAlias_mDelta)

    lpa_logger.info('Evaluate ValueAlias')
    # ValueAlias(x, y) :- ValueFlow(z, x), ValueFlow(z, y).
    # ValueAlias(x, y) :- ValueFlow(z, x), MemoryAlias(z, w), ValueFlow(w, y).
    ValueAlias_tmp_mDelta = Table('ValueAlias_tmp_mDelta')
    ValueAlias_tmp_mDelta.add_attribute('src', 'int')
    ValueAlias_tmp_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(ValueAlias_tmp_mDelta)
    if iter_num == 6:
        quickstep_shell_instance.stop()
        sys.exit()
    quickstep_shell_instance.sql_command('insert into ValueAlias_tmp_mDelta select * from (' + 'select v0.dest as src, v1.dest as dest from ' + 'ValueFlowPrev' + ' v0, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v1 where v0.src = v1.src AND v0.src != -1' + ' union all ' + 'select v0.dest as src, v1.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'ValueFlowPrev' + ' v1 where v0.src = v1.src AND v0.src != -1' + ' union all ' + 'select v0.dest as src, v1.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v1 where v0.src = v1.src AND v0.src != -1' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlowPrev' + ' v0, ' + 'MemoryAliasPrev' + ' m1, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlowPrev' + ' v0, ' + 'MemoryAlias_Delta' + str(iter_num-1) + ' m1, ' + 'ValueFlowPrev' + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlowPrev' + ' v0, ' + 'MemoryAlias_Delta' + str(iter_num-1) + ' m1, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'MemoryAliasPrev' + ' m1, ' + 'ValueFlowPrev' + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'MemoryAliasPrev' + ' m1, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'MemoryAlias_Delta' + str(iter_num-1) + ' m1, ' + 'ValueFlowPrev' + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ' union all ' + 'select v0.dest as src, v2.dest as dest from ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v0, ' + 'MemoryAlias_Delta' + str(iter_num-1) + ' m1, ' + 'ValueFlow_Delta' + str(iter_num-1) + ' v2 where v0.src = m1.src AND m1.dest = v2.src' + ') t;')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    time_monitor.update()
    lpa_logger.info('Deduplicate ValueAlias_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount ValueAlias_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('\\analyzerange ValueAlias_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into ValueAlias_mDelta select * from ValueAlias_tmp_mDelta group by src, dest;')
    quickstep_shell_instance.drop_table('ValueAlias_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount ValueAlias_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()
    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into ValueAlias_Common_Delta select ValueAlias_mDelta.src, ValueAlias_mDelta.dest from ValueAlias_mDelta, ValueAlias where ValueAlias_mDelta.src = ValueAlias.src and ValueAlias_mDelta.dest = ValueAlias.dest;')
    quickstep_shell_instance.sql_command('\\analyzecount ValueAlias_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'ValueAlias_Delta' + str(iter_num) + ' select * from ValueAlias_mDelta where not exists (select ValueAlias_Common_Delta.src, ValueAlias_Common_Delta.dest from ValueAlias_Common_Delta where ValueAlias_Common_Delta.src = ValueAlias_mDelta.src and ValueAlias_Common_Delta.dest = ValueAlias_mDelta.dest);')
    quickstep_shell_instance.sql_command('\\analyze ValueAlias_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    # drop mDelta table
    quickstep_shell_instance.drop_table('ValueAlias_mDelta')
    quickstep_shell_instance.drop_table('ValueAlias_Common_Delta')
    quickstep_shell_instance.create_table(ValueAlias_Common_Delta)
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update ValueAlias
    quickstep_shell_instance.sql_command('insert into ValueAlias select * from ' + new_ValueAlias_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount ValueAlias\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # MemoryAlias
    MemoryAlias_mDelta = Table('MemoryAlias_mDelta')
    MemoryAlias_mDelta.add_attribute('src', 'int')
    MemoryAlias_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(MemoryAlias_mDelta)

    lpa_logger.info('Evaluate MemoryAlias')
    # MemoryAlias(x, w) :- Dereference(y, x), ValueAlias(y, z), Dereference(z, w).
    MemoryAlias_tmp_mDelta = Table('MemoryAlias_tmp_mDelta')
    MemoryAlias_tmp_mDelta.add_attribute('src', 'int')
    MemoryAlias_tmp_mDelta.add_attribute('dest', 'int')
    quickstep_shell_instance.create_table(MemoryAlias_tmp_mDelta)
    quickstep_shell_instance.sql_command('insert into MemoryAlias_tmp_mDelta select * from (' + 'select d0.dest as src, d2.dest as dest from Dereference d0, ' + 'ValueAlias_Delta' + str(iter_num-1) + ' v1, Dereference d2 where v1.dest = d2.src AND d0.src = v1.src' + ') t;')
    quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into MemoryAlias_mDelta select * from MemoryAlias_tmp_mDelta group by src, dest;')
    quickstep_shell_instance.drop_table('MemoryAlias_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()
    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into MemoryAlias_Common_Delta select MemoryAlias_mDelta.src, MemoryAlias_mDelta.dest from MemoryAlias_mDelta, MemoryAlias where MemoryAlias_mDelta.src = MemoryAlias.src and MemoryAlias_mDelta.dest = MemoryAlias.dest;')
    quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'MemoryAlias_Delta' + str(iter_num) + ' select * from MemoryAlias_mDelta where not exists (select MemoryAlias_Common_Delta.src, MemoryAlias_Common_Delta.dest from MemoryAlias_Common_Delta where MemoryAlias_Common_Delta.src = MemoryAlias_mDelta.src and MemoryAlias_Common_Delta.dest = MemoryAlias_mDelta.dest);')
    quickstep_shell_instance.sql_command('\\analyze MemoryAlias_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    # drop mDelta table
    quickstep_shell_instance.drop_table('MemoryAlias_mDelta')
    quickstep_shell_instance.drop_table('MemoryAlias_Common_Delta')
    quickstep_shell_instance.create_table(MemoryAlias_Common_Delta)
    lpa_logger.info('Save the current IDB')
    time_monitor.update()
    # save the current idb
    quickstep_shell_instance.drop_table('MemoryAliasPrev')
    quickstep_shell_instance.create_table(MemoryAliasPrev)
    quickstep_shell_instance.sql_command('insert into MemoryAliasPrev select * from MemoryAlias;')
    quickstep_shell_instance.sql_command('\\analyzecount MemoryAliasPrev\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update MemoryAlias
    quickstep_shell_instance.sql_command('insert into MemoryAlias select * from ' + new_MemoryAlias_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount MemoryAlias\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # drop all old intermediate tables 
    quickstep_shell_instance.drop_table(ValueFlow_delta_name)
    quickstep_shell_instance.drop_table(ValueAlias_delta_name)
    quickstep_shell_instance.drop_table(MemoryAlias_delta_name)

    is_delta_empty = quickstep_shell_instance.is_table_empty(new_ValueFlow_delta_name) and quickstep_shell_instance.is_table_empty(new_ValueAlias_delta_name) and quickstep_shell_instance.is_table_empty(new_MemoryAlias_delta_name)
    ValueFlow_Delta_count=quickstep_shell_instance.count_rows(new_ValueFlow_delta_name)
    lpa_logger.info('Number of tuples in ValueFlow_Delta: ' + str(ValueFlow_Delta_count))
    ValueAlias_Delta_count=quickstep_shell_instance.count_rows(new_ValueAlias_delta_name)
    lpa_logger.info('Number of tuples in ValueAlias_Delta: ' + str(ValueAlias_Delta_count))
    MemoryAlias_Delta_count=quickstep_shell_instance.count_rows(new_MemoryAlias_delta_name)
    lpa_logger.info('Number of tuples in MemoryAlias_Delta: ' + str(MemoryAlias_Delta_count))
    ValueFlow_count=quickstep_shell_instance.count_rows('ValueFlow')
    lpa_logger.info('Number of tuples in ValueFlow: ' + str(ValueFlow_count))
    ValueAlias_count=quickstep_shell_instance.count_rows('ValueAlias')
    lpa_logger.info('Number of tuples in ValueAlias: ' + str(ValueAlias_count))
    MemoryAlias_count=quickstep_shell_instance.count_rows('MemoryAlias')
    lpa_logger.info('Number of tuples in MemoryAlias: ' + str(MemoryAlias_count))
    time_monitor.update()
    cur_iter_end_time = time_monitor.local_start_time
    lpa_logger.info('Iteration Time: ' + str(cur_iter_end_time - cur_iter_start_time) + 's')
    lpa_logger.info('')
    lpa_logger.info('')

# clear all prev tables
quickstep_shell_instance.drop_table('MemoryAliasPrev')
quickstep_shell_instance.drop_table('ValueFlowPrev')

# clear all  deltas
quickstep_shell_instance.drop_table('ValueFlow_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('ValueFlow_Common_Delta')
quickstep_shell_instance.drop_table('ValueAlias_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('ValueAlias_Common_Delta')
quickstep_shell_instance.drop_table('MemoryAlias_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('MemoryAlias_Common_Delta')
lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
