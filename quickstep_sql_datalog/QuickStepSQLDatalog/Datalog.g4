grammar Datalog;

@header{
}

@parser::members {

class AtomArg():
    def __init__(self, arg_name, arg_type):
        self.name = arg_name
        self.type = arg_type
}

datalog_edb_declare returns[r]
    : {edb_list = []}
      TOKEN_EDB TOKEN_COLON
      schema1 = datalog_relation_schema   {edb_list.append($schema1.r)}
      (schema2 = datalog_relation_schema {edb_list.append($schema2.r)})*
      {$r = edb_list}
    ;

datalog_idb_declare returns [r]
    : {idb_list = []}
      TOKEN_IDB TOKEN_COLON
      schema1 = datalog_relation_schema   {idb_list.append($schema1.r)}
      (schema2 = datalog_relation_schema {idb_list.append($schema2.r)})*
      {$r = idb_list}
    ;

datalog_relation_schema returns [r]
    : {schema = {'name': '', 'attributes': []}}
      relation_name = TOKEN_ID                    {schema['name'] = $relation_name.text}
      TOKEN_LEFT_PAREN
      t1 = TOKEN_ID d1 = data_type                {schema['attributes'].append(self.AtomArg($t1.text, $d1.r))}
      (TOKEN_COMMA t2 = TOKEN_ID d2 = data_type   {schema['attributes'].append(self.AtomArg($t2.text, $d2.r))})*
      TOKEN_RIGHT_PAREN
      {$r = schema}
    ;

datalog_rule_declare returns [r]
    : TOKEN_RULE TOKEN_COLON dp = datalog_program {$r = $dp.r} EOF
    ;

datalog_program returns [r]
	: {rule_list = []}
	  r1 = datalog_rule  {rule_list.append($r1.r)} 
	  (r2 = datalog_rule {rule_list.append($r2.r)})*
	  {$r = rule_list}
	;

/** Rule that has no body must be the rule specifying "facts insertion" **/
datalog_rule returns [r]
    : {rule_dic = {}}
	    h = head {rule_dic['head'] = $h.r}
	    TOKEN_BODY_HEAD_SEP {rule_dic['body'] = None}
	    (b = body {rule_dic['body'] = $b.r})?
	    TOKEN_DOT
	  {$r = rule_dic}
    ;

head returns [r]
	: a = atom
	  {$r = $a.r}
	;

body returns [r] 
    : {body_dic = {'atoms':[], 'compares': [], 'assigns':[], 'negations':[]}}
	  ((b1 = atom 			{body_dic['atoms'].append($b1.r)} | 
	   	b2 = compare_expr 	{body_dic['compares'].append($b2.r)} | 
	  	b3 = assign 		{body_dic['assigns'].append($b3.r)} | 
	    b4 = negation 		{body_dic['negations'].append($b4.r)}) 
		TOKEN_COMMA)*
	  ((b5 = atom 			{body_dic['atoms'].append($b5.r)} |
		b6 = compare_expr 	{body_dic['compares'].append($b6.r)} |
		b7 = assign		    {body_dic['assigns'].append($b7.r)} |
		b8 = negation 		{body_dic['negations'].append($b8.r)}))
	  {$r = body_dic}
	;


negation returns [r]
	: TOKEN_NOT a = atom
	  {$r = $a.r}
	;

atom returns [r]
	: {atom_dic = {'name': 'unknown', 'arg_list':[]}}
	  a1 = TOKEN_ID 		  {atom_dic['name'] = $a1.text}  
	  TOKEN_LEFT_PAREN 
	  (a2 = TOKEN_ID 		  {atom_dic['arg_list'].append(self.AtomArg($a2.text, 'variable'))} |
	   a3 = aggregation_expr  {atom_dic['arg_list'].append(self.AtomArg($a3.r, 'aggregation'))} |
	   a4 = TOKEN_ANY 		  {atom_dic['arg_list'].append(self.AtomArg($a4.text, 'any'))} |
	   a5 = constant 		  {atom_dic['arg_list'].append(self.AtomArg($a5.text, 'constant'))})
	  (TOKEN_COMMA 
	   (a6 = TOKEN_ID 		  {atom_dic['arg_list'].append(self.AtomArg($a6.text, 'variable'))} |
		a7 = aggregation_expr {atom_dic['arg_list'].append(self.AtomArg($a7.r, 'aggregation'))}|
		a8 = TOKEN_ANY		  {atom_dic['arg_list'].append(self.AtomArg($a8.text, 'any'))} |
		a9 = constant 		  {atom_dic['arg_list'].append(self.AtomArg($a9.text, 'constant'))}))*
	  TOKEN_RIGHT_PAREN
	  {$r = atom_dic}
	;

assign returns [r] 
	: {assign_dic = {}}
	  a1 = TOKEN_ID 	  {assign_dic['lhs'] = $a1.text} 
	  TOKEN_EQUALS
	  a2 = math_expr 	  {assign_dic['rhs'] = $a2.r}
	  {$r = assign_dic}
	;

math_expr returns [r]
	: {math_dic = {}}
	  m1 = TOKEN_ID		  {math_dic['lhs'] = $m1.text} 
	  m2 = math_op		  {math_dic['op'] = $m2.r}
      m3 = TOKEN_ID 	  {math_dic['rhs'] = $m3.text}
	  {$r = math_dic}
	;

compare_expr returns [r]
	: {compare_dic = {}}
      (c1 = TOKEN_ID      {compare_dic['lhs'] = [$c1.text, 'var']} |
       c2 = TOKEN_INTEGER  {compare_dic['lhs'] = [$c2.text, 'num']})
       op = compare_op	  {compare_dic['op'] = $op.r}
      (c4 = TOKEN_ID      {compare_dic['rhs'] = [$c4.text, 'var']} |
       c5 = TOKEN_INTEGER  {compare_dic['rhs'] = [$c5.text, 'num']})
	  {$r = compare_dic}
	; 

aggregation_expr returns [r]
	: {agg_dic = {}}
	  a1 = aggregation_op {agg_dic['agg_op'] = $a1.r} 
	  TOKEN_LEFT_PAREN 
	  a2 = TOKEN_ID 	  {agg_dic['agg_arg'] = $a2.text}
	  TOKEN_RIGHT_PAREN 
	  {$r = agg_dic}
	;

compare_op returns [r]
	: op1 = TOKEN_NOT_EQUALS          {$r = $op1.text}         
    | op2 = TOKEN_EQUALS			  {$r = $op2.text}
	| op3 = TOKEN_GREATER_THAN		  {$r = $op3.text}
	| op4 = TOKEN_GREATER_EQUAL_THAN  {$r = $op4.text}
    | op5 = TOKEN_LESS_THAN			  {$r = $op5.text}
    | op6 = TOKEN_LESS_EQUAL_THAN	  {$r = $op6.text}
	;

aggregation_op returns [r]
	: op1 = TOKEN_MIN   {$r = $op1.text}
	| op2 = TOKEN_MAX   {$r = $op2.text}
	| op3 = TOKEN_SUM	{$r = $op3.text}
	| op4 = TOKEN_COUNT {$r = $op4.text}
	;

math_op returns [r] 
	: op1 = TOKEN_PLUS  {$r = $op1.text}
	| op2 = TOKEN_MINUS {$r = $op2.text}
	| op3 = TOKEN_MULT  {$r = $op3.text}
	| op4 = TOKEN_DIV	{$r = $op4.text}
	;	

constant returns [r]
	: c1 = TOKEN_INTEGER {$r = $c1.text}
	| c2 = TOKEN_STRING {$r = $c2.text}
	;

data_type returns [r]
    : dt1 = TOKEN_INT {$r = $dt1.text}
    ;

TOKEN_EDB: 'EDB_DECL';
TOKEN_IDB: 'IDB_DECL';
TOKEN_RULE: 'RULE_DECL';
TOKEN_INTEGER: [-+]?[0-9]+;
TOKEN_STRING: '\''([A-Za-z] | [0-9])+'\'';
TOKEN_INT: ('i'|'I')('n'|'N')('t'|'T');
TOKEN_MIN: 'MIN';
TOKEN_MAX: 'MAX';
TOKEN_SUM: 'SUM';
TOKEN_COUNT: 'COUNT';
TOKEN_ID: [A-Za-z]([A-Za-z]|[0-9])*;
TOKEN_BODY_HEAD_SEP: ':-';
TOKEN_ANY: '_';
TOKEN_COMMA: ',';
TOKEN_SEMICOLON: ';';
TOKEN_COLON: ':';
TOKEN_DOT: '.';
TOKEN_PLUS: '+';
TOKEN_MINUS: '-';
TOKEN_MULT: '*';
TOKEN_DIV: '/'; 
TOKEN_NOT: '!';
TOKEN_NOT_EQUALS: '!=';
TOKEN_EQUALS: '=';
TOKEN_GREATER_EQUAL_THAN: '>=';
TOKEN_GREATER_THAN: '>';
TOKEN_LESS_EQUAL_THAN: '<=';
TOKEN_LESS_THAN: '<';
TOKEN_LEFT_PAREN: '(';
TOKEN_RIGHT_PAREN: ')';
TOKEN_WS: [ \t\r\n]+ -> skip;
