from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
	config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
AddressOf = Table('AddressOf')
AddressOf.add_attribute('y', 'int')
AddressOf.add_attribute('x', 'int')
quickstep_shell_instance.create_table(AddressOf)
Assign = Table('Assign')
Assign.add_attribute('y', 'int')
Assign.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Assign)
# create idb tables
Test0 = Table('Test0')
Test0.add_attribute('y', 'int')
Test0.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Test0)
Test1 = Table('Test1')
Test1.add_attribute('y', 'int')
Test1.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Test1)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(AddressOf, './Input/AddressOf.tbl', ',')
quickstep_shell_instance.load_data_from_file(Assign, './Input/Assign.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('Test0(y, w) :- AddressOf(y, x), Assign(x, z), Assign(z, w).')
time_monitor.update()
# Test0(y, w) :- AddressOf(y, x), Assign(x, z), Assign(z, w).
Test0_count=quickstep_shell_instance.count_rows('Test0')
lpa_logger.info('Number of tuples in Test0: ' + str(Test0_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('y', 'int')
tmp_res_table.add_attribute('x', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.y as y, a2.x as x from AddressOf a0, Assign a1, Assign a2 where a0.x = a1.y AND a1.x = a2.y AND a1.x != -1;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['y', 'x'], Test0, ['y', 'x'])
quickstep_shell_instance.sql_command('\\analyzecount Test0\n')
quickstep_shell_instance.drop_table('tmp_res_table')
Test0_count=quickstep_shell_instance.count_rows('Test0')
lpa_logger.info('Number of tuples in Test0: ' + str(Test0_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('Test1(x, MIN(x)) :- AddressOf(x, _).')
time_monitor.update()
# Test1(x, MIN(x)) :- AddressOf(x, _).
Test1_count=quickstep_shell_instance.count_rows('Test1')
lpa_logger.info('Number of tuples in Test1: ' + str(Test1_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('y', 'int')
tmp_res_table.add_attribute('x', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.y as y, MIN(a0.y) as x from AddressOf a0 group by y;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['y', 'x'], Test1, ['y', 'x'])
quickstep_shell_instance.sql_command('\\analyzecount Test1\n')
quickstep_shell_instance.drop_table('tmp_res_table')
Test1_count=quickstep_shell_instance.count_rows('Test1')
lpa_logger.info('Number of tuples in Test1: ' + str(Test1_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
