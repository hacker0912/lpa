from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
	config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
aggtest = Table('aggtest')
aggtest.add_attribute('x', 'int')
aggtest.add_attribute('y', 'int')
quickstep_shell_instance.create_table(aggtest)
# create idb tables
path = Table('path')
path.add_attribute('x', 'int')
path.add_attribute('y', 'int')
quickstep_shell_instance.create_table(path)
remax = Table('remax')
remax.add_attribute('x', 'int')
remax.add_attribute('y', 'int')
quickstep_shell_instance.create_table(remax)
finalmax = Table('finalmax')
finalmax.add_attribute('x', 'int')
finalmax.add_attribute('y', 'int')
quickstep_shell_instance.create_table(finalmax)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(aggtest, './Input/aggtest.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('path(x, y) :- aggtest(x, y).')
time_monitor.update()
# path(x, y) :- aggtest(x, y).
path_count=quickstep_shell_instance.count_rows('path')
lpa_logger.info('Number of tuples in path: ' + str(path_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
tmp_res_table.add_attribute('y', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select a0.x as x, a0.y as y from aggtest a0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x', 'y'], path, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyzecount path\n')
quickstep_shell_instance.drop_table('tmp_res_table')
path_count=quickstep_shell_instance.count_rows('path')
lpa_logger.info('Number of tuples in path: ' + str(path_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('remax(x, MAX(y)) :- path(x, w), aggtest(w, y).')
time_monitor.update()
# remax(x, MAX(y)) :- path(x, w), aggtest(w, y).
remax_count=quickstep_shell_instance.count_rows('remax')
lpa_logger.info('Number of tuples in remax: ' + str(remax_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
tmp_res_table.add_attribute('y', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select p0.x as x, MAX(a1.y) as y from path p0, aggtest a1 where p0.y = a1.x group by x;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x', 'y'], remax, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyzecount remax\n')
quickstep_shell_instance.drop_table('tmp_res_table')
remax_count=quickstep_shell_instance.count_rows('remax')
lpa_logger.info('Number of tuples in remax: ' + str(remax_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# non-recursive rule evaluation
lpa_logger.info('finalmax(x, MAX(y)) :- remax(x, y).')
time_monitor.update()
# finalmax(x, MAX(y)) :- remax(x, y).
finalmax_count=quickstep_shell_instance.count_rows('finalmax')
lpa_logger.info('Number of tuples in finalmax: ' + str(finalmax_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
tmp_res_table.add_attribute('y', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select r0.x as x, MAX(r0.y) as y from remax r0 group by x;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x', 'y'], finalmax, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyzecount finalmax\n')
quickstep_shell_instance.drop_table('tmp_res_table')
finalmax_count=quickstep_shell_instance.count_rows('finalmax')
lpa_logger.info('Number of tuples in finalmax: ' + str(finalmax_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
