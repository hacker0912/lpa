# Generated from Datalog.g4 by ANTLR 4.7
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3!")
        buf.write("\u0123\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2\61\n\2")
        buf.write("\f\2\16\2\64\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\7\3@\n\3\f\3\16\3C\13\3\3\3\3\3\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4S\n\4\f\4\16\4V\13\4")
        buf.write("\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\7\6g\n\6\f\6\16\6j\13\6\3\6\3\6\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\5\7v\n\7\3\7\3\7\3\7\3\b\3\b\3\b\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u008b")
        buf.write("\n\t\3\t\3\t\7\t\u008f\n\t\f\t\16\t\u0092\13\t\3\t\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00a0\n\t")
        buf.write("\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00b6\n")
        buf.write("\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\5\13\u00c3\n\13\7\13\u00c5\n\13\f\13\16\13\u00c8")
        buf.write("\13\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3")
        buf.write("\16\3\16\5\16\u00e3\n\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\5\16\u00eb\n\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3")
        buf.write("\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\5\20\u0104\n\20\3\21\3\21\3")
        buf.write("\21\3\21\3\21\3\21\3\21\3\21\5\21\u010e\n\21\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u0118\n\22\3\23\3")
        buf.write("\23\3\23\3\23\5\23\u011e\n\23\3\24\3\24\3\24\3\24\2\2")
        buf.write("\25\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&\2\2\2")
        buf.write("\u0130\2(\3\2\2\2\4\67\3\2\2\2\6F\3\2\2\2\bZ\3\2\2\2\n")
        buf.write("`\3\2\2\2\fm\3\2\2\2\16z\3\2\2\2\20}\3\2\2\2\22\u00a3")
        buf.write("\3\2\2\2\24\u00a7\3\2\2\2\26\u00cc\3\2\2\2\30\u00d4\3")
        buf.write("\2\2\2\32\u00dd\3\2\2\2\34\u00ee\3\2\2\2\36\u0103\3\2")
        buf.write("\2\2 \u010d\3\2\2\2\"\u0117\3\2\2\2$\u011d\3\2\2\2&\u011f")
        buf.write("\3\2\2\2()\b\2\1\2)*\7\3\2\2*+\7\22\2\2+,\5\6\4\2,\62")
        buf.write("\b\2\1\2-.\5\6\4\2./\b\2\1\2/\61\3\2\2\2\60-\3\2\2\2\61")
        buf.write("\64\3\2\2\2\62\60\3\2\2\2\62\63\3\2\2\2\63\65\3\2\2\2")
        buf.write("\64\62\3\2\2\2\65\66\b\2\1\2\66\3\3\2\2\2\678\b\3\1\2")
        buf.write("89\7\4\2\29:\7\22\2\2:;\5\6\4\2;A\b\3\1\2<=\5\6\4\2=>")
        buf.write("\b\3\1\2>@\3\2\2\2?<\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2")
        buf.write("\2\2BD\3\2\2\2CA\3\2\2\2DE\b\3\1\2E\5\3\2\2\2FG\b\4\1")
        buf.write("\2GH\7\r\2\2HI\b\4\1\2IJ\7\37\2\2JK\7\r\2\2KL\5&\24\2")
        buf.write("LT\b\4\1\2MN\7\20\2\2NO\7\r\2\2OP\5&\24\2PQ\b\4\1\2QS")
        buf.write("\3\2\2\2RM\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2\2\2UW\3\2")
        buf.write("\2\2VT\3\2\2\2WX\7 \2\2XY\b\4\1\2Y\7\3\2\2\2Z[\7\5\2\2")
        buf.write("[\\\7\22\2\2\\]\5\n\6\2]^\b\5\1\2^_\7\2\2\3_\t\3\2\2\2")
        buf.write("`a\b\6\1\2ab\5\f\7\2bh\b\6\1\2cd\5\f\7\2de\b\6\1\2eg\3")
        buf.write("\2\2\2fc\3\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2ik\3\2\2")
        buf.write("\2jh\3\2\2\2kl\b\6\1\2l\13\3\2\2\2mn\b\7\1\2no\5\16\b")
        buf.write("\2op\b\7\1\2pq\7\16\2\2qu\b\7\1\2rs\5\20\t\2st\b\7\1\2")
        buf.write("tv\3\2\2\2ur\3\2\2\2uv\3\2\2\2vw\3\2\2\2wx\7\23\2\2xy")
        buf.write("\b\7\1\2y\r\3\2\2\2z{\5\24\13\2{|\b\b\1\2|\17\3\2\2\2")
        buf.write("}\u0090\b\t\1\2~\177\5\24\13\2\177\u0080\b\t\1\2\u0080")
        buf.write("\u008b\3\2\2\2\u0081\u0082\5\32\16\2\u0082\u0083\b\t\1")
        buf.write("\2\u0083\u008b\3\2\2\2\u0084\u0085\5\26\f\2\u0085\u0086")
        buf.write("\b\t\1\2\u0086\u008b\3\2\2\2\u0087\u0088\5\22\n\2\u0088")
        buf.write("\u0089\b\t\1\2\u0089\u008b\3\2\2\2\u008a~\3\2\2\2\u008a")
        buf.write("\u0081\3\2\2\2\u008a\u0084\3\2\2\2\u008a\u0087\3\2\2\2")
        buf.write("\u008b\u008c\3\2\2\2\u008c\u008d\7\20\2\2\u008d\u008f")
        buf.write("\3\2\2\2\u008e\u008a\3\2\2\2\u008f\u0092\3\2\2\2\u0090")
        buf.write("\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u009f\3\2\2\2")
        buf.write("\u0092\u0090\3\2\2\2\u0093\u0094\5\24\13\2\u0094\u0095")
        buf.write("\b\t\1\2\u0095\u00a0\3\2\2\2\u0096\u0097\5\32\16\2\u0097")
        buf.write("\u0098\b\t\1\2\u0098\u00a0\3\2\2\2\u0099\u009a\5\26\f")
        buf.write("\2\u009a\u009b\b\t\1\2\u009b\u00a0\3\2\2\2\u009c\u009d")
        buf.write("\5\22\n\2\u009d\u009e\b\t\1\2\u009e\u00a0\3\2\2\2\u009f")
        buf.write("\u0093\3\2\2\2\u009f\u0096\3\2\2\2\u009f\u0099\3\2\2\2")
        buf.write("\u009f\u009c\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\b")
        buf.write("\t\1\2\u00a2\21\3\2\2\2\u00a3\u00a4\7\30\2\2\u00a4\u00a5")
        buf.write("\5\24\13\2\u00a5\u00a6\b\n\1\2\u00a6\23\3\2\2\2\u00a7")
        buf.write("\u00a8\b\13\1\2\u00a8\u00a9\7\r\2\2\u00a9\u00aa\b\13\1")
        buf.write("\2\u00aa\u00b5\7\37\2\2\u00ab\u00ac\7\r\2\2\u00ac\u00b6")
        buf.write("\b\13\1\2\u00ad\u00ae\5\34\17\2\u00ae\u00af\b\13\1\2\u00af")
        buf.write("\u00b6\3\2\2\2\u00b0\u00b1\7\17\2\2\u00b1\u00b6\b\13\1")
        buf.write("\2\u00b2\u00b3\5$\23\2\u00b3\u00b4\b\13\1\2\u00b4\u00b6")
        buf.write("\3\2\2\2\u00b5\u00ab\3\2\2\2\u00b5\u00ad\3\2\2\2\u00b5")
        buf.write("\u00b0\3\2\2\2\u00b5\u00b2\3\2\2\2\u00b6\u00c6\3\2\2\2")
        buf.write("\u00b7\u00c2\7\20\2\2\u00b8\u00b9\7\r\2\2\u00b9\u00c3")
        buf.write("\b\13\1\2\u00ba\u00bb\5\34\17\2\u00bb\u00bc\b\13\1\2\u00bc")
        buf.write("\u00c3\3\2\2\2\u00bd\u00be\7\17\2\2\u00be\u00c3\b\13\1")
        buf.write("\2\u00bf\u00c0\5$\23\2\u00c0\u00c1\b\13\1\2\u00c1\u00c3")
        buf.write("\3\2\2\2\u00c2\u00b8\3\2\2\2\u00c2\u00ba\3\2\2\2\u00c2")
        buf.write("\u00bd\3\2\2\2\u00c2\u00bf\3\2\2\2\u00c3\u00c5\3\2\2\2")
        buf.write("\u00c4\u00b7\3\2\2\2\u00c5\u00c8\3\2\2\2\u00c6\u00c4\3")
        buf.write("\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c9\3\2\2\2\u00c8\u00c6")
        buf.write("\3\2\2\2\u00c9\u00ca\7 \2\2\u00ca\u00cb\b\13\1\2\u00cb")
        buf.write("\25\3\2\2\2\u00cc\u00cd\b\f\1\2\u00cd\u00ce\7\r\2\2\u00ce")
        buf.write("\u00cf\b\f\1\2\u00cf\u00d0\7\32\2\2\u00d0\u00d1\5\30\r")
        buf.write("\2\u00d1\u00d2\b\f\1\2\u00d2\u00d3\b\f\1\2\u00d3\27\3")
        buf.write("\2\2\2\u00d4\u00d5\b\r\1\2\u00d5\u00d6\7\r\2\2\u00d6\u00d7")
        buf.write("\b\r\1\2\u00d7\u00d8\5\"\22\2\u00d8\u00d9\b\r\1\2\u00d9")
        buf.write("\u00da\7\r\2\2\u00da\u00db\b\r\1\2\u00db\u00dc\b\r\1\2")
        buf.write("\u00dc\31\3\2\2\2\u00dd\u00e2\b\16\1\2\u00de\u00df\7\r")
        buf.write("\2\2\u00df\u00e3\b\16\1\2\u00e0\u00e1\7\6\2\2\u00e1\u00e3")
        buf.write("\b\16\1\2\u00e2\u00de\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e3")
        buf.write("\u00e4\3\2\2\2\u00e4\u00e5\5\36\20\2\u00e5\u00ea\b\16")
        buf.write("\1\2\u00e6\u00e7\7\r\2\2\u00e7\u00eb\b\16\1\2\u00e8\u00e9")
        buf.write("\7\6\2\2\u00e9\u00eb\b\16\1\2\u00ea\u00e6\3\2\2\2\u00ea")
        buf.write("\u00e8\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec\u00ed\b\16\1")
        buf.write("\2\u00ed\33\3\2\2\2\u00ee\u00ef\b\17\1\2\u00ef\u00f0\5")
        buf.write(" \21\2\u00f0\u00f1\b\17\1\2\u00f1\u00f2\7\37\2\2\u00f2")
        buf.write("\u00f3\7\r\2\2\u00f3\u00f4\b\17\1\2\u00f4\u00f5\7 \2\2")
        buf.write("\u00f5\u00f6\b\17\1\2\u00f6\35\3\2\2\2\u00f7\u00f8\7\31")
        buf.write("\2\2\u00f8\u0104\b\20\1\2\u00f9\u00fa\7\32\2\2\u00fa\u0104")
        buf.write("\b\20\1\2\u00fb\u00fc\7\34\2\2\u00fc\u0104\b\20\1\2\u00fd")
        buf.write("\u00fe\7\33\2\2\u00fe\u0104\b\20\1\2\u00ff\u0100\7\36")
        buf.write("\2\2\u0100\u0104\b\20\1\2\u0101\u0102\7\35\2\2\u0102\u0104")
        buf.write("\b\20\1\2\u0103\u00f7\3\2\2\2\u0103\u00f9\3\2\2\2\u0103")
        buf.write("\u00fb\3\2\2\2\u0103\u00fd\3\2\2\2\u0103\u00ff\3\2\2\2")
        buf.write("\u0103\u0101\3\2\2\2\u0104\37\3\2\2\2\u0105\u0106\7\t")
        buf.write("\2\2\u0106\u010e\b\21\1\2\u0107\u0108\7\n\2\2\u0108\u010e")
        buf.write("\b\21\1\2\u0109\u010a\7\13\2\2\u010a\u010e\b\21\1\2\u010b")
        buf.write("\u010c\7\f\2\2\u010c\u010e\b\21\1\2\u010d\u0105\3\2\2")
        buf.write("\2\u010d\u0107\3\2\2\2\u010d\u0109\3\2\2\2\u010d\u010b")
        buf.write("\3\2\2\2\u010e!\3\2\2\2\u010f\u0110\7\24\2\2\u0110\u0118")
        buf.write("\b\22\1\2\u0111\u0112\7\25\2\2\u0112\u0118\b\22\1\2\u0113")
        buf.write("\u0114\7\26\2\2\u0114\u0118\b\22\1\2\u0115\u0116\7\27")
        buf.write("\2\2\u0116\u0118\b\22\1\2\u0117\u010f\3\2\2\2\u0117\u0111")
        buf.write("\3\2\2\2\u0117\u0113\3\2\2\2\u0117\u0115\3\2\2\2\u0118")
        buf.write("#\3\2\2\2\u0119\u011a\7\6\2\2\u011a\u011e\b\23\1\2\u011b")
        buf.write("\u011c\7\7\2\2\u011c\u011e\b\23\1\2\u011d\u0119\3\2\2")
        buf.write("\2\u011d\u011b\3\2\2\2\u011e%\3\2\2\2\u011f\u0120\7\b")
        buf.write("\2\2\u0120\u0121\b\24\1\2\u0121\'\3\2\2\2\23\62AThu\u008a")
        buf.write("\u0090\u009f\u00b5\u00c2\u00c6\u00e2\u00ea\u0103\u010d")
        buf.write("\u0117\u011d")
        return buf.getvalue()


class DatalogParser ( Parser ):

    grammarFileName = "Datalog.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'EDB_DECL'", "'IDB_DECL'", "'RULE_DECL'", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "'MIN'", "'MAX'", 
                     "'SUM'", "'COUNT'", "<INVALID>", "':-'", "'_'", "','", 
                     "';'", "':'", "'.'", "'+'", "'-'", "'*'", "'/'", "'!'", 
                     "'!='", "'='", "'>='", "'>'", "'<='", "'<'", "'('", 
                     "')'" ]

    symbolicNames = [ "<INVALID>", "TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", 
                      "TOKEN_INTEGER", "TOKEN_STRING", "TOKEN_INT", "TOKEN_MIN", 
                      "TOKEN_MAX", "TOKEN_SUM", "TOKEN_COUNT", "TOKEN_ID", 
                      "TOKEN_BODY_HEAD_SEP", "TOKEN_ANY", "TOKEN_COMMA", 
                      "TOKEN_SEMICOLON", "TOKEN_COLON", "TOKEN_DOT", "TOKEN_PLUS", 
                      "TOKEN_MINUS", "TOKEN_MULT", "TOKEN_DIV", "TOKEN_NOT", 
                      "TOKEN_NOT_EQUALS", "TOKEN_EQUALS", "TOKEN_GREATER_EQUAL_THAN", 
                      "TOKEN_GREATER_THAN", "TOKEN_LESS_EQUAL_THAN", "TOKEN_LESS_THAN", 
                      "TOKEN_LEFT_PAREN", "TOKEN_RIGHT_PAREN", "TOKEN_WS" ]

    RULE_datalog_edb_declare = 0
    RULE_datalog_idb_declare = 1
    RULE_datalog_relation_schema = 2
    RULE_datalog_rule_declare = 3
    RULE_datalog_program = 4
    RULE_datalog_rule = 5
    RULE_head = 6
    RULE_body = 7
    RULE_negation = 8
    RULE_atom = 9
    RULE_assign = 10
    RULE_math_expr = 11
    RULE_compare_expr = 12
    RULE_aggregation_expr = 13
    RULE_compare_op = 14
    RULE_aggregation_op = 15
    RULE_math_op = 16
    RULE_constant = 17
    RULE_data_type = 18

    ruleNames =  [ "datalog_edb_declare", "datalog_idb_declare", "datalog_relation_schema", 
                   "datalog_rule_declare", "datalog_program", "datalog_rule", 
                   "head", "body", "negation", "atom", "assign", "math_expr", 
                   "compare_expr", "aggregation_expr", "compare_op", "aggregation_op", 
                   "math_op", "constant", "data_type" ]

    EOF = Token.EOF
    TOKEN_EDB=1
    TOKEN_IDB=2
    TOKEN_RULE=3
    TOKEN_INTEGER=4
    TOKEN_STRING=5
    TOKEN_INT=6
    TOKEN_MIN=7
    TOKEN_MAX=8
    TOKEN_SUM=9
    TOKEN_COUNT=10
    TOKEN_ID=11
    TOKEN_BODY_HEAD_SEP=12
    TOKEN_ANY=13
    TOKEN_COMMA=14
    TOKEN_SEMICOLON=15
    TOKEN_COLON=16
    TOKEN_DOT=17
    TOKEN_PLUS=18
    TOKEN_MINUS=19
    TOKEN_MULT=20
    TOKEN_DIV=21
    TOKEN_NOT=22
    TOKEN_NOT_EQUALS=23
    TOKEN_EQUALS=24
    TOKEN_GREATER_EQUAL_THAN=25
    TOKEN_GREATER_THAN=26
    TOKEN_LESS_EQUAL_THAN=27
    TOKEN_LESS_THAN=28
    TOKEN_LEFT_PAREN=29
    TOKEN_RIGHT_PAREN=30
    TOKEN_WS=31

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class AtomArg():
        def __init__(self, arg_name, arg_type):
            self.name = arg_name
            self.type = arg_type


    class Datalog_edb_declareContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.schema1 = None # Datalog_relation_schemaContext
            self.schema2 = None # Datalog_relation_schemaContext

        def TOKEN_EDB(self):
            return self.getToken(DatalogParser.TOKEN_EDB, 0)

        def TOKEN_COLON(self):
            return self.getToken(DatalogParser.TOKEN_COLON, 0)

        def datalog_relation_schema(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Datalog_relation_schemaContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Datalog_relation_schemaContext,i)


        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_edb_declare




    def datalog_edb_declare(self):

        localctx = DatalogParser.Datalog_edb_declareContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_datalog_edb_declare)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            edb_list = []
            self.state = 39
            self.match(DatalogParser.TOKEN_EDB)
            self.state = 40
            self.match(DatalogParser.TOKEN_COLON)
            self.state = 41
            localctx.schema1 = self.datalog_relation_schema()
            edb_list.append(localctx.schema1.r)
            self.state = 48
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DatalogParser.TOKEN_ID:
                self.state = 43
                localctx.schema2 = self.datalog_relation_schema()
                edb_list.append(localctx.schema2.r)
                self.state = 50
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            localctx.r = edb_list
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Datalog_idb_declareContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.schema1 = None # Datalog_relation_schemaContext
            self.schema2 = None # Datalog_relation_schemaContext

        def TOKEN_IDB(self):
            return self.getToken(DatalogParser.TOKEN_IDB, 0)

        def TOKEN_COLON(self):
            return self.getToken(DatalogParser.TOKEN_COLON, 0)

        def datalog_relation_schema(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Datalog_relation_schemaContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Datalog_relation_schemaContext,i)


        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_idb_declare




    def datalog_idb_declare(self):

        localctx = DatalogParser.Datalog_idb_declareContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_datalog_idb_declare)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            idb_list = []
            self.state = 54
            self.match(DatalogParser.TOKEN_IDB)
            self.state = 55
            self.match(DatalogParser.TOKEN_COLON)
            self.state = 56
            localctx.schema1 = self.datalog_relation_schema()
            idb_list.append(localctx.schema1.r)
            self.state = 63
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DatalogParser.TOKEN_ID:
                self.state = 58
                localctx.schema2 = self.datalog_relation_schema()
                idb_list.append(localctx.schema2.r)
                self.state = 65
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            localctx.r = idb_list
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Datalog_relation_schemaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.relation_name = None # Token
            self.t1 = None # Token
            self.d1 = None # Data_typeContext
            self.t2 = None # Token
            self.d2 = None # Data_typeContext

        def TOKEN_LEFT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_LEFT_PAREN, 0)

        def TOKEN_RIGHT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_RIGHT_PAREN, 0)

        def TOKEN_ID(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_ID)
            else:
                return self.getToken(DatalogParser.TOKEN_ID, i)

        def data_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Data_typeContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Data_typeContext,i)


        def TOKEN_COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_COMMA)
            else:
                return self.getToken(DatalogParser.TOKEN_COMMA, i)

        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_relation_schema




    def datalog_relation_schema(self):

        localctx = DatalogParser.Datalog_relation_schemaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_datalog_relation_schema)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            schema = {'name': '', 'attributes': []}
            self.state = 69
            localctx.relation_name = self.match(DatalogParser.TOKEN_ID)
            schema['name'] = (None if localctx.relation_name is None else localctx.relation_name.text)
            self.state = 71
            self.match(DatalogParser.TOKEN_LEFT_PAREN)
            self.state = 72
            localctx.t1 = self.match(DatalogParser.TOKEN_ID)
            self.state = 73
            localctx.d1 = self.data_type()
            schema['attributes'].append(self.AtomArg((None if localctx.t1 is None else localctx.t1.text), localctx.d1.r))
            self.state = 82
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DatalogParser.TOKEN_COMMA:
                self.state = 75
                self.match(DatalogParser.TOKEN_COMMA)
                self.state = 76
                localctx.t2 = self.match(DatalogParser.TOKEN_ID)
                self.state = 77
                localctx.d2 = self.data_type()
                schema['attributes'].append(self.AtomArg((None if localctx.t2 is None else localctx.t2.text), localctx.d2.r))
                self.state = 84
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 85
            self.match(DatalogParser.TOKEN_RIGHT_PAREN)
            localctx.r = schema
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Datalog_rule_declareContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.dp = None # Datalog_programContext

        def TOKEN_RULE(self):
            return self.getToken(DatalogParser.TOKEN_RULE, 0)

        def TOKEN_COLON(self):
            return self.getToken(DatalogParser.TOKEN_COLON, 0)

        def EOF(self):
            return self.getToken(DatalogParser.EOF, 0)

        def datalog_program(self):
            return self.getTypedRuleContext(DatalogParser.Datalog_programContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_rule_declare




    def datalog_rule_declare(self):

        localctx = DatalogParser.Datalog_rule_declareContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_datalog_rule_declare)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 88
            self.match(DatalogParser.TOKEN_RULE)
            self.state = 89
            self.match(DatalogParser.TOKEN_COLON)
            self.state = 90
            localctx.dp = self.datalog_program()
            localctx.r = localctx.dp.r
            self.state = 92
            self.match(DatalogParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Datalog_programContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.r1 = None # Datalog_ruleContext
            self.r2 = None # Datalog_ruleContext

        def datalog_rule(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Datalog_ruleContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Datalog_ruleContext,i)


        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_program




    def datalog_program(self):

        localctx = DatalogParser.Datalog_programContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_datalog_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            rule_list = []
            self.state = 95
            localctx.r1 = self.datalog_rule()
            rule_list.append(localctx.r1.r)
            self.state = 102
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DatalogParser.TOKEN_ID:
                self.state = 97
                localctx.r2 = self.datalog_rule()
                rule_list.append(localctx.r2.r)
                self.state = 104
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            localctx.r = rule_list
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Datalog_ruleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.h = None # HeadContext
            self.b = None # BodyContext

        def TOKEN_BODY_HEAD_SEP(self):
            return self.getToken(DatalogParser.TOKEN_BODY_HEAD_SEP, 0)

        def TOKEN_DOT(self):
            return self.getToken(DatalogParser.TOKEN_DOT, 0)

        def head(self):
            return self.getTypedRuleContext(DatalogParser.HeadContext,0)


        def body(self):
            return self.getTypedRuleContext(DatalogParser.BodyContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_datalog_rule




    def datalog_rule(self):

        localctx = DatalogParser.Datalog_ruleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_datalog_rule)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            rule_dic = {}
            self.state = 108
            localctx.h = self.head()
            rule_dic['head'] = localctx.h.r
            self.state = 110
            self.match(DatalogParser.TOKEN_BODY_HEAD_SEP)
            rule_dic['body'] = None
            self.state = 115
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << DatalogParser.TOKEN_INTEGER) | (1 << DatalogParser.TOKEN_ID) | (1 << DatalogParser.TOKEN_NOT))) != 0):
                self.state = 112
                localctx.b = self.body()
                rule_dic['body'] = localctx.b.r


            self.state = 117
            self.match(DatalogParser.TOKEN_DOT)
            localctx.r = rule_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HeadContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.a = None # AtomContext

        def atom(self):
            return self.getTypedRuleContext(DatalogParser.AtomContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_head




    def head(self):

        localctx = DatalogParser.HeadContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_head)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 120
            localctx.a = self.atom()
            localctx.r = localctx.a.r
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.b1 = None # AtomContext
            self.b2 = None # Compare_exprContext
            self.b3 = None # AssignContext
            self.b4 = None # NegationContext
            self.b5 = None # AtomContext
            self.b6 = None # Compare_exprContext
            self.b7 = None # AssignContext
            self.b8 = None # NegationContext

        def TOKEN_COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_COMMA)
            else:
                return self.getToken(DatalogParser.TOKEN_COMMA, i)

        def atom(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.AtomContext)
            else:
                return self.getTypedRuleContext(DatalogParser.AtomContext,i)


        def compare_expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Compare_exprContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Compare_exprContext,i)


        def assign(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.AssignContext)
            else:
                return self.getTypedRuleContext(DatalogParser.AssignContext,i)


        def negation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.NegationContext)
            else:
                return self.getTypedRuleContext(DatalogParser.NegationContext,i)


        def getRuleIndex(self):
            return DatalogParser.RULE_body




    def body(self):

        localctx = DatalogParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_body)
        try:
            self.enterOuterAlt(localctx, 1)
            body_dic = {'atoms':[], 'compares': [], 'assigns':[], 'negations':[]}
            self.state = 142
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 136
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                    if la_ == 1:
                        self.state = 124
                        localctx.b1 = self.atom()
                        body_dic['atoms'].append(localctx.b1.r)
                        pass

                    elif la_ == 2:
                        self.state = 127
                        localctx.b2 = self.compare_expr()
                        body_dic['compares'].append(localctx.b2.r)
                        pass

                    elif la_ == 3:
                        self.state = 130
                        localctx.b3 = self.assign()
                        body_dic['assigns'].append(localctx.b3.r)
                        pass

                    elif la_ == 4:
                        self.state = 133
                        localctx.b4 = self.negation()
                        body_dic['negations'].append(localctx.b4.r)
                        pass


                    self.state = 138
                    self.match(DatalogParser.TOKEN_COMMA) 
                self.state = 144
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

            self.state = 157
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.state = 145
                localctx.b5 = self.atom()
                body_dic['atoms'].append(localctx.b5.r)
                pass

            elif la_ == 2:
                self.state = 148
                localctx.b6 = self.compare_expr()
                body_dic['compares'].append(localctx.b6.r)
                pass

            elif la_ == 3:
                self.state = 151
                localctx.b7 = self.assign()
                body_dic['assigns'].append(localctx.b7.r)
                pass

            elif la_ == 4:
                self.state = 154
                localctx.b8 = self.negation()
                body_dic['negations'].append(localctx.b8.r)
                pass


            localctx.r = body_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NegationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.a = None # AtomContext

        def TOKEN_NOT(self):
            return self.getToken(DatalogParser.TOKEN_NOT, 0)

        def atom(self):
            return self.getTypedRuleContext(DatalogParser.AtomContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_negation




    def negation(self):

        localctx = DatalogParser.NegationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_negation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 161
            self.match(DatalogParser.TOKEN_NOT)
            self.state = 162
            localctx.a = self.atom()
            localctx.r = localctx.a.r
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.a1 = None # Token
            self.a2 = None # Token
            self.a3 = None # Aggregation_exprContext
            self.a4 = None # Token
            self.a5 = None # ConstantContext
            self.a6 = None # Token
            self.a7 = None # Aggregation_exprContext
            self.a8 = None # Token
            self.a9 = None # ConstantContext

        def TOKEN_LEFT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_LEFT_PAREN, 0)

        def TOKEN_RIGHT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_RIGHT_PAREN, 0)

        def TOKEN_ID(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_ID)
            else:
                return self.getToken(DatalogParser.TOKEN_ID, i)

        def aggregation_expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.Aggregation_exprContext)
            else:
                return self.getTypedRuleContext(DatalogParser.Aggregation_exprContext,i)


        def TOKEN_ANY(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_ANY)
            else:
                return self.getToken(DatalogParser.TOKEN_ANY, i)

        def constant(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(DatalogParser.ConstantContext)
            else:
                return self.getTypedRuleContext(DatalogParser.ConstantContext,i)


        def TOKEN_COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_COMMA)
            else:
                return self.getToken(DatalogParser.TOKEN_COMMA, i)

        def getRuleIndex(self):
            return DatalogParser.RULE_atom




    def atom(self):

        localctx = DatalogParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_atom)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            atom_dic = {'name': 'unknown', 'arg_list':[]}
            self.state = 166
            localctx.a1 = self.match(DatalogParser.TOKEN_ID)
            atom_dic['name'] = (None if localctx.a1 is None else localctx.a1.text)
            self.state = 168
            self.match(DatalogParser.TOKEN_LEFT_PAREN)
            self.state = 179
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_ID]:
                self.state = 169
                localctx.a2 = self.match(DatalogParser.TOKEN_ID)
                atom_dic['arg_list'].append(self.AtomArg((None if localctx.a2 is None else localctx.a2.text), 'variable'))
                pass
            elif token in [DatalogParser.TOKEN_MIN, DatalogParser.TOKEN_MAX, DatalogParser.TOKEN_SUM, DatalogParser.TOKEN_COUNT]:
                self.state = 171
                localctx.a3 = self.aggregation_expr()
                atom_dic['arg_list'].append(self.AtomArg(localctx.a3.r, 'aggregation'))
                pass
            elif token in [DatalogParser.TOKEN_ANY]:
                self.state = 174
                localctx.a4 = self.match(DatalogParser.TOKEN_ANY)
                atom_dic['arg_list'].append(self.AtomArg((None if localctx.a4 is None else localctx.a4.text), 'any'))
                pass
            elif token in [DatalogParser.TOKEN_INTEGER, DatalogParser.TOKEN_STRING]:
                self.state = 176
                localctx.a5 = self.constant()
                atom_dic['arg_list'].append(self.AtomArg((None if localctx.a5 is None else self._input.getText((localctx.a5.start,localctx.a5.stop))), 'constant'))
                pass
            else:
                raise NoViableAltException(self)

            self.state = 196
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==DatalogParser.TOKEN_COMMA:
                self.state = 181
                self.match(DatalogParser.TOKEN_COMMA)
                self.state = 192
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [DatalogParser.TOKEN_ID]:
                    self.state = 182
                    localctx.a6 = self.match(DatalogParser.TOKEN_ID)
                    atom_dic['arg_list'].append(self.AtomArg((None if localctx.a6 is None else localctx.a6.text), 'variable'))
                    pass
                elif token in [DatalogParser.TOKEN_MIN, DatalogParser.TOKEN_MAX, DatalogParser.TOKEN_SUM, DatalogParser.TOKEN_COUNT]:
                    self.state = 184
                    localctx.a7 = self.aggregation_expr()
                    atom_dic['arg_list'].append(self.AtomArg(localctx.a7.r, 'aggregation'))
                    pass
                elif token in [DatalogParser.TOKEN_ANY]:
                    self.state = 187
                    localctx.a8 = self.match(DatalogParser.TOKEN_ANY)
                    atom_dic['arg_list'].append(self.AtomArg((None if localctx.a8 is None else localctx.a8.text), 'any'))
                    pass
                elif token in [DatalogParser.TOKEN_INTEGER, DatalogParser.TOKEN_STRING]:
                    self.state = 189
                    localctx.a9 = self.constant()
                    atom_dic['arg_list'].append(self.AtomArg((None if localctx.a9 is None else self._input.getText((localctx.a9.start,localctx.a9.stop))), 'constant'))
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 198
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 199
            self.match(DatalogParser.TOKEN_RIGHT_PAREN)
            localctx.r = atom_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.a1 = None # Token
            self.a2 = None # Math_exprContext

        def TOKEN_EQUALS(self):
            return self.getToken(DatalogParser.TOKEN_EQUALS, 0)

        def TOKEN_ID(self):
            return self.getToken(DatalogParser.TOKEN_ID, 0)

        def math_expr(self):
            return self.getTypedRuleContext(DatalogParser.Math_exprContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_assign




    def assign(self):

        localctx = DatalogParser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            assign_dic = {}
            self.state = 203
            localctx.a1 = self.match(DatalogParser.TOKEN_ID)
            assign_dic['lhs'] = (None if localctx.a1 is None else localctx.a1.text)
            self.state = 205
            self.match(DatalogParser.TOKEN_EQUALS)
            self.state = 206
            localctx.a2 = self.math_expr()
            assign_dic['rhs'] = localctx.a2.r
            localctx.r = assign_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Math_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.m1 = None # Token
            self.m2 = None # Math_opContext
            self.m3 = None # Token

        def TOKEN_ID(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_ID)
            else:
                return self.getToken(DatalogParser.TOKEN_ID, i)

        def math_op(self):
            return self.getTypedRuleContext(DatalogParser.Math_opContext,0)


        def getRuleIndex(self):
            return DatalogParser.RULE_math_expr




    def math_expr(self):

        localctx = DatalogParser.Math_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_math_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            math_dic = {}
            self.state = 211
            localctx.m1 = self.match(DatalogParser.TOKEN_ID)
            math_dic['lhs'] = (None if localctx.m1 is None else localctx.m1.text)
            self.state = 213
            localctx.m2 = self.math_op()
            math_dic['op'] = localctx.m2.r
            self.state = 215
            localctx.m3 = self.match(DatalogParser.TOKEN_ID)
            math_dic['rhs'] = (None if localctx.m3 is None else localctx.m3.text)
            localctx.r = math_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Compare_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.c1 = None # Token
            self.c2 = None # Token
            self.op = None # Compare_opContext
            self.c4 = None # Token
            self.c5 = None # Token

        def compare_op(self):
            return self.getTypedRuleContext(DatalogParser.Compare_opContext,0)


        def TOKEN_ID(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_ID)
            else:
                return self.getToken(DatalogParser.TOKEN_ID, i)

        def TOKEN_INTEGER(self, i:int=None):
            if i is None:
                return self.getTokens(DatalogParser.TOKEN_INTEGER)
            else:
                return self.getToken(DatalogParser.TOKEN_INTEGER, i)

        def getRuleIndex(self):
            return DatalogParser.RULE_compare_expr




    def compare_expr(self):

        localctx = DatalogParser.Compare_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_compare_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            compare_dic = {}
            self.state = 224
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_ID]:
                self.state = 220
                localctx.c1 = self.match(DatalogParser.TOKEN_ID)
                compare_dic['lhs'] = [(None if localctx.c1 is None else localctx.c1.text), 'var']
                pass
            elif token in [DatalogParser.TOKEN_INTEGER]:
                self.state = 222
                localctx.c2 = self.match(DatalogParser.TOKEN_INTEGER)
                compare_dic['lhs'] = [(None if localctx.c2 is None else localctx.c2.text), 'num']
                pass
            else:
                raise NoViableAltException(self)

            self.state = 226
            localctx.op = self.compare_op()
            compare_dic['op'] = localctx.op.r
            self.state = 232
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_ID]:
                self.state = 228
                localctx.c4 = self.match(DatalogParser.TOKEN_ID)
                compare_dic['rhs'] = [(None if localctx.c4 is None else localctx.c4.text), 'var']
                pass
            elif token in [DatalogParser.TOKEN_INTEGER]:
                self.state = 230
                localctx.c5 = self.match(DatalogParser.TOKEN_INTEGER)
                compare_dic['rhs'] = [(None if localctx.c5 is None else localctx.c5.text), 'num']
                pass
            else:
                raise NoViableAltException(self)

            localctx.r = compare_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Aggregation_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.a1 = None # Aggregation_opContext
            self.a2 = None # Token

        def TOKEN_LEFT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_LEFT_PAREN, 0)

        def TOKEN_RIGHT_PAREN(self):
            return self.getToken(DatalogParser.TOKEN_RIGHT_PAREN, 0)

        def aggregation_op(self):
            return self.getTypedRuleContext(DatalogParser.Aggregation_opContext,0)


        def TOKEN_ID(self):
            return self.getToken(DatalogParser.TOKEN_ID, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_aggregation_expr




    def aggregation_expr(self):

        localctx = DatalogParser.Aggregation_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_aggregation_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            agg_dic = {}
            self.state = 237
            localctx.a1 = self.aggregation_op()
            agg_dic['agg_op'] = localctx.a1.r
            self.state = 239
            self.match(DatalogParser.TOKEN_LEFT_PAREN)
            self.state = 240
            localctx.a2 = self.match(DatalogParser.TOKEN_ID)
            agg_dic['agg_arg'] = (None if localctx.a2 is None else localctx.a2.text)
            self.state = 242
            self.match(DatalogParser.TOKEN_RIGHT_PAREN)
            localctx.r = agg_dic
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Compare_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.op1 = None # Token
            self.op2 = None # Token
            self.op3 = None # Token
            self.op4 = None # Token
            self.op5 = None # Token
            self.op6 = None # Token

        def TOKEN_NOT_EQUALS(self):
            return self.getToken(DatalogParser.TOKEN_NOT_EQUALS, 0)

        def TOKEN_EQUALS(self):
            return self.getToken(DatalogParser.TOKEN_EQUALS, 0)

        def TOKEN_GREATER_THAN(self):
            return self.getToken(DatalogParser.TOKEN_GREATER_THAN, 0)

        def TOKEN_GREATER_EQUAL_THAN(self):
            return self.getToken(DatalogParser.TOKEN_GREATER_EQUAL_THAN, 0)

        def TOKEN_LESS_THAN(self):
            return self.getToken(DatalogParser.TOKEN_LESS_THAN, 0)

        def TOKEN_LESS_EQUAL_THAN(self):
            return self.getToken(DatalogParser.TOKEN_LESS_EQUAL_THAN, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_compare_op




    def compare_op(self):

        localctx = DatalogParser.Compare_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_compare_op)
        try:
            self.state = 257
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_NOT_EQUALS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 245
                localctx.op1 = self.match(DatalogParser.TOKEN_NOT_EQUALS)
                localctx.r = (None if localctx.op1 is None else localctx.op1.text)
                pass
            elif token in [DatalogParser.TOKEN_EQUALS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 247
                localctx.op2 = self.match(DatalogParser.TOKEN_EQUALS)
                localctx.r = (None if localctx.op2 is None else localctx.op2.text)
                pass
            elif token in [DatalogParser.TOKEN_GREATER_THAN]:
                self.enterOuterAlt(localctx, 3)
                self.state = 249
                localctx.op3 = self.match(DatalogParser.TOKEN_GREATER_THAN)
                localctx.r = (None if localctx.op3 is None else localctx.op3.text)
                pass
            elif token in [DatalogParser.TOKEN_GREATER_EQUAL_THAN]:
                self.enterOuterAlt(localctx, 4)
                self.state = 251
                localctx.op4 = self.match(DatalogParser.TOKEN_GREATER_EQUAL_THAN)
                localctx.r = (None if localctx.op4 is None else localctx.op4.text)
                pass
            elif token in [DatalogParser.TOKEN_LESS_THAN]:
                self.enterOuterAlt(localctx, 5)
                self.state = 253
                localctx.op5 = self.match(DatalogParser.TOKEN_LESS_THAN)
                localctx.r = (None if localctx.op5 is None else localctx.op5.text)
                pass
            elif token in [DatalogParser.TOKEN_LESS_EQUAL_THAN]:
                self.enterOuterAlt(localctx, 6)
                self.state = 255
                localctx.op6 = self.match(DatalogParser.TOKEN_LESS_EQUAL_THAN)
                localctx.r = (None if localctx.op6 is None else localctx.op6.text)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Aggregation_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.op1 = None # Token
            self.op2 = None # Token
            self.op3 = None # Token
            self.op4 = None # Token

        def TOKEN_MIN(self):
            return self.getToken(DatalogParser.TOKEN_MIN, 0)

        def TOKEN_MAX(self):
            return self.getToken(DatalogParser.TOKEN_MAX, 0)

        def TOKEN_SUM(self):
            return self.getToken(DatalogParser.TOKEN_SUM, 0)

        def TOKEN_COUNT(self):
            return self.getToken(DatalogParser.TOKEN_COUNT, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_aggregation_op




    def aggregation_op(self):

        localctx = DatalogParser.Aggregation_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_aggregation_op)
        try:
            self.state = 267
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_MIN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 259
                localctx.op1 = self.match(DatalogParser.TOKEN_MIN)
                localctx.r = (None if localctx.op1 is None else localctx.op1.text)
                pass
            elif token in [DatalogParser.TOKEN_MAX]:
                self.enterOuterAlt(localctx, 2)
                self.state = 261
                localctx.op2 = self.match(DatalogParser.TOKEN_MAX)
                localctx.r = (None if localctx.op2 is None else localctx.op2.text)
                pass
            elif token in [DatalogParser.TOKEN_SUM]:
                self.enterOuterAlt(localctx, 3)
                self.state = 263
                localctx.op3 = self.match(DatalogParser.TOKEN_SUM)
                localctx.r = (None if localctx.op3 is None else localctx.op3.text)
                pass
            elif token in [DatalogParser.TOKEN_COUNT]:
                self.enterOuterAlt(localctx, 4)
                self.state = 265
                localctx.op4 = self.match(DatalogParser.TOKEN_COUNT)
                localctx.r = (None if localctx.op4 is None else localctx.op4.text)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Math_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.op1 = None # Token
            self.op2 = None # Token
            self.op3 = None # Token
            self.op4 = None # Token

        def TOKEN_PLUS(self):
            return self.getToken(DatalogParser.TOKEN_PLUS, 0)

        def TOKEN_MINUS(self):
            return self.getToken(DatalogParser.TOKEN_MINUS, 0)

        def TOKEN_MULT(self):
            return self.getToken(DatalogParser.TOKEN_MULT, 0)

        def TOKEN_DIV(self):
            return self.getToken(DatalogParser.TOKEN_DIV, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_math_op




    def math_op(self):

        localctx = DatalogParser.Math_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_math_op)
        try:
            self.state = 277
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_PLUS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 269
                localctx.op1 = self.match(DatalogParser.TOKEN_PLUS)
                localctx.r = (None if localctx.op1 is None else localctx.op1.text)
                pass
            elif token in [DatalogParser.TOKEN_MINUS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 271
                localctx.op2 = self.match(DatalogParser.TOKEN_MINUS)
                localctx.r = (None if localctx.op2 is None else localctx.op2.text)
                pass
            elif token in [DatalogParser.TOKEN_MULT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 273
                localctx.op3 = self.match(DatalogParser.TOKEN_MULT)
                localctx.r = (None if localctx.op3 is None else localctx.op3.text)
                pass
            elif token in [DatalogParser.TOKEN_DIV]:
                self.enterOuterAlt(localctx, 4)
                self.state = 275
                localctx.op4 = self.match(DatalogParser.TOKEN_DIV)
                localctx.r = (None if localctx.op4 is None else localctx.op4.text)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ConstantContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.c1 = None # Token
            self.c2 = None # Token

        def TOKEN_INTEGER(self):
            return self.getToken(DatalogParser.TOKEN_INTEGER, 0)

        def TOKEN_STRING(self):
            return self.getToken(DatalogParser.TOKEN_STRING, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_constant




    def constant(self):

        localctx = DatalogParser.ConstantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_constant)
        try:
            self.state = 283
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [DatalogParser.TOKEN_INTEGER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 279
                localctx.c1 = self.match(DatalogParser.TOKEN_INTEGER)
                localctx.r = (None if localctx.c1 is None else localctx.c1.text)
                pass
            elif token in [DatalogParser.TOKEN_STRING]:
                self.enterOuterAlt(localctx, 2)
                self.state = 281
                localctx.c2 = self.match(DatalogParser.TOKEN_STRING)
                localctx.r = (None if localctx.c2 is None else localctx.c2.text)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Data_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.r = None
            self.dt1 = None # Token

        def TOKEN_INT(self):
            return self.getToken(DatalogParser.TOKEN_INT, 0)

        def getRuleIndex(self):
            return DatalogParser.RULE_data_type




    def data_type(self):

        localctx = DatalogParser.Data_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_data_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 285
            localctx.dt1 = self.match(DatalogParser.TOKEN_INT)
            localctx.r = (None if localctx.dt1 is None else localctx.dt1.text)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





