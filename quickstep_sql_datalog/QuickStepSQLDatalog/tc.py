from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
    config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
Edge = Table('Edge')
Edge.add_attribute('x', 'int')
Edge.add_attribute('y', 'int')
quickstep_shell_instance.create_table(Edge)
# create idb tables
Path = Table('Path')
Path.add_attribute('x', 'int')
Path.add_attribute('y', 'int')
quickstep_shell_instance.create_table(Path)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(Edge, './Input/Edge.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('Path(x, y) :- Edge(x, y).')
time_monitor.update()
# Path(x, y) :- Edge(x, y).
Path_count=quickstep_shell_instance.count_rows('Path')
lpa_logger.info('Number of tuples in Path: ' + str(Path_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
tmp_res_table.add_attribute('y', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select e0.x as x, e0.y as y from Edge e0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x', 'y'], Path, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyzecount Path\n')
quickstep_shell_instance.drop_table('tmp_res_table')
Path_count=quickstep_shell_instance.count_rows('Path')
lpa_logger.info('Number of tuples in Path: ' + str(Path_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# recursive rules evaluation
# Path(x, y) :- Path(x, w), Edge(w, y).
lpa_logger.info('Start creating delta, prev, common-delta tables for semi-naive evaluation')
time_monitor.update()
Path_Delta = Table('Path_Delta0')
Path_Delta.add_attribute('x', 'int')
Path_Delta.add_attribute('y', 'int')
quickstep_shell_instance.create_table(Path_Delta)
Path_Common_Delta = Table('Path_Common_Delta')
Path_Common_Delta.add_attribute('x', 'int')
Path_Common_Delta.add_attribute('y', 'int')
quickstep_shell_instance.create_table(Path_Common_Delta)
quickstep_shell_instance.sql_command('\\analyze \n')
quickstep_shell_instance.load_data_from_table(Path, ['x', 'y'], Path_Delta, ['x', 'y'])
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
is_delta_empty = quickstep_shell_instance.is_table_empty('Path_Delta0')
iter_num = 0
while(not is_delta_empty):
    lpa_logger.info('#####Start Iteration ' + str(iter_num + 1) + '#####')
    time_monitor.update()
    cur_iter_start_time = time_monitor.local_start_time
    Path_delta_name = 'Path_Delta' + str(iter_num)

    iter_num += 1

    new_Path_delta_name = 'Path_Delta' + str(iter_num)

    Path_Delta.rename(new_Path_delta_name)

    quickstep_shell_instance.create_table(Path_Delta)

    # Path
    Path_mDelta = Table('Path_mDelta')
    Path_mDelta.add_attribute('x', 'int')
    Path_mDelta.add_attribute('y', 'int')
    quickstep_shell_instance.create_table(Path_mDelta)

    lpa_logger.info('Evaluate Path')
    # Path(x, y) :- Path(x, w), Edge(w, y).
    Path_tmp_mDelta = Table('Path_tmp_mDelta')
    Path_tmp_mDelta.add_attribute('x', 'int')
    Path_tmp_mDelta.add_attribute('y', 'int')
    quickstep_shell_instance.create_table(Path_tmp_mDelta)
    quickstep_shell_instance.sql_command('insert into Path_tmp_mDelta select * from (' + 'select p0.x as x, e1.y as y from ' + 'Path_Delta' + str(iter_num-1) + ' p0, Edge e1 where p0.y = e1.x' + ') t;')
    quickstep_shell_instance.sql_command('\\analyzecount Path_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into Path_mDelta select * from Path_tmp_mDelta group by x, y;')
    quickstep_shell_instance.drop_table('Path_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount Path_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()


    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into Path_Common_Delta select Path_mDelta.x, Path_mDelta.y from Path_mDelta, Path where Path_mDelta.x = Path.x and Path_mDelta.y = Path.y;')
    quickstep_shell_instance.sql_command('\\analyzecount Path_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')



    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'Path_Delta' + str(iter_num) + ' select * from Path_mDelta where not exists (select Path_Common_Delta.x, Path_Common_Delta.y from Path_Common_Delta where Path_Common_Delta.x = Path_mDelta.x and Path_Common_Delta.y = Path_mDelta.y);')
    quickstep_shell_instance.sql_command('\\analyze Path_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')



    # drop mDelta table
    quickstep_shell_instance.drop_table('Path_mDelta')
    quickstep_shell_instance.drop_table('Path_Common_Delta')
    quickstep_shell_instance.create_table(Path_Common_Delta)
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update Path
    quickstep_shell_instance.sql_command('insert into Path select * from ' + new_Path_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount Path\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # drop all old intermediate tables 
    quickstep_shell_instance.drop_table(Path_delta_name)

    is_delta_empty = quickstep_shell_instance.is_table_empty(new_Path_delta_name)
    Path_Delta_count=quickstep_shell_instance.count_rows(new_Path_delta_name)
    lpa_logger.info('Number of tuples in Path_Delta: ' + str(Path_Delta_count))
    Path_count=quickstep_shell_instance.count_rows('Path')
    lpa_logger.info('Number of tuples in Path: ' + str(Path_count))
    time_monitor.update()
    cur_iter_end_time = time_monitor.local_start_time
    lpa_logger.info('Iteration Time: ' + str(cur_iter_end_time - cur_iter_start_time) + 's')
    lpa_logger.info('')
    lpa_logger.info('')

# clear all prev tables

# clear all  deltas
quickstep_shell_instance.drop_table('Path_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('Path_Common_Delta')
lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
