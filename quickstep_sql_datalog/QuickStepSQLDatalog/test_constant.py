from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
	config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
AddressOf = Table('AddressOf')
AddressOf.add_attribute('y', 'int')
AddressOf.add_attribute('x', 'int')
quickstep_shell_instance.create_table(AddressOf)
# create idb tables
Test0 = Table('Test0')
Test0.add_attribute('w', 'int')
Test0.add_attribute('y', 'int')
Test0.add_attribute('x', 'int')
quickstep_shell_instance.create_table(Test0)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(AddressOf, './Input/AddressOf.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('Test0(2, x, x) :- AddressOf(x, 3).')
time_monitor.update()
# Test0(2, x, x) :- AddressOf(x, 3).
Test0_count=quickstep_shell_instance.count_rows('Test0')
lpa_logger.info('Number of tuples in Test0: ' + str(Test0_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('w', 'int')
tmp_res_table.add_attribute('y', 'int')
tmp_res_table.add_attribute('x', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select 2 as w, a0.y as y, a0.y as x from AddressOf a0 where a0.x = 3;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['w', 'y', 'x'], Test0, ['w', 'y', 'x'])
quickstep_shell_instance.sql_command('\\analyzecount Test0\n')
quickstep_shell_instance.drop_table('tmp_res_table')
Test0_count=quickstep_shell_instance.count_rows('Test0')
lpa_logger.info('Number of tuples in Test0: ' + str(Test0_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
