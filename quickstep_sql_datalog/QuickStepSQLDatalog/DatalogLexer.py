# Generated from Datalog.g4 by ANTLR 4.7
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys




def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2!")
        buf.write("\u00bb\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2")
        buf.write("\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\5\5_\n\5\3\5\6\5b\n\5\r")
        buf.write("\5\16\5c\3\6\3\6\6\6h\n\6\r\6\16\6i\3\6\3\6\3\7\3\7\3")
        buf.write("\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n")
        buf.write("\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\7\f\u0086\n\f\f")
        buf.write("\f\16\f\u0089\13\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\20")
        buf.write("\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25")
        buf.write("\3\26\3\26\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3\32\3\32")
        buf.write("\3\32\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\36\3\36\3\37")
        buf.write("\3\37\3 \6 \u00b6\n \r \16 \u00b7\3 \3 \2\2!\3\3\5\4\7")
        buf.write("\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17")
        buf.write("\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63")
        buf.write("\33\65\34\67\359\36;\37= ?!\3\2\n\4\2--//\3\2\62;\5\2")
        buf.write("\62;C\\c|\4\2KKkk\4\2PPpp\4\2VVvv\4\2C\\c|\5\2\13\f\17")
        buf.write("\17\"\"\2\u00bf\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2")
        buf.write("\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2")
        buf.write("\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2")
        buf.write("\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2")
        buf.write("\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2")
        buf.write("\2\2\2?\3\2\2\2\3A\3\2\2\2\5J\3\2\2\2\7S\3\2\2\2\t^\3")
        buf.write("\2\2\2\13e\3\2\2\2\rm\3\2\2\2\17q\3\2\2\2\21u\3\2\2\2")
        buf.write("\23y\3\2\2\2\25}\3\2\2\2\27\u0083\3\2\2\2\31\u008a\3\2")
        buf.write("\2\2\33\u008d\3\2\2\2\35\u008f\3\2\2\2\37\u0091\3\2\2")
        buf.write("\2!\u0093\3\2\2\2#\u0095\3\2\2\2%\u0097\3\2\2\2\'\u0099")
        buf.write("\3\2\2\2)\u009b\3\2\2\2+\u009d\3\2\2\2-\u009f\3\2\2\2")
        buf.write("/\u00a1\3\2\2\2\61\u00a4\3\2\2\2\63\u00a6\3\2\2\2\65\u00a9")
        buf.write("\3\2\2\2\67\u00ab\3\2\2\29\u00ae\3\2\2\2;\u00b0\3\2\2")
        buf.write("\2=\u00b2\3\2\2\2?\u00b5\3\2\2\2AB\7G\2\2BC\7F\2\2CD\7")
        buf.write("D\2\2DE\7a\2\2EF\7F\2\2FG\7G\2\2GH\7E\2\2HI\7N\2\2I\4")
        buf.write("\3\2\2\2JK\7K\2\2KL\7F\2\2LM\7D\2\2MN\7a\2\2NO\7F\2\2")
        buf.write("OP\7G\2\2PQ\7E\2\2QR\7N\2\2R\6\3\2\2\2ST\7T\2\2TU\7W\2")
        buf.write("\2UV\7N\2\2VW\7G\2\2WX\7a\2\2XY\7F\2\2YZ\7G\2\2Z[\7E\2")
        buf.write("\2[\\\7N\2\2\\\b\3\2\2\2]_\t\2\2\2^]\3\2\2\2^_\3\2\2\2")
        buf.write("_a\3\2\2\2`b\t\3\2\2a`\3\2\2\2bc\3\2\2\2ca\3\2\2\2cd\3")
        buf.write("\2\2\2d\n\3\2\2\2eg\7)\2\2fh\t\4\2\2gf\3\2\2\2hi\3\2\2")
        buf.write("\2ig\3\2\2\2ij\3\2\2\2jk\3\2\2\2kl\7)\2\2l\f\3\2\2\2m")
        buf.write("n\t\5\2\2no\t\6\2\2op\t\7\2\2p\16\3\2\2\2qr\7O\2\2rs\7")
        buf.write("K\2\2st\7P\2\2t\20\3\2\2\2uv\7O\2\2vw\7C\2\2wx\7Z\2\2")
        buf.write("x\22\3\2\2\2yz\7U\2\2z{\7W\2\2{|\7O\2\2|\24\3\2\2\2}~")
        buf.write("\7E\2\2~\177\7Q\2\2\177\u0080\7W\2\2\u0080\u0081\7P\2")
        buf.write("\2\u0081\u0082\7V\2\2\u0082\26\3\2\2\2\u0083\u0087\t\b")
        buf.write("\2\2\u0084\u0086\t\4\2\2\u0085\u0084\3\2\2\2\u0086\u0089")
        buf.write("\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088")
        buf.write("\30\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008b\7<\2\2\u008b")
        buf.write("\u008c\7/\2\2\u008c\32\3\2\2\2\u008d\u008e\7a\2\2\u008e")
        buf.write("\34\3\2\2\2\u008f\u0090\7.\2\2\u0090\36\3\2\2\2\u0091")
        buf.write("\u0092\7=\2\2\u0092 \3\2\2\2\u0093\u0094\7<\2\2\u0094")
        buf.write("\"\3\2\2\2\u0095\u0096\7\60\2\2\u0096$\3\2\2\2\u0097\u0098")
        buf.write("\7-\2\2\u0098&\3\2\2\2\u0099\u009a\7/\2\2\u009a(\3\2\2")
        buf.write("\2\u009b\u009c\7,\2\2\u009c*\3\2\2\2\u009d\u009e\7\61")
        buf.write("\2\2\u009e,\3\2\2\2\u009f\u00a0\7#\2\2\u00a0.\3\2\2\2")
        buf.write("\u00a1\u00a2\7#\2\2\u00a2\u00a3\7?\2\2\u00a3\60\3\2\2")
        buf.write("\2\u00a4\u00a5\7?\2\2\u00a5\62\3\2\2\2\u00a6\u00a7\7@")
        buf.write("\2\2\u00a7\u00a8\7?\2\2\u00a8\64\3\2\2\2\u00a9\u00aa\7")
        buf.write("@\2\2\u00aa\66\3\2\2\2\u00ab\u00ac\7>\2\2\u00ac\u00ad")
        buf.write("\7?\2\2\u00ad8\3\2\2\2\u00ae\u00af\7>\2\2\u00af:\3\2\2")
        buf.write("\2\u00b0\u00b1\7*\2\2\u00b1<\3\2\2\2\u00b2\u00b3\7+\2")
        buf.write("\2\u00b3>\3\2\2\2\u00b4\u00b6\t\t\2\2\u00b5\u00b4\3\2")
        buf.write("\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8")
        buf.write("\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00ba\b \2\2\u00ba")
        buf.write("@\3\2\2\2\n\2^cgi\u0085\u0087\u00b7\3\b\2\2")
        return buf.getvalue()


class DatalogLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    TOKEN_EDB = 1
    TOKEN_IDB = 2
    TOKEN_RULE = 3
    TOKEN_INTEGER = 4
    TOKEN_STRING = 5
    TOKEN_INT = 6
    TOKEN_MIN = 7
    TOKEN_MAX = 8
    TOKEN_SUM = 9
    TOKEN_COUNT = 10
    TOKEN_ID = 11
    TOKEN_BODY_HEAD_SEP = 12
    TOKEN_ANY = 13
    TOKEN_COMMA = 14
    TOKEN_SEMICOLON = 15
    TOKEN_COLON = 16
    TOKEN_DOT = 17
    TOKEN_PLUS = 18
    TOKEN_MINUS = 19
    TOKEN_MULT = 20
    TOKEN_DIV = 21
    TOKEN_NOT = 22
    TOKEN_NOT_EQUALS = 23
    TOKEN_EQUALS = 24
    TOKEN_GREATER_EQUAL_THAN = 25
    TOKEN_GREATER_THAN = 26
    TOKEN_LESS_EQUAL_THAN = 27
    TOKEN_LESS_THAN = 28
    TOKEN_LEFT_PAREN = 29
    TOKEN_RIGHT_PAREN = 30
    TOKEN_WS = 31

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'EDB_DECL'", "'IDB_DECL'", "'RULE_DECL'", "'MIN'", "'MAX'", 
            "'SUM'", "'COUNT'", "':-'", "'_'", "','", "';'", "':'", "'.'", 
            "'+'", "'-'", "'*'", "'/'", "'!'", "'!='", "'='", "'>='", "'>'", 
            "'<='", "'<'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", "TOKEN_INTEGER", "TOKEN_STRING", 
            "TOKEN_INT", "TOKEN_MIN", "TOKEN_MAX", "TOKEN_SUM", "TOKEN_COUNT", 
            "TOKEN_ID", "TOKEN_BODY_HEAD_SEP", "TOKEN_ANY", "TOKEN_COMMA", 
            "TOKEN_SEMICOLON", "TOKEN_COLON", "TOKEN_DOT", "TOKEN_PLUS", 
            "TOKEN_MINUS", "TOKEN_MULT", "TOKEN_DIV", "TOKEN_NOT", "TOKEN_NOT_EQUALS", 
            "TOKEN_EQUALS", "TOKEN_GREATER_EQUAL_THAN", "TOKEN_GREATER_THAN", 
            "TOKEN_LESS_EQUAL_THAN", "TOKEN_LESS_THAN", "TOKEN_LEFT_PAREN", 
            "TOKEN_RIGHT_PAREN", "TOKEN_WS" ]

    ruleNames = [ "TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", "TOKEN_INTEGER", 
                  "TOKEN_STRING", "TOKEN_INT", "TOKEN_MIN", "TOKEN_MAX", 
                  "TOKEN_SUM", "TOKEN_COUNT", "TOKEN_ID", "TOKEN_BODY_HEAD_SEP", 
                  "TOKEN_ANY", "TOKEN_COMMA", "TOKEN_SEMICOLON", "TOKEN_COLON", 
                  "TOKEN_DOT", "TOKEN_PLUS", "TOKEN_MINUS", "TOKEN_MULT", 
                  "TOKEN_DIV", "TOKEN_NOT", "TOKEN_NOT_EQUALS", "TOKEN_EQUALS", 
                  "TOKEN_GREATER_EQUAL_THAN", "TOKEN_GREATER_THAN", "TOKEN_LESS_EQUAL_THAN", 
                  "TOKEN_LESS_THAN", "TOKEN_LEFT_PAREN", "TOKEN_RIGHT_PAREN", 
                  "TOKEN_WS" ]

    grammarFileName = "Datalog.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


