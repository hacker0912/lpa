import sys
import collections
from copy import deepcopy
import DatalogProgram
import ruleAnalyzer
import queryGenerator

LOG_ON = True
tab_space = '    '


def code_gen_write_code(file, code_str, if_write=True):
    if if_write:
        file.write(code_str)


def code_gen_log_info(info_str, var_info_str='', if_write=True, indent=''):
    if_write = LOG_ON
    log_info_str = ''
    if if_write:
        log_info_str += indent + 'lpa_logger.info('
        if len(var_info_str) == 0:
            log_info_str += '\'' + info_str + '\''
        else:
            log_info_str += var_info_str
        log_info_str += ')\n'
    return log_info_str


def code_gen_log_info_time(time_descrip_var='Time', time_var='time_monitor.local_elapse_time()', if_write=True,
                           indent=''):
    if_write = LOG_ON
    log_info_str = ''
    if if_write:
        log_info_str += indent + 'lpa_logger.info(' + '\'' + time_descrip_var + ': ' + '\'' + ' + '
        log_info_str += 'str(' + time_var + ') + '
        log_info_str += '\'' + 's' + '\'' + ')\n'
    return log_info_str


def code_gen_log_info_update_time(if_write=True, indent=''):
    if_write = LOG_ON
    update_time_str = ''
    if if_write:
        update_time_str = indent + 'time_monitor.update()\n'
    return update_time_str


def code_gen_log_info_get_cur_time(time_var_name, if_write=True, indent=''):
    if_write = LOG_ON
    cur_time_str = ''
    if if_write:
        cur_time_str = indent + time_var_name + ' = ' + 'time_monitor.local_start_time\n'
    return cur_time_str


def code_gen_log_info_count_row(table_name='', var_table_name='', if_write=True, indent=''):
    if_write = LOG_ON
    log_info_str = ''
    if if_write:
        log_info_str += indent + table_name + '_count' + '=' + 'quickstep_shell_instance.count_rows('
        if len(var_table_name) != 0:
            log_info_str += var_table_name
        else:
            log_info_str += '\'' + table_name + '\''
        log_info_str += ')' + '\n'
        log_info_str += indent + 'lpa_logger.info('
        log_info_str += '\'' + 'Number of tuples in ' + table_name + ': ' + '\'' + ' + ' + 'str(' + table_name + '_count' + ')'
        log_info_str += ')' + '\n'
    return log_info_str


def is_trivial_scc(scc, dependency_graph):
    """
        A given scc is called trivial if it only contains a single rule and there is no
        self-loop on that rule in the dependency graph - such scc evaluation requires no iteration
        """
    if len(scc) == 1:
        rule_index = scc[0]
        dependent_rules = dependency_graph[rule_index]
        for dependent_rule_index in dependent_rules:
            if dependent_rule_index == rule_index:
                return False
        return True

    return False


def code_gen_create_table(relation, relation_var_name='', indent=''):
    """
    Given the data structure storing the information of the relation, generate the corresponding
    code to create table in quickstep
    """
    create_table_code = ''
    var_table_name = relation['name']
    if len(relation_var_name) > 0:
        var_table_name = relation_var_name

    table_name = relation['name']
    attributes = relation['attributes']

    create_table_code += indent + var_table_name + ' = ' + 'Table' + '(' + '\'' + table_name + '\'' + ')' + '\n'
    for attribute in attributes:
        create_table_code += indent + var_table_name + '.add_attribute' + '(' + \
                             '\'' + attribute.name + '\'' + ', ' + \
                             '\'' + attribute.type + '\'' + ')' + '\n'
    create_table_code += indent + 'quickstep_shell_instance.create_table' + '(' + var_table_name + ')' + '\n'
    return create_table_code


def code_gen_populate_data_into_edb(relation):
    """
    Given the data structure storing the information of the relation, load the data into the
    created table from the file given under the specified path ./Input/relation.tbl
    """
    table_name = relation['name']
    populate_data_code = 'quickstep_shell_instance.load_data_from_file'
    populate_data_code += '(' + table_name + ', ' + '\'' + './Input/' + table_name + '.tbl' + '\'' + ', ' + '\'' + ',' + '\'' + ')' + '\n'
    return populate_data_code


def code_gen_analyze_table(table_name, count_only=False, range_only=False, indent=''):
    """
    Given the data structure storing the information of the relation, analyze the table in order
    to construct statistic for quickstep to choose better query execution plan later
    """
    if count_only:
        analyze_code = indent + 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyzecount ' + table_name + '\\n' + '\'' + ')' + '\n'
    elif range_only:
        analyze_code = indent + 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyzerange ' + table_name + '\\n' + '\'' + ')' + '\n'
    else:
        analyze_code = indent + 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyze ' + table_name + '\\n' + '\'' + ')' + '\n'

    return analyze_code


def code_gen_analyze_all(count_only=False, range_only=False):
    """
    Analyze all the tables at once
    """
    if count_only:
        analyze_code = 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyzecount ' + '\\n' + '\'' + ')' + '\n'
    elif range_only:
        analyze_code = 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyzerange ' + '\\n' + '\'' + ')' + '\n'
    else:
        analyze_code = 'quickstep_shell_instance.sql_command' + '(' + '\'' + '\\\\' + 'analyze ' + '\\n' + '\'' + ')' + '\n'

    return analyze_code


def code_gen_load_data_from_table(src_table, dest_table, var_src_table='', var_dest_table='', indent=''):
    """
    Generate the code to insert all the tuples from source table into the destination tuples with deduplication
        Args:
            src_table: relation data structure
            dest_table: relation data structure
    """
    src_table_attributes = src_table['attributes']
    dest_table_attributes = dest_table['attributes']

    load_table_code = indent + 'quickstep_shell_instance.load_data_from_table('
    if len(var_src_table) > 0:
        load_table_code += var_src_table + ', ' + '['
    else:
        load_table_code += src_table['name'] + ', ' + '['

    for attribute in src_table_attributes:
        load_table_code += '\'' + attribute.name + '\'' + ', '
    load_table_code = load_table_code[:len(load_table_code) - 2]
    load_table_code += ']' + ', '

    if len(var_dest_table) > 0:
        load_table_code += var_dest_table + ', ' + '['
    else:
        load_table_code += dest_table['name'] + ', ' + '['

    for attribute in dest_table_attributes:
        load_table_code += '\'' + attribute.name + '\'' + ', '
    load_table_code = load_table_code[:len(load_table_code) - 2]
    load_table_code += ']' + ')' + '\n'

    return load_table_code


def code_gen_drop_table(relation_name, indent=''):
    drop_table_code = indent + 'quickstep_shell_instance.drop_table' + '(' + '\'' + relation_name + '\'' + ')' + '\n'
    return drop_table_code


def code_gen_copy_table(original_relation, copy_relation_name='', indent=''):
    """
    Generate the code to copy original_relation into a new relation named "original_relation_copy" if no specific name is given
    for the copy
    """
    copy_table_code = ''
    if len(copy_relation_name) > 0:
        copy_name = copy_relation_name
    else:
        copy_name = original_relation['name'] + '_copy'

    copy_relation = deepcopy(original_relation)
    copy_relation['name'] = copy_name

    copy_table_code += code_gen_create_table(copy_relation, indent=indent)
    copy_table_code += code_gen_load_data_from_table(original_relation, copy_relation, indent=indent)

    return copy_table_code


def code_gen_intersection(relation1, relation2, indent='', sub_query=False):
    """
    Generate the code to select common tuples (intersection) from the two given tables
    """
    intersection_str = indent
    intersection_str += 'select '
    relation1_name = relation1['name']
    relation2_name = relation2['name']
    relation1_attributes = relation1['attributes']
    relation2_attributes = relation2['attributes']
    relation1_attri_num = len(relation1_attributes)
    relation2_attri_num = len(relation2_attributes)
    if relation1_attri_num != relation2_attri_num:
        raise Exception('code_gen_intersection: Inconsistent attribute number')
    for i in range(relation1_attri_num):
        relation1_attri = relation1_attributes[i]
        relation2_attri = relation2_attributes[i]
        if relation1_attri.name != relation2_attri.name:
            raise Exception('code_gen_intersection: Inconsistent attribute names')
        if relation1_attri.type != relation2_attri.type:
            raise Exception('code_gen_intersection: Inconsistent attribute types')
        attri_name = relation1_attri.name
        intersection_str += relation1_name + '.' + attri_name
        if i != relation1_attri_num - 1:
            intersection_str += ', '

    if sub_query:
        intersection_str += ' from ' + relation1_name
    else:
        intersection_str += ' from ' + relation1_name + ', ' + relation2_name
    intersection_str += ' where '
    for i in range(relation1_attri_num):
        intersection_str += relation1_name + '.' + relation1_attributes[i].name + ' = ' + relation2_name + '.' + \
                            relation2_attributes[i].name
        if i != relation1_attri_num - 1:
            intersection_str += ' and '

    return intersection_str


def code_gen_non_recursive_rule_evaluation(datalog_rule, relation_def_map):
    """
        Example:
        Schema: A(a,b), B(a,b), C(a,b), D(a,b)
        Rule: A(x,y) :- B(z,x), C(z,w), D(w,y)
        1. Map the attributes in the head to the attributes in the body -> attributes_map (for 'select' and 'from')
            key-value pair:  <head_atom_arg_index, [body_atom_index, body_atom_arg_index]>
        2. Construct the map summarizing the join operations
            key-value pairs: <body_atom_arg_index, [<body_atom_index, body_atom_arg_indices>]>
        3. Currently, self-join is resolved by 'fake filter' (only consider int types)
    """

    # map attributes to be projected
    head = datalog_rule['head']
    head_name = head['name']
    body = datalog_rule['body']

    if body is None:
        return 'quickstep_shell_instance.sql_command(' + '\'' + \
               queryGenerator.generate_insertion_str(head) + ';' + \
               '\'' + ')' + '\n'

    original_body_atom_list = body['atoms']
    body_atom_list = deepcopy(body['atoms'])
    negation_atom_list = body['negations']

    start_log_str = code_gen_log_info_count_row(table_name=head_name, if_write=LOG_ON)

    # select_info = [attributes_map, attributes_type_map, aggregation_map]
    select_info = ruleAnalyzer.extract_selection_info(datalog_rule)

    # join_info = [join_map, self_join_map]
    join_info = ruleAnalyzer.extract_join_info(datalog_rule)

    # negation_map
    negation_info = ruleAnalyzer.extract_negation_map(body)

    # comparison_map
    comparison_map = ruleAnalyzer.extract_comparison_map(body, body_atom_list)

    # constant constraint map
    constant_constraint_map = ruleAnalyzer.extract_constant_constraint_map(body)

    body_atom_alias_list = ruleAnalyzer.build_atom_aliases(body_atom_list)

    negation_atom_alias_list = ruleAnalyzer.build_negation_atom_aliases(negation_atom_list)

    # select
    select_str = queryGenerator.generate_select(datalog_rule, select_info,
                                                relation_def_map, body_atom_alias_list)

    # from
    from_str = queryGenerator.generate_from(body_atom_list, body_atom_alias_list)

    # where::join
    join_str = queryGenerator.generate_join_str(join_info, original_body_atom_list,
                                                body_atom_alias_list, relation_def_map)

    # where::comparison
    compare_str = queryGenerator.generate_compare_str(comparison_map, original_body_atom_list,
                                                      body_atom_alias_list, relation_def_map)

    # where::constant_constraint
    constant_constraint_str = queryGenerator.generate_constant_constraint_str(constant_constraint_map, body,
                                                                              body_atom_alias_list, relation_def_map)

    # where::negation
    negation_str = queryGenerator.generate_negation_str(negation_info, original_body_atom_list, negation_atom_list,
                                                        body_atom_alias_list, negation_atom_alias_list,
                                                        relation_def_map)

    non_recursive_rule_eval_str = select_str + ' ' + \
                                  from_str

    if len(join_str) > 0 or len(compare_str) or len(constant_constraint_str) > 0:
        non_recursive_rule_eval_str += ' ' + \
                                       'where' + ' '

        condition = False
        if len(join_str) > 0:
            non_recursive_rule_eval_str += join_str
            condition = True
        if len(compare_str) > 0:
            if condition:
                non_recursive_rule_eval_str += ' AND '
            non_recursive_rule_eval_str += compare_str
            condition = True
        if len(constant_constraint_str) > 0:
            if condition:
                non_recursive_rule_eval_str += ' AND '
            non_recursive_rule_eval_str += constant_constraint_str
            condition = True
        if len(negation_str) > 0:
            if condition:
                non_recursive_rule_eval_str += ' AND '
            non_recursive_rule_eval_str += negation_str

    # aggregation (group by)
    aggregation_map = select_info[2]
    if len(aggregation_map) > 0:
        group_by_str = queryGenerator.generate_group_by_str(relation_def_map[head_name][0]['attributes'],
                                                            aggregation_map)
        non_recursive_rule_eval_str += ' ' + group_by_str

    non_recursive_rule_eval_str += ';'

    # create tmp table to store the evaluation results
    head_relation_name = head['name']
    head_relation = relation_def_map[head_relation_name][0]
    tmp_relation = deepcopy(relation_def_map[head_relation_name][0])
    tmp_relation['name'] = 'tmp_res_table'
    create_tmp_table_str = code_gen_create_table(tmp_relation)

    # insert the evaluation results into tmp table
    create_tmp_table_str += 'quickstep_shell_instance.sql_command(' + '\'' + 'insert into tmp_res_table ' + non_recursive_rule_eval_str + '\'' + ')' + '\n'
    # load data from tmp table into the table corresponding to the head atom
    load_data_from_table_code = code_gen_load_data_from_table(tmp_relation, head_relation)
    load_data_from_table_code += code_gen_analyze_table(head_relation['name'], count_only=True)

    # drop tmp table
    drop_tmp_table_code = code_gen_drop_table('tmp_res_table')

    end_log_str = code_gen_log_info_count_row(table_name=head_name, if_write=LOG_ON)

    eval_str = start_log_str + create_tmp_table_str + load_data_from_table_code + drop_tmp_table_code + end_log_str

    return eval_str


def code_gen_recursive_rule_evaluation(datalog_rule, relation_def_map, eval_idbs):
    """
    Similar to the function 'code_gen_non_recursive_rule_evaluation'. 'Delta' relation is considered
    during the join mapping stage and rule evaluation
    """

    # map attributes to be projected
    head = datalog_rule['head']
    head_name = head['name']
    body = datalog_rule['body']
    original_body_atom_list = body['atoms']
    body_atom_list = deepcopy(body['atoms'])

    # recursive_rule_eval_strs: one recursive rule could be evaluated by
    # 'multiple sub-queries' (delta & non-delta combination)
    recursive_rule_eval_strs = []

    # select_info = [attributes_map, attributes_type_map, aggregation_map]
    select_info = ruleAnalyzer.extract_selection_info(datalog_rule)

    # join_info = [join_map, self_join_map]
    join_info = ruleAnalyzer.extract_join_info(datalog_rule)

    # comparison_map
    comparison_map = ruleAnalyzer.extract_comparison_map(body, body_atom_list)

    # constant constraint map
    constant_constraint_map = ruleAnalyzer.extract_constant_constraint_map(body)

    body_atom_alias_list = ruleAnalyzer.build_atom_aliases(body_atom_list)

    body_atom_eval_names, idb_num = ruleAnalyzer.build_recursive_atom_aliases(body_atom_list, eval_idbs)

    atom_eval_name_list = ruleAnalyzer.build_recursive_atom_alias_combinations(body_atom_list, body_atom_eval_names,
                                                                               eval_idbs, idb_num)

    # select
    select_str = queryGenerator.generate_select(datalog_rule, select_info, relation_def_map, body_atom_alias_list)

    # from
    from_strs = queryGenerator.generate_from_recursive(body_atom_alias_list, atom_eval_name_list)

    # where::join
    join_str = queryGenerator.generate_join_str(join_info, original_body_atom_list,
                                                body_atom_alias_list, relation_def_map)

    # where::comparison
    compare_str = queryGenerator.generate_compare_str(comparison_map, original_body_atom_list,
                                                      body_atom_alias_list, relation_def_map)

    # where::constant_constraint
    constant_constraint_str = queryGenerator.generate_constant_constraint_str(constant_constraint_map, body,
                                                                              body_atom_alias_list, relation_def_map)

    recursive_rule_num = len(from_strs)

    for rule_index in range(recursive_rule_num):
        recursive_rule_eval_strs.append('\'' + select_str + ' ' + from_strs[rule_index])

    if len(join_str) > 0 or len(compare_str) > 0 or len(constant_constraint_str) > 0:

        for rule_index in range(recursive_rule_num):
            recursive_rule_eval_strs[rule_index] += ' ' + 'where' + ' '

            condition = False
            if len(join_str) > 0:
                recursive_rule_eval_strs[rule_index] += join_str
                condition = True
            if len(compare_str) > 0:
                if condition:
                    recursive_rule_eval_strs[rule_index] += ' AND '
                recursive_rule_eval_strs[rule_index] += compare_str
                condition = True
            if len(constant_constraint_str) > 0:
                if condition:
                    recursive_rule_eval_strs[rule_index] += ' AND '
                recursive_rule_eval_strs[rule_index] += constant_constraint_str

    # aggregation (group by)
    aggregation_map = select_info[2]
    if len(aggregation_map) > 0:

        group_by_str = queryGenerator.generate_group_by_str(relation_def_map[head_name][0]['attributes'],
                                                            aggregation_map)

        for rule_index in range(recursive_rule_num):
            recursive_rule_eval_strs[rule_index] += ' ' + group_by_str

    for rule_index in range(recursive_rule_num):
        recursive_rule_eval_strs[rule_index] += '\''

    print('size of total evaluation queries: ' + str(len(recursive_rule_eval_strs)))
    return recursive_rule_eval_strs


def code_gen_initialize_delta_tables(relation_set, relation_def_map):
    """
    generate strings to create 'detla0' table and 'common-delta'(for set-difference) for each relation to be evaluated
    """
    create_delta_str = ''
    create_delta_str += code_gen_log_info('Start creating delta, prev, common-delta tables for semi-naive evaluation',
                                          if_write=LOG_ON)
    create_delta_str += code_gen_log_info_update_time(if_write=LOG_ON)
    for relation_name in relation_set:
        relation = relation_def_map[relation_name][0]
        delta_relation = deepcopy(relation)
        delta_relation['name'] = relation['name'] + '_Delta0'
        create_delta_str += code_gen_create_table(delta_relation, relation_var_name=relation['name'] + '_Delta')
        create_delta_str += code_gen_analyze_table(delta_relation['name'], count_only=True)
        delta_relation['name'] = relation['name'] + '_Common_Delta'
        create_delta_str += code_gen_create_table(delta_relation)
        create_delta_str += code_gen_analyze_table(delta_relation['name'], count_only=True)

    return create_delta_str


def code_gen_initialize_prev_tables(recursive_rules, relation_set, relation_def_map):
    # if the rule body contains 'more than one' (>=2) idb atoms (count repeated ones) in the current scc,
    # then 'prev' table needs to be created for these idb atoms for non-linear rule evaluation
    pre_table_set = set([])
    rule_num = len(recursive_rules)
    for rule_index in range(rule_num):
        idb_atom_counter = 0
        rule = recursive_rules[rule_index]
        rule_body_atoms = rule['body']['atoms']
        idb_atom_candidates = []
        for atom in rule_body_atoms:
            if atom['name'] in relation_set:
                idb_atom_candidates.append(atom['name'])
                idb_atom_counter += 1
        if idb_atom_counter >= 2:
            for idb_atom_candidate in idb_atom_candidates:
                pre_table_set.add(idb_atom_candidate)

    # create 'prev' tables
    create_prev_str = ''
    for relation_name in pre_table_set:
        relation = relation_def_map[relation_name][0]
        prev_relation = deepcopy(relation)
        prev_relation['name'] = relation['name'] + 'Prev'
        create_prev_str += code_gen_create_table(prev_relation)
        create_prev_str += code_gen_analyze_table(prev_relation['name'], count_only=True)

    return create_prev_str, pre_table_set


def code_gen_load_data_into_delta(relation_set, relation_def_map):
    """
    Generate string to load data from previous evaluated results into 'deltas'
    """
    load_prev_str = ''
    for table in relation_set:
        src_relation = relation_def_map[table][0]
        delta_relation = deepcopy(src_relation)
        delta_relation['name'] = src_relation['name'] + '_Delta0'
        load_prev_str += code_gen_load_data_from_table(src_relation, delta_relation,
                                                       var_dest_table=src_relation['name'] + '_Delta')
        load_prev_str += code_gen_analyze_table(delta_relation['name'], count_only=True)

    return load_prev_str


def code_gen_set_diff(alpha=1.38, beta=10, prev_mu=5,if_incremental_hash=False):
    """ Returns the string to compute set-difference between two tables

    Generate string to perform sef-difference between table S and R (R - S)

    Parameters used in the cost model:

        Args:
             |S|: the size of the IDB relation
             |R|: the size of the relation storing the evaluated results
             |r|: the size of (S intersect R)
              Cb: building hash table cost per tuple
              Cp: probing hash table cost per tuple
           alpha:  Cb/Cp
            beta: |S|/|R|
              mu: |R|/|r|
            alg1: delta_S = R - S
            alg2: r = S intersect R, delta_S = R - r

        Cost Model based algorithm selection:
            1. Without incremental hash table being available
                Choice of algorithm depends on the *beta* interval:
                    [ 0, alpha/(alpha-1) ]:
                        Choose alg1
                    ( alpha/(alpha-1), 2*alpha/(alpha-1) ):
                        approximate Cost_Diff ~= Cost(alg1) - Cost(alg2) = beta(alpha-1) - (alpha + (prev_mu/alpha))
                            if Cost_Diff < 0:
                                Choose alg1
                            else:
                                Choose alg2
                    [2*alpha/(alpha-1), +infinity]:
                        Choose alg2
            2. With incremental hash table being available
                Choose alg1

        Returns:
    """
    set_diff_str = code_gen_log_info('Compute set-difference',
                                           if_write=LOG_ON, indent=tab_space)
    if not if_incremental_hash:
        if beta < alpha/(alpha-1):

    else:
        print('TODO')



def code_gen_check_empty_delta(relation_set, if_start_recursion=False):
    check_delta_empty_str = 'is_delta_empty = '
    if not if_start_recursion:
        for relation in relation_set:
            check_delta_empty_str += 'quickstep_shell_instance.is_table_empty(' + '\'' + relation + '_Delta0' + '\'' + ')' + ' and '
        check_delta_empty_str = check_delta_empty_str[:len(check_delta_empty_str) - 5] + '\n'
    else:
        print('TODO')

    return check_delta_empty_str


def code_gen_recursive_rules_evaluation(recursive_rules, relation_def_map):
    """
        Args:
            recursive_rules:
            relation_def_map: the map that maps relation name and corresponding relation data structure
    """
    # construct relation set to store all the unique relations (appear in the head of certain rules) to be evaluated
    # rules evaluating the same relation are grouped together and will be evaluated together later
    relation_set = collections.OrderedDict({})
    for rule in recursive_rules:
        eval_relation_name = rule['head']['name']
        if eval_relation_name not in relation_set:
            relation_set[eval_relation_name] = []
        relation_set[eval_relation_name].append(rule)

    # generate string to create 'detla0' table and 'common-delta'(for set-difference) for each relation to be evaluated
    create_delta_str, pre_table_set = code_gen_initialize_delta_tables(relation_set, relation_def_map)

    # generate string to create 'prev' tables for idb atoms in non-linear rule evaluation
    create_prev_str = code_gen_initialize_prev_tables(recursive_rules, relation_set, relation_def_map)

    # generate string to load data from previous evaluated results into 'deltas'
    load_prev_str = code_gen_load_data_into_delta(relation_set, relation_def_map)

    create_prev_str += load_prev_str
    create_prev_str += code_gen_log_info_time(if_write=LOG_ON)

    # generate string to check whether all "delta" relations are empty *before* the recursive evaluation
    check_delta_empty_str = code_gen_check_empty_delta(relation_set)

    # code before the recursive evaluation process in the *current strata*
    before_recursive_eval_str = create_delta_str + create_prev_str + load_prev_str + check_delta_empty_str

    # generate iterative evaluation (semi-naive) code
    iter_eval_str = 'iter_num = 0\n'
    iter_eval_str += 'while(not is_delta_empty):\n'
    iter_num_str = '\'' + '#####Start Iteration ' + '\'' + ' + ' + 'str(iter_num + 1)' + ' + ' + '\'' + '#####' + '\''
    iter_eval_str += code_gen_log_info('', var_info_str=iter_num_str, if_write=LOG_ON, indent=tab_space)
    iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
    iter_eval_str += code_gen_log_info_get_cur_time('cur_iter_start_time', if_write=LOG_ON, indent=tab_space)
    # generate code store the current delta table names in the corresponding variables
    for table in relation_set:
        iter_eval_str += tab_space + table + '_delta_name = ' + '\'' + table + '_Delta' + '\'' + ' + str(iter_num)\n'

    iter_eval_str += '\n'
    iter_eval_str += tab_space + 'iter_num += 1\n'

    # generate code to store the delta table names to be used in the next iteration
    # (to be evaluated in the current iteration)
    iter_eval_str += '\n'
    for table in relation_set:
        iter_eval_str += tab_space + 'new_' + table + '_delta_name = ' + '\'' + table + '_Delta' + '\'' + ' + str(iter_num)\n'

    # generate code to rename the delta table names in corresponding relation data structures
    iter_eval_str += '\n'
    for table in relation_set:
        iter_eval_str += tab_space + table + '_Delta' + '.rename(' + 'new_' + table + '_delta_name)' + '\n'

    # generate code to create delta tables
    iter_eval_str += '\n'
    for table in relation_set:
        iter_eval_str += tab_space + 'quickstep_shell_instance.create_table(' + table + '_Delta' + ')\n'

    # evaluate rules grouped by 'evaluated relation'
    for idb in relation_set:
        # generate code to create mDelta (results evaluated without duplication)
        original_relation = relation_def_map[idb][0]
        iter_eval_str += '\n'
        iter_eval_str += tab_space + '# ' + original_relation['name'] + '\n'
        eval_relation_attributes = original_relation['attributes']
        mDelta_relation = deepcopy(original_relation)
        mDelta_relation['name'] = original_relation['name'] + '_mDelta'
        iter_eval_str += code_gen_create_table(mDelta_relation, indent=tab_space)
        iter_eval_str += '\n'
        # if there is more than one rule to evaluate the idb, multiple mDelta will be generated (merged into one)
        eval_rules = relation_set[idb]
        eval_rule_num = len(eval_rules)
        iter_eval_str += code_gen_log_info('Evaluate ' + original_relation['name'], if_write=LOG_ON, indent=tab_space)
        sub_query_list = []
        # generate code for actual evaluation
        for eval_rule in eval_rules:
            eval_rule_str = DatalogProgram.iterate_datalog_rule(eval_rule)
            iter_eval_str += tab_space + '# ' + eval_rule_str + '\n'
            # there might be different combinations of 'delta' and 'non-delta' joins
            sub_res_index = 0
            delta_atom_num = 0
            eval_rule_body = eval_rule['body']['atoms']
            for atom in eval_rule_body:
                if atom['name'] in relation_set:
                    delta_atom_num += 1

            sub_queries = code_gen_recursive_rule_evaluation(eval_rule, relation_def_map, relation_set)
            sub_query_num = len(sub_queries)
            sub_query_relation = deepcopy(original_relation)
            for sub_query_index in range(sub_query_num):
                sub_query_str = sub_queries[sub_query_index]
                sub_query_list.append(sub_query_str)

        # generate code to create a temporary table to store the results before deduplication
        tmp_mDelta_relation = deepcopy(original_relation)
        tmp_mDelta_relation['name'] = original_relation['name'] + '_tmp_mDelta'
        iter_eval_str += code_gen_create_table(tmp_mDelta_relation, indent=tab_space)

        eval_mDelta_str = '\'' + 'insert into ' + tmp_mDelta_relation['name'] + \
                          ' select * from (' + '\'' + ' + '

        eval_sub_query_num = len(sub_query_list)

        for eval_sub_query_index in range(eval_sub_query_num):
            eval_mDelta_str += sub_query_list[eval_sub_query_index]
            if eval_sub_query_index != eval_sub_query_num - 1:
                eval_mDelta_str += ' + ' + '\'' + ' union all ' + '\'' + ' + '

        eval_mDelta_str += ' + ' + '\'' + ') t;' + '\''

        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + eval_mDelta_str + ')' + '\n'
        iter_eval_str += code_gen_analyze_table(tmp_mDelta_relation['name'], count_only=True, indent=tab_space)
        # deduplication
        deduplication_str = '\'' + 'insert into ' + mDelta_relation['name'] + \
                            ' select * from ' + tmp_mDelta_relation['name'] + ' group by '

        for attribute in eval_relation_attributes:
            deduplication_str += attribute.name + ', '
        deduplication_str = deduplication_str[:len(deduplication_str) - 2]
        deduplication_str += ';\''

        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + deduplication_str + ')' + '\n'
        # drop the tmp table
        iter_eval_str += code_gen_drop_table(tmp_mDelta_relation['name'], indent=tab_space)
        iter_eval_str += code_gen_analyze_table(mDelta_relation['name'], count_only=True, indent=tab_space)
        iter_eval_str += code_gen_log_info_time(if_write=LOG_ON, indent=tab_space)

        # build common delta table for deduplication
        iter_eval_str += code_gen_log_info('Compute intersection (common-delta) for set-difference computation',
                                           if_write=LOG_ON, indent=tab_space)
        iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
        build_common_delta_str = 'insert into ' + original_relation['name'] + '_Common_Delta '
        build_common_delta_str += code_gen_intersection(mDelta_relation, original_relation) + ';'
        iter_eval_str += tab_space + '# compute intersection for anti-join (set-difference)' + '\n'
        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + '\'' + build_common_delta_str + '\'' + ')' + '\n'
        iter_eval_str += code_gen_analyze_table(original_relation['name'] + '_Common_Delta', count_only=True,
                                                indent=tab_space)
        iter_eval_str += code_gen_log_info_time(if_write=LOG_ON, indent=tab_space)





        # generate code to perform set-difference
        iter_eval_str += code_gen_log_info('Compute set-difference',
                                           if_write=LOG_ON, indent=tab_space)
        iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
        iter_eval_str += tab_space + '# compute set-difference\n'
        common_delta_relation = deepcopy(original_relation)
        common_delta_relation['name'] = original_relation['name'] + '_Common_Delta'


        iter_eval_str += tab_space + '# compute delta\n'
        compute_set_diff_str = '\'' + 'insert into ' + '\'' + ' +  ' + '\'' + original_relation[
            'name'] + '_Delta' + '\'' + ' + ' + 'str(iter_num)' + ' + ' + \
                               '\'' + ' select * from ' + original_relation[
                                   'name'] + '_mDelta' + ' where not exists' + ' (' + \
                               code_gen_intersection(common_delta_relation, mDelta_relation,
                                                     sub_query=True) + ');' + '\''



        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + compute_set_diff_str + ')' + '\n'
        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + '\'' + '\\\\' + 'analyzecount ' + \
                         original_relation['name'] + '_Delta' + '\'' + ' + ' + 'str(iter_num)' + \
                         ' + ' + '\'' + '\\n' + '\'' + ')' + '\n'
        iter_eval_str += code_gen_log_info_time(if_write=LOG_ON, indent=tab_space)
        iter_eval_str += tab_space + '# drop mDelta table\n'
        iter_eval_str += code_gen_drop_table(original_relation['name'] + '_mDelta', indent=tab_space)
        # drop common delta table and recreate a new one for next iteration
        iter_eval_str += code_gen_drop_table(original_relation['name'] + '_Common_Delta', indent=tab_space)
        iter_eval_str += tab_space + 'quickstep_shell_instance.create_table(' + original_relation[
            'name'] + '_Common_Delta' + ')\n'





        # generate code to save the current idb
        if original_relation['name'] in pre_table_set:
            iter_eval_str += code_gen_log_info('Save the current IDB',
                                               if_write=LOG_ON, indent=tab_space)
            iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
            iter_eval_str += tab_space + '# save the current idb\n'
            iter_eval_str += code_gen_drop_table(original_relation['name'] + 'Prev', tab_space)
            iter_eval_str += tab_space + 'quickstep_shell_instance.create_table(' + original_relation[
                'name'] + 'Prev' + ')' + '\n'
            copy_idb_str = 'insert into ' + original_relation['name'] + 'Prev' + ' select * from ' + original_relation[
                'name'] + ';'
            iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + '\'' + copy_idb_str + '\'' + ')' + '\n'
            iter_eval_str += code_gen_analyze_table(original_relation['name'] + 'Prev', count_only=True,
                                                    indent=tab_space)
            iter_eval_str += code_gen_log_info_time(if_write=LOG_ON, indent=tab_space)
        # update evaluated idb tables
        iter_eval_str += code_gen_log_info('Update IDB (union delta)',
                                           if_write=LOG_ON, indent=tab_space)
        iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
        iter_eval_str += tab_space + '# update ' + original_relation['name'] + '\n'
        update_idb_str = '\'' + 'insert into ' + original_relation['name'] + ' select * from ' + '\'' + ' + ' + \
                         'new_' + original_relation['name'] + '_delta_name' + ' + ' + '\';\''
        iter_eval_str += tab_space + 'quickstep_shell_instance.sql_command(' + update_idb_str + ')' + '\n'
        iter_eval_str += code_gen_analyze_table(original_relation['name'], count_only=True, indent=tab_space)
        iter_eval_str += code_gen_log_info_time(if_write=LOG_ON, indent=tab_space)

    # drop all old deltas
    iter_eval_str += '\n'
    iter_eval_str += tab_space + '# drop all old intermediate tables ' + '\n'
    for idb in relation_set:
        iter_eval_str += tab_space + 'quickstep_shell_instance.drop_table(' + idb + '_delta_name' + ')' + '\n'

    # generate code to check whether the evaluation reaches the 'fix-point'
    iter_eval_str += '\n'
    iter_eval_str += tab_space + 'is_delta_empty = '
    for idb in relation_set:
        iter_eval_str += 'quickstep_shell_instance.is_table_empty(' + 'new_' + idb + '_delta_name' + ')' + ' and '
    iter_eval_str = iter_eval_str[:len(iter_eval_str) - 5] + '\n'
    # logging code to check the number of tuples in delta tables evaluated in the current iteration
    for idb in relation_set:
        iter_eval_str += code_gen_log_info_count_row(table_name=idb + '_Delta',
                                                     var_table_name='new_' + idb + '_delta_name', if_write=LOG_ON,
                                                     indent=tab_space)
    # logging code to check the number of tuples in the idb tables after the evaluation in the current iteration
    for idb in relation_set:
        iter_eval_str += code_gen_log_info_count_row(table_name=idb, if_write=LOG_ON, indent=tab_space)

    iter_eval_str += code_gen_log_info_update_time(if_write=LOG_ON, indent=tab_space)
    iter_eval_str += code_gen_log_info_get_cur_time('cur_iter_end_time', if_write=LOG_ON, indent=tab_space)
    iter_eval_str += code_gen_log_info_time(time_descrip_var='Iteration Time', \
                                            time_var='cur_iter_end_time - cur_iter_start_time', \
                                            indent=tab_space)
    iter_eval_str += code_gen_log_info('', if_write=LOG_ON, indent=tab_space)
    iter_eval_str += code_gen_log_info('', if_write=LOG_ON, indent=tab_space)

    # generate code after recursive evaluation process completes (after while loop)
    iter_eval_str += '\n'
    iter_eval_str += '# clear all prev tables\n'
    for table in pre_table_set:
        iter_eval_str += code_gen_drop_table(table + 'Prev')
    iter_eval_str += '\n'
    iter_eval_str += '# clear all  deltas\n'
    for idb in relation_set:
        iter_eval_str += 'quickstep_shell_instance.drop_table(' + '\'' + idb + '_Delta' + '\'' + ' + ' + 'str(iter_num)' + ')' + '\n'
        iter_eval_str += 'quickstep_shell_instance.drop_table(' + '\'' + idb + '_Common_Delta' + '\'' + ')' + '\n'

    return before_recursive_eval_str + iter_eval_str


def generate_code(input_datalog_program_file):
    """
    Before iterative process starts:
        1. Read the name of datalog program => create the corresponding python file with name <datalog_program_name>.py
        2. Create EDBs and load data (data should be put under the directory named './Input'
        3. Create IDBs
        4. Analyze all the tables => build catalog including analytical stats of all tables
        5. Evaluate rules following the stratification
        6. Initialize all deltas => analyze all delta tables (For recursive rules if any)
        
    Iterative evaluation process (For recursive rules if any):
    """
    # get the data structures storing the information of the input datalog program
    datalog_program_instance = DatalogProgram.datalog_program(input_datalog_program_file)

    rules = datalog_program_instance.datalog_program
    edb_decl = datalog_program_instance.edb_decl
    idb_decl = datalog_program_instance.idb_decl
    dependency_graph = datalog_program_instance.dependency_graph
    sccs = datalog_program_instance.scc

    # build a mapping between relation name and the corresponding relation definition
    relation_def_map = {}
    for relation in edb_decl:
        relation_def_map[relation['name']] = [relation, 'edb']
    for relation in idb_decl:
        relation_def_map[relation['name']] = [relation, 'idb']

    # create the python script corresponding the datalog program 
    datalog_program_name = input_datalog_program_file.split('.')[0]
    datalog_program_pyscript = open(datalog_program_name + '.py', 'w')

    # import
    code_gen_write_code(datalog_program_pyscript, 'from table import Table\n')
    code_gen_write_code(datalog_program_pyscript, 'import quickstep\n')
    code_gen_write_code(datalog_program_pyscript, 'import json\n')
    if LOG_ON:
        code_gen_write_code(datalog_program_pyscript, 'from monitoring import TimeMonitor\n')
        code_gen_write_code(datalog_program_pyscript, 'from lpalogging import LpaLogger\n')
        code_gen_write_code(datalog_program_pyscript, '\n\n')
        code_gen_write_code(datalog_program_pyscript, 'lpa_logger = LpaLogger()\n')
        code_gen_write_code(datalog_program_pyscript, 'time_monitor = TimeMonitor()\n')
    code_gen_write_code(datalog_program_pyscript, '\n\n')

    # configure and initialize quickstep instance
    code_gen_write_code(datalog_program_pyscript,
                        '# read the configuration of quickstep backend from json config file\n')
    code_gen_write_code(datalog_program_pyscript, 'config_json_file_name = \'Config.json\'\n')
    code_gen_write_code(datalog_program_pyscript, 'with open(config_json_file_name) as config_json_file:\n')
    code_gen_write_code(datalog_program_pyscript, tab_space + 'config = json.load(config_json_file)\n')
    code_gen_write_code(datalog_program_pyscript,
                        'quickstep_shell_path = config[' + '\'' + 'QuickStep_Shell_Path' + '\'' + ']\n')
    code_gen_write_code(datalog_program_pyscript,
                        'quickstep_shell_instance = quickstep.Database(quickstep_shell_path)\n')

    code_gen_write_code(datalog_program_pyscript,
                        code_gen_log_info('Start creating IDB and EDB tables and populating facts',
                                          if_write=LOG_ON))
    code_gen_write_code(datalog_program_pyscript, code_gen_log_info_update_time(), if_write=LOG_ON)

    code_gen_write_code(datalog_program_pyscript, '# create edb tables\n')
    for relation in edb_decl:
        code_gen_write_code(datalog_program_pyscript, code_gen_create_table(relation))

    code_gen_write_code(datalog_program_pyscript, '# create idb tables\n')
    for relation in idb_decl:
        code_gen_write_code(datalog_program_pyscript, code_gen_create_table(relation))

    code_gen_write_code(datalog_program_pyscript, '# populate facts into edbs\n')
    for relation in edb_decl:
        code_gen_write_code(datalog_program_pyscript, code_gen_populate_data_into_edb(relation))

    code_gen_write_code(datalog_program_pyscript, '# analyze all tables\n')
    code_gen_write_code(datalog_program_pyscript, code_gen_analyze_all(count_only=True))

    code_gen_write_code(datalog_program_pyscript, code_gen_log_info_time(), if_write=LOG_ON)

    # start generating code to evaluate sccs
    for scc in sccs:
        if is_trivial_scc(sccs[scc], dependency_graph):
            code_gen_write_code(datalog_program_pyscript, '# non-recursive rule evaluation\n')
            datalog_rule = rules[scc]
            rule_str = DatalogProgram.iterate_datalog_rule(datalog_rule)
            code_gen_write_code(datalog_program_pyscript, code_gen_log_info(rule_str), if_write=LOG_ON)
            code_gen_write_code(datalog_program_pyscript, code_gen_log_info_update_time(), if_write=LOG_ON)
            code_gen_write_code(datalog_program_pyscript, '# ' + rule_str + '\n')
            code_gen_write_code(datalog_program_pyscript,
                                code_gen_non_recursive_rule_evaluation(datalog_rule, relation_def_map))
            code_gen_write_code(datalog_program_pyscript, '\n')
            code_gen_write_code(datalog_program_pyscript,
                                code_gen_log_info_time(time_descrip_var='Rule Evaluation Time'),
                                if_write=LOG_ON)
            code_gen_write_code(datalog_program_pyscript, '\n')
        else:
            code_gen_write_code(datalog_program_pyscript, '# recursive rules evaluation\n')
            recursive_rules_indices = sccs[scc]
            recursive_rules = []
            for rule_index in recursive_rules_indices:
                recursive_rules.append(rules[rule_index])
                code_gen_write_code(datalog_program_pyscript,
                                    '# ' + DatalogProgram.iterate_datalog_rule(rules[rule_index]) + '\n')
            code_gen_write_code(datalog_program_pyscript,
                                code_gen_recursive_rules_evaluation(recursive_rules, relation_def_map))

    code_gen_write_code(datalog_program_pyscript, code_gen_log_info_time(time_descrip_var='Total Evaluation Time',
                                                                         time_var='time_monitor.global_elapse_time()',
                                                                         if_write=LOG_ON))
    code_gen_write_code(datalog_program_pyscript, 'quickstep_shell_instance.stop()\n')
    datalog_program_pyscript.close()


def main():
    try:
        input_file_name = sys.argv[1]
    except:
        raise Exception('No input file is given specifying the datalog program')

    generate_code(input_file_name)


if __name__ == '__main__':
    main()
