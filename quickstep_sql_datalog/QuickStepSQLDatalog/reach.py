from table import Table
import quickstep
import json
from monitoring import TimeMonitor
from lpalogging import LpaLogger


lpa_logger = LpaLogger()
time_monitor = TimeMonitor()


# read the configuration of quickstep backend from json config file
config_json_file_name = 'Config.json'
with open(config_json_file_name) as config_json_file:
    config = json.load(config_json_file)
quickstep_shell_path = config['QuickStep_Shell_Path']
quickstep_shell_instance = quickstep.Database(quickstep_shell_path)
lpa_logger.info('Start creating IDB and EDB tables and populating facts')
time_monitor.update()
# create edb tables
id = Table('id')
id.add_attribute('x', 'int')
quickstep_shell_instance.create_table(id)
arc = Table('arc')
arc.add_attribute('x', 'int')
arc.add_attribute('y', 'int')
quickstep_shell_instance.create_table(arc)
# create idb tables
reach = Table('reach')
reach.add_attribute('x', 'int')
quickstep_shell_instance.create_table(reach)
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
# populate facts into edbs
quickstep_shell_instance.load_data_from_file(id, './Input/id.tbl', ',')
quickstep_shell_instance.load_data_from_file(arc, './Input/arc.tbl', ',')
# analyze all tables
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
# non-recursive rule evaluation
lpa_logger.info('reach(y) :- id(y).')
time_monitor.update()
# reach(y) :- id(y).
reach_count=quickstep_shell_instance.count_rows('reach')
lpa_logger.info('Number of tuples in reach: ' + str(reach_count))
tmp_res_table = Table('tmp_res_table')
tmp_res_table.add_attribute('x', 'int')
quickstep_shell_instance.create_table(tmp_res_table)
quickstep_shell_instance.sql_command('insert into tmp_res_table select i0.x as x from id i0;')
quickstep_shell_instance.load_data_from_table(tmp_res_table, ['x'], reach, ['x'])
quickstep_shell_instance.sql_command('\\analyzecount reach\n')
quickstep_shell_instance.drop_table('tmp_res_table')
reach_count=quickstep_shell_instance.count_rows('reach')
lpa_logger.info('Number of tuples in reach: ' + str(reach_count))

lpa_logger.info('Rule Evaluation Time: ' + str(time_monitor.local_elapse_time()) + 's')

# recursive rules evaluation
# reach(y) :- reach(x), arc(x, y).
lpa_logger.info('Start creating delta, prev, common-delta tables for semi-naive evaluation')
time_monitor.update()
reach_Delta = Table('reach_Delta0')
reach_Delta.add_attribute('x', 'int')
quickstep_shell_instance.create_table(reach_Delta)
reach_Common_Delta = Table('reach_Common_Delta')
reach_Common_Delta.add_attribute('x', 'int')
quickstep_shell_instance.create_table(reach_Common_Delta)
quickstep_shell_instance.sql_command('\\analyze \n')
quickstep_shell_instance.load_data_from_table(reach, ['x'], reach_Delta, ['x'])
quickstep_shell_instance.sql_command('\\analyze \n')
lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
is_delta_empty = quickstep_shell_instance.is_table_empty('reach_Delta0')
iter_num = 0
while(not is_delta_empty):
    lpa_logger.info('#####Start Iteration ' + str(iter_num + 1) + '#####')
    time_monitor.update()
    cur_iter_start_time = time_monitor.local_start_time
    reach_delta_name = 'reach_Delta' + str(iter_num)

    iter_num += 1

    new_reach_delta_name = 'reach_Delta' + str(iter_num)

    reach_Delta.rename(new_reach_delta_name)

    quickstep_shell_instance.create_table(reach_Delta)

    # reach
    reach_mDelta = Table('reach_mDelta')
    reach_mDelta.add_attribute('x', 'int')
    quickstep_shell_instance.create_table(reach_mDelta)

    lpa_logger.info('Evaluate reach')
    # reach(y) :- reach(x), arc(x, y).
    reach_tmp_mDelta = Table('reach_tmp_mDelta')
    reach_tmp_mDelta.add_attribute('x', 'int')
    quickstep_shell_instance.create_table(reach_tmp_mDelta)
    quickstep_shell_instance.sql_command('insert into reach_tmp_mDelta select * from (' + 'select a1.y as x from ' + 'reach_Delta' + str(iter_num-1) + ' r0, arc a1 where r0.x = a1.x' + ') t;')
    quickstep_shell_instance.sql_command('\\analyzecount reach_tmp_mDelta\n')
    quickstep_shell_instance.sql_command('insert into reach_mDelta select * from reach_tmp_mDelta group by x;')
    quickstep_shell_instance.drop_table('reach_tmp_mDelta')
    quickstep_shell_instance.sql_command('\\analyzecount reach_mDelta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute intersection (common-delta) for set-difference computation')
    time_monitor.update()
    # compute intersection for anti-join (set-difference)
    quickstep_shell_instance.sql_command('insert into reach_Common_Delta select reach_mDelta.x from reach_mDelta, reach where reach_mDelta.x = reach.x;')
    quickstep_shell_instance.sql_command('\\analyzecount reach_Common_Delta\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    lpa_logger.info('Compute set-difference')
    time_monitor.update()
    # compute set-difference
    # compute delta
    quickstep_shell_instance.sql_command('insert into ' +  'reach_Delta' + str(iter_num) + ' select * from reach_mDelta where not exists (select reach_Common_Delta.x from reach_Common_Delta where reach_Common_Delta.x = reach_mDelta.x);')
    quickstep_shell_instance.sql_command('\\analyze reach_Delta' + str(iter_num) + '\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')
    # drop mDelta table
    quickstep_shell_instance.drop_table('reach_mDelta')
    quickstep_shell_instance.drop_table('reach_Common_Delta')
    quickstep_shell_instance.create_table(reach_Common_Delta)
    lpa_logger.info('Update IDB (union delta)')
    time_monitor.update()
    # update reach
    quickstep_shell_instance.sql_command('insert into reach select * from ' + new_reach_delta_name + ';')
    quickstep_shell_instance.sql_command('\\analyzecount reach\n')
    lpa_logger.info('Time: ' + str(time_monitor.local_elapse_time()) + 's')

    # drop all old intermediate tables 
    quickstep_shell_instance.drop_table(reach_delta_name)

    is_delta_empty = quickstep_shell_instance.is_table_empty(new_reach_delta_name)
    reach_Delta_count=quickstep_shell_instance.count_rows(new_reach_delta_name)
    lpa_logger.info('Number of tuples in reach_Delta: ' + str(reach_Delta_count))
    reach_count=quickstep_shell_instance.count_rows('reach')
    lpa_logger.info('Number of tuples in reach: ' + str(reach_count))
    time_monitor.update()
    cur_iter_end_time = time_monitor.local_start_time
    lpa_logger.info('Iteration Time: ' + str(cur_iter_end_time - cur_iter_start_time) + 's')
    lpa_logger.info('')
    lpa_logger.info('')

# clear all prev tables

# clear all  deltas
quickstep_shell_instance.drop_table('reach_Delta' + str(iter_num))
quickstep_shell_instance.drop_table('reach_Common_Delta')
lpa_logger.info('Total Evaluation Time: ' + str(time_monitor.global_elapse_time()) + 's')
quickstep_shell_instance.stop()
