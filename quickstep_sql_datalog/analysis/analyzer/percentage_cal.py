input_file = open('input')
output_file = open('output', 'w')

for line in input_file:
    if not line:
        continue
    else:
        data = [i.strip() for i in line.split()]
        number = float(data[0])
        p = []
        for d in range(len(data)-1):
            ds = data[d+1].split('(')
            if(len(ds) == 1):
                p.append(ds[0].split('%')[0])
            else:
                p.append(ds[1].split('%')[0])
                          
        output_file.write(str(number) + '\t')
        for p_n in p:
            print(p_n)
            val = number * 0.01 * float(p_n)
            output_file.write(str(val) + '\t')
        output_file.write('\n')

input_file.close()
output_file.close()
