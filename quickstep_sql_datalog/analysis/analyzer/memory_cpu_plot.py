"""
Currently assume that the total number of cores on the machine is 8, 
this assumption will be thrown away later (generalize)
"""

import sys, math, numpy
import matplotlib.pyplot as plt
import matplotlib

core_num = int(sys.argv[1])
normalize_factor = int(sys.argv[2])

memory_file = open('memory_log')
cpu_file = open('cpu_log')

memory = []
cpu = []

for i in range(core_num):
    cpu.append([])

for line in memory_file:
    if not line:
        continue
    else:
        memory.append(float([i.strip() for i in line.split()][0]))

for line in cpu_file:
    if not line:
        continue 
    else:
        core_info = [i.strip() for i in line.split(',')]
        for i in range(core_num):
            cpu[i].append(float(core_info[i]))

record_num = len(memory)
normalize_time = [normalize_factor*i for i in range(record_num)]


fig = plt.figure(figsize=(16,8))
plt.ylabel('Memory Usage Percentage (%)')
plt.xlabel('Time (s)')
plt.plot(normalize_time, memory, '-', color = 'blue')
plt.ylim(ymin=0)
plt.savefig('memory_by_time.png')
plt.clf()

core_legend_list = []
for i in range(core_num):
    core_legend_list.append('core' + str(i+1))

core_legend_tuple = tuple(core_legend_list)

fig = plt.figure(figsize=(16,8))
plt.ylabel('CPU Cores Ultilization Percentage (%)')
plt.xlabel('Time (s)')

colors = matplotlib.colors.cnames
for core_index in range(core_num):
    plt.plot(normalize_time, cpu[core_index], '-', color = colors[core_index] )

plt.legend(core_legend_tuple, loc='best')
plt.ylim(ymin=0, ymax=110)
plt.savefig('cpu_by_time.png')
plt.clf()
