import pydot
import pyparsing
import json
import collections
import sys

def pasere_span(info_str):
    """
    return:
        [start_time, end_time] ((int, int)) 
    """
    str1 = info_str.split(':')[1].split(' ') 
    start_str = str1[1] 
    end_str = str1[2]
    start_val = start_str.split('ms')[0].split('[')[1]
    end_val = end_str.split('ms')[0]
    return [int(start_val), int(end_val)]

def parse_total(info_str):
    """
    return:
        total_time (int) 
    """
    total_val = info_str.split(':')[1].split('ms')[0]
    return int(total_val)

def parse_ec(info_str):
    """
    return:
        effective_concurrency (float)
    """
    ec_val = info_str.split(':')[1].split('&')[0]
    return float(ec_val)

def parse_wo(info_str):
    """
    return:
        number of work order (int)
    """
    wo_val = info_str.split('work')[0].strip() 
    return int(wo_val)

def parse_mwoet(info_str):
    """
    return:
        mean work worder execution time (float)
    """
    mwoet_val = info_str.split(':')[1].split('ms')[0].strip() 
    return float(mwoet_val)


dot_file = sys.argv[1]
graph = pydot.graph_from_dot_file(dot_file)
(g,) = graph
nodes = g.get_nodes()

"""
for n in nodes:
    print(n.get_attributes())

Example of attribute of dot.node in execution plan:
    {'style': 'filled', 'label': '"[82] FinalizeAggregationOperator&#10;span: [9538ms, 10617ms] (1.73%)&#10;total: 3ms (0.00%)&#10;effective concurrency: 0.00&#10;41 work orders&#10;Mean work order execution time: 0.07 ms"', 'fillcolor': '"0.024293 0.024293 1.0"'}

All we need is label
"""

operator_info = [] 

"""
Parse the operator info and store the structured info into object for later use

Object keys:

    op_name: the name of the operator 
    op_des: additional info about the operator 
    span: the participating time interval of the operator
    total: the total time of the operator (in a single core) 
    ec: effective concurrency
    mwoet: mean work order execution time
"""
for n in nodes:
    try:
        label_info = n.get_attributes()['label']
    except:
        continue

    info_list = label_info[1:len(label_info)-1].split(';')
    info_object = collections.OrderedDict({})
    key_index = 0
    info_object['op_name'] = info_list[key_index]
    
    object_num = len(info_list)
    if(object_num == 7):
        # with specific description
        key_index += 1
        info_object['op_des'] = info_list[key_index]
    else:
        info_object['op_des'] = 'none'    

    key_index += 1
    info_object['span'] = pasere_span(info_list[key_index])
    key_index += 1
    info_object['total'] = parse_total(info_list[key_index])
    key_index += 1
    info_object['ec'] = parse_ec(info_list[key_index])
    key_index += 1
    info_object['wo'] = parse_wo(info_list[key_index])
    key_index += 1
    info_object['mwoet'] = parse_mwoet(info_list[key_index])

    operator_info.append(info_object)

# write the parsed result into txt file
interval_output = open('interval_output', 'w')
op_name_output = open('op_name_output', 'w')
op_des_output = open('op_des_output', 'w')
op_data = open('op_data_output', 'w')

for op_info in operator_info:
    """
    for info in op_info:
        output.write(str(op_info[info]) + '\t') 
    """
    op_name = op_info['op_name'] 
    op_des = op_info['op_des']
    span_info = op_info['span']

    print(span_info)
    interval_output.write(str(span_info[0]) + '\t' + str(span_info[1]) + '\n')
    op_name_output.write(str(op_name)+'\n')
    op_des_output.write(str(op_des) + '\n')
     
    # write other op relevant data
    print(op_info['ec'])
    norm_total = op_info['total']
    if(op_info['ec'] > 1):
        norm_total = int(norm_total/op_info['ec'])
    op_data.write(str(op_info['ec']) + '\t' + str(op_info['wo']) + '\t' + str(op_info['mwoet']) + '\t' + str(op_info['total']) + '\t' + str(norm_total) + '\n')

interval_output.close()
op_name_output.close()
op_des_output.close()
op_data.close()
