"""
Currently assume that the total number of cores on the machine is 8, 
this assumption will be thrown away later (generalize)
"""

import sys, math, numpy
import matplotlib.pyplot as plt

core_num = 8

memory_file = open('memory_log')
cpu_file = open('cpu_log')

memory = []
cpu = []

for i in range(core_num):
    cpu.append([])

for line in memory_file:
    if not line:
        continue
    else:
        memory.append(float([i.strip() for i in line.split()][0]))

for line in cpu_file:
    if not line:
        continue 
    else:
        core_info = [i.strip() for i in line.split(',')]
        for i in range(core_num):
            cpu[i].append(float(core_info[i]))

record_num = len(memory)
normalize_time = [5*i for i in range(record_num)]


fig = plt.figure(figsize=(16,8))
plt.ylabel('Memory Usage Percentage (%)')
plt.xlabel('Time (s)')
plt.plot(normalize_time, memory, '-', color = 'blue')
plt.ylim(ymin=0)
plt.savefig('memory_by_time.png')
plt.clf()

fig = plt.figure(figsize=(16,8))
plt.ylabel('CPU Cores Ultilization Percentage (%)')
plt.xlabel('Time (s)')
plt.plot(normalize_time, cpu[0], '-', color = 'blue')
plt.plot(normalize_time, cpu[1], '-', color = 'black')
plt.plot(normalize_time, cpu[2], '-', color = 'red')
plt.plot(normalize_time, cpu[3], '-', color = 'green')
plt.plot(normalize_time, cpu[4], '-', color = 'yellow')
plt.plot(normalize_time, cpu[5], '-', color = 'pink')
plt.plot(normalize_time, cpu[6], '-', color = 'brown')
plt.plot(normalize_time, cpu[7], '-', color = 'orange')
plt.legend(('core1', 'core2', 'core3', 'core4', 'core5', 'core6', 'core7', 'core8'), loc='best')
plt.ylim(ymin=0, ymax=110)
plt.savefig('cpu_by_time.png')
plt.clf()
