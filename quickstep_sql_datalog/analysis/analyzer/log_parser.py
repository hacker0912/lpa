import sys

# Usage: python log_parser <log_file>
input_file_name = sys.argv[1]
input_file = open(input_file_name) 

"""
 Write the time per iteration to an output file: iter_time.output:
 
 <1st iteration time>
 <2nd iteration time>
 ... 
"""
output_file = open('iter_time.txt', 'w')
for line in input_file:
    if not line:
        continue
    else:
        info = line.split('INFO:root')
        if(len(info) >= 2):
            iter_time_info = info[1].split('Iteration Time:')
            if(len(iter_time_info) == 2):
                time_str = iter_time_info[1].split('s')[0].strip()
                output_file.write(time_str + '\n')

input_file.close()
output_file.close()  
