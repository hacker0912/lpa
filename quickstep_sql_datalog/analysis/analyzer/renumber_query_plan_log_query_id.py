import sys

input_file_name = 'query_plan'
input_file = open(input_file_name)

output_file = open('query_plan_renumber', 'w')

query_id = 0
for line in input_file:
    if not line:
        continue
    else:
        if(len(line) >= 21 and line[:5] == '#####'):
            output_file.write('#####' + 'QUERY ' + 'ID ' + str(query_id) + '#####' + '\n')
            query_id += 1
        else:
            output_file.write(line)

input_file.close()
output_file.close()
