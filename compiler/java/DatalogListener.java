// Generated from Datalog.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DatalogParser}.
 */
public interface DatalogListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DatalogParser#datalog_program}.
	 * @param ctx the parse tree
	 */
	void enterDatalog_program(DatalogParser.Datalog_programContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#datalog_program}.
	 * @param ctx the parse tree
	 */
	void exitDatalog_program(DatalogParser.Datalog_programContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#datalog_rule}.
	 * @param ctx the parse tree
	 */
	void enterDatalog_rule(DatalogParser.Datalog_ruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#datalog_rule}.
	 * @param ctx the parse tree
	 */
	void exitDatalog_rule(DatalogParser.Datalog_ruleContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(DatalogParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(DatalogParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#head}.
	 * @param ctx the parse tree
	 */
	void enterHead(DatalogParser.HeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#head}.
	 * @param ctx the parse tree
	 */
	void exitHead(DatalogParser.HeadContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#negation}.
	 * @param ctx the parse tree
	 */
	void enterNegation(DatalogParser.NegationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#negation}.
	 * @param ctx the parse tree
	 */
	void exitNegation(DatalogParser.NegationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(DatalogParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(DatalogParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(DatalogParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(DatalogParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#math_expr}.
	 * @param ctx the parse tree
	 */
	void enterMath_expr(DatalogParser.Math_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#math_expr}.
	 * @param ctx the parse tree
	 */
	void exitMath_expr(DatalogParser.Math_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#compare_expr}.
	 * @param ctx the parse tree
	 */
	void enterCompare_expr(DatalogParser.Compare_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#compare_expr}.
	 * @param ctx the parse tree
	 */
	void exitCompare_expr(DatalogParser.Compare_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#aggregation_expr}.
	 * @param ctx the parse tree
	 */
	void enterAggregation_expr(DatalogParser.Aggregation_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#aggregation_expr}.
	 * @param ctx the parse tree
	 */
	void exitAggregation_expr(DatalogParser.Aggregation_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#compare_op}.
	 * @param ctx the parse tree
	 */
	void enterCompare_op(DatalogParser.Compare_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#compare_op}.
	 * @param ctx the parse tree
	 */
	void exitCompare_op(DatalogParser.Compare_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#aggregation_op}.
	 * @param ctx the parse tree
	 */
	void enterAggregation_op(DatalogParser.Aggregation_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#aggregation_op}.
	 * @param ctx the parse tree
	 */
	void exitAggregation_op(DatalogParser.Aggregation_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#math_op}.
	 * @param ctx the parse tree
	 */
	void enterMath_op(DatalogParser.Math_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#math_op}.
	 * @param ctx the parse tree
	 */
	void exitMath_op(DatalogParser.Math_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link DatalogParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(DatalogParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link DatalogParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(DatalogParser.ConstantContext ctx);
}