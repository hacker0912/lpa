// Generated from Datalog.g4 by ANTLR 4.7
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DatalogLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TOKEN_EDB=1, TOKEN_IDB=2, TOKEN_RULE=3, TOKEN_MIN=4, TOKEN_MAX=5, TOKEN_SUM=6, 
		TOKEN_COUNT=7, TOKEN_ID=8, TOKEN_NUMBER=9, TOKEN_STRING=10, TOKEN_ANY=11, 
		TOKEN_COMMA=12, TOKEN_SEMICOLON=13, TOKEN_DOT=14, TOKEN_BODY_HEAD_SEP=15, 
		TOKEN_PLUS=16, TOKEN_MINUS=17, TOKEN_MULT=18, TOKEN_DIV=19, TOKEN_NOT=20, 
		TOKEN_NOT_EQUALS=21, TOKEN_EQUALS=22, TOKEN_GREATER_EQUAL_THAN=23, TOKEN_GREATER_THAN=24, 
		TOKEN_LESS_EQUAL_THAN=25, TOKEN_LESS_THAN=26, TOKEN_LEFT_PAREN=27, TOKEN_RIGHT_PAREN=28, 
		TOKEN_WS=29;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", "TOKEN_MIN", "TOKEN_MAX", "TOKEN_SUM", 
		"TOKEN_COUNT", "TOKEN_ID", "TOKEN_NUMBER", "TOKEN_STRING", "TOKEN_ANY", 
		"TOKEN_COMMA", "TOKEN_SEMICOLON", "TOKEN_DOT", "TOKEN_BODY_HEAD_SEP", 
		"TOKEN_PLUS", "TOKEN_MINUS", "TOKEN_MULT", "TOKEN_DIV", "TOKEN_NOT", "TOKEN_NOT_EQUALS", 
		"TOKEN_EQUALS", "TOKEN_GREATER_EQUAL_THAN", "TOKEN_GREATER_THAN", "TOKEN_LESS_EQUAL_THAN", 
		"TOKEN_LESS_THAN", "TOKEN_LEFT_PAREN", "TOKEN_RIGHT_PAREN", "TOKEN_WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'EDB_DECL'", "'IDB_DECL'", "'RULE_DECL'", "'MIN'", "'MAX'", "'SUM'", 
		"'COUNT'", null, null, null, "'_'", "','", "';'", "'.'", "':-'", "'+'", 
		"'-'", "'*'", "'/'", "'!'", "'!='", "'='", "'>='", "'>'", "'<='", "'<'", 
		"'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", "TOKEN_MIN", "TOKEN_MAX", 
		"TOKEN_SUM", "TOKEN_COUNT", "TOKEN_ID", "TOKEN_NUMBER", "TOKEN_STRING", 
		"TOKEN_ANY", "TOKEN_COMMA", "TOKEN_SEMICOLON", "TOKEN_DOT", "TOKEN_BODY_HEAD_SEP", 
		"TOKEN_PLUS", "TOKEN_MINUS", "TOKEN_MULT", "TOKEN_DIV", "TOKEN_NOT", "TOKEN_NOT_EQUALS", 
		"TOKEN_EQUALS", "TOKEN_GREATER_EQUAL_THAN", "TOKEN_GREATER_THAN", "TOKEN_LESS_EQUAL_THAN", 
		"TOKEN_LESS_THAN", "TOKEN_LEFT_PAREN", "TOKEN_RIGHT_PAREN", "TOKEN_WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public DatalogLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Datalog.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\37\u00bf\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\7\tn\n\t\f\t\16\tq\13\t\3\n\5\n"+
		"t\n\n\3\n\7\nw\n\n\f\n\16\nz\13\n\3\n\3\n\6\n~\n\n\r\n\16\n\177\3\n\6"+
		"\n\u0083\n\n\r\n\16\n\u0084\5\n\u0087\n\n\3\13\3\13\6\13\u008b\n\13\r"+
		"\13\16\13\u008c\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20"+
		"\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\26"+
		"\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\34\3\34"+
		"\3\35\3\35\3\36\6\36\u00ba\n\36\r\36\16\36\u00bb\3\36\3\36\2\2\37\3\3"+
		"\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21"+
		"!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37\3\2"+
		"\7\4\2C\\c|\5\2\62;C\\c|\4\2--//\3\2\62;\5\2\13\f\17\17\"\"\2\u00c6\2"+
		"\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2"+
		"\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2"+
		"\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2"+
		"\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2"+
		"\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2"+
		"\2\2\3=\3\2\2\2\5F\3\2\2\2\7O\3\2\2\2\tY\3\2\2\2\13]\3\2\2\2\ra\3\2\2"+
		"\2\17e\3\2\2\2\21k\3\2\2\2\23s\3\2\2\2\25\u0088\3\2\2\2\27\u0090\3\2\2"+
		"\2\31\u0092\3\2\2\2\33\u0094\3\2\2\2\35\u0096\3\2\2\2\37\u0098\3\2\2\2"+
		"!\u009b\3\2\2\2#\u009d\3\2\2\2%\u009f\3\2\2\2\'\u00a1\3\2\2\2)\u00a3\3"+
		"\2\2\2+\u00a5\3\2\2\2-\u00a8\3\2\2\2/\u00aa\3\2\2\2\61\u00ad\3\2\2\2\63"+
		"\u00af\3\2\2\2\65\u00b2\3\2\2\2\67\u00b4\3\2\2\29\u00b6\3\2\2\2;\u00b9"+
		"\3\2\2\2=>\7G\2\2>?\7F\2\2?@\7D\2\2@A\7a\2\2AB\7F\2\2BC\7G\2\2CD\7E\2"+
		"\2DE\7N\2\2E\4\3\2\2\2FG\7K\2\2GH\7F\2\2HI\7D\2\2IJ\7a\2\2JK\7F\2\2KL"+
		"\7G\2\2LM\7E\2\2MN\7N\2\2N\6\3\2\2\2OP\7T\2\2PQ\7W\2\2QR\7N\2\2RS\7G\2"+
		"\2ST\7a\2\2TU\7F\2\2UV\7G\2\2VW\7E\2\2WX\7N\2\2X\b\3\2\2\2YZ\7O\2\2Z["+
		"\7K\2\2[\\\7P\2\2\\\n\3\2\2\2]^\7O\2\2^_\7C\2\2_`\7Z\2\2`\f\3\2\2\2ab"+
		"\7U\2\2bc\7W\2\2cd\7O\2\2d\16\3\2\2\2ef\7E\2\2fg\7Q\2\2gh\7W\2\2hi\7P"+
		"\2\2ij\7V\2\2j\20\3\2\2\2ko\t\2\2\2ln\t\3\2\2ml\3\2\2\2nq\3\2\2\2om\3"+
		"\2\2\2op\3\2\2\2p\22\3\2\2\2qo\3\2\2\2rt\t\4\2\2sr\3\2\2\2st\3\2\2\2t"+
		"\u0086\3\2\2\2uw\t\5\2\2vu\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3\2\2\2y{\3\2"+
		"\2\2zx\3\2\2\2{}\13\2\2\2|~\t\5\2\2}|\3\2\2\2~\177\3\2\2\2\177}\3\2\2"+
		"\2\177\u0080\3\2\2\2\u0080\u0087\3\2\2\2\u0081\u0083\t\5\2\2\u0082\u0081"+
		"\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085"+
		"\u0087\3\2\2\2\u0086x\3\2\2\2\u0086\u0082\3\2\2\2\u0087\24\3\2\2\2\u0088"+
		"\u008a\7)\2\2\u0089\u008b\t\3\2\2\u008a\u0089\3\2\2\2\u008b\u008c\3\2"+
		"\2\2\u008c\u008a\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\3\2\2\2\u008e"+
		"\u008f\7)\2\2\u008f\26\3\2\2\2\u0090\u0091\7a\2\2\u0091\30\3\2\2\2\u0092"+
		"\u0093\7.\2\2\u0093\32\3\2\2\2\u0094\u0095\7=\2\2\u0095\34\3\2\2\2\u0096"+
		"\u0097\7\60\2\2\u0097\36\3\2\2\2\u0098\u0099\7<\2\2\u0099\u009a\7/\2\2"+
		"\u009a \3\2\2\2\u009b\u009c\7-\2\2\u009c\"\3\2\2\2\u009d\u009e\7/\2\2"+
		"\u009e$\3\2\2\2\u009f\u00a0\7,\2\2\u00a0&\3\2\2\2\u00a1\u00a2\7\61\2\2"+
		"\u00a2(\3\2\2\2\u00a3\u00a4\7#\2\2\u00a4*\3\2\2\2\u00a5\u00a6\7#\2\2\u00a6"+
		"\u00a7\7?\2\2\u00a7,\3\2\2\2\u00a8\u00a9\7?\2\2\u00a9.\3\2\2\2\u00aa\u00ab"+
		"\7@\2\2\u00ab\u00ac\7?\2\2\u00ac\60\3\2\2\2\u00ad\u00ae\7@\2\2\u00ae\62"+
		"\3\2\2\2\u00af\u00b0\7>\2\2\u00b0\u00b1\7?\2\2\u00b1\64\3\2\2\2\u00b2"+
		"\u00b3\7>\2\2\u00b3\66\3\2\2\2\u00b4\u00b5\7*\2\2\u00b58\3\2\2\2\u00b6"+
		"\u00b7\7+\2\2\u00b7:\3\2\2\2\u00b8\u00ba\t\6\2\2\u00b9\u00b8\3\2\2\2\u00ba"+
		"\u00bb\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\3\2"+
		"\2\2\u00bd\u00be\b\36\2\2\u00be<\3\2\2\2\r\2mosx\177\u0084\u0086\u008a"+
		"\u008c\u00bb\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}