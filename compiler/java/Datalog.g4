grammar Datalog;

datalog_program
	: datalog_rule (datalog_rule)*
	;

datalog_rule
    : head TOKEN_BODY_HEAD_SEP body TOKEN_DOT
    ;

body
	: ((atom | compare_expr | assign | negation) TOKEN_COMMA)* atom (TOKEN_COMMA (atom | compare_expr | assign | negation))*
	;

head
	:atom
	;


negation
	: TOKEN_NOT atom
	;

atom
	: TOKEN_ID 
	  TOKEN_LEFT_PAREN 
	  (TOKEN_ID | aggregation_expr | TOKEN_ANY | constant) 
	  (TOKEN_COMMA (TOKEN_ID | aggregation_expr | TOKEN_ANY | constant))* 
	  TOKEN_RIGHT_PAREN
	;

assign
	: TOKEN_ID TOKEN_EQUALS math_expr
	;

math_expr
	: TOKEN_ID math_op TOKEN_ID
	;

compare_expr
	: TOKEN_ID compare_op TOKEN_ID
	| TOKEN_ID compare_op TOKEN_NUMBER
    | TOKEN_NUMBER compare_op TOKEN_ID
	;

aggregation_expr
	: aggregation_op TOKEN_LEFT_PAREN TOKEN_ID TOKEN_RIGHT_PAREN 
	;

compare_op 
	: TOKEN_NOT_EQUALS
    | TOKEN_EQUALS
	| TOKEN_GREATER_THAN
	| TOKEN_GREATER_EQUAL_THAN
    | TOKEN_LESS_THAN
    | TOKEN_LESS_EQUAL_THAN
	;

aggregation_op  
	: TOKEN_MIN
	| TOKEN_MAX
	| TOKEN_SUM
	| TOKEN_COUNT
	;

math_op 
	: TOKEN_PLUS
	| TOKEN_MINUS
	| TOKEN_MULT
	| TOKEN_DIV
	;

constant
	: TOKEN_NUMBER
	| TOKEN_STRING
	;


TOKEN_EDB: 'EDB_DECL';
TOKEN_IDB: 'IDB_DECL';
TOKEN_RULE: 'RULE_DECL';
TOKEN_MIN: 'MIN';
TOKEN_MAX: 'MAX';
TOKEN_SUM: 'SUM';
TOKEN_COUNT: 'COUNT';
TOKEN_ID: [A-Za-z]([A-Za-z]|[0-9])*;
TOKEN_NUMBER: [-+]?(([0-9]*.[0-9]+)|([0-9]+));
TOKEN_STRING: '\''([A-Za-z] | [0-9])+'\'';
TOKEN_ANY: '_';
TOKEN_COMMA: ',';
TOKEN_SEMICOLON: ';';
TOKEN_DOT: '.';
TOKEN_BODY_HEAD_SEP: ':-';
TOKEN_PLUS: '+';
TOKEN_MINUS: '-';
TOKEN_MULT: '*';
TOKEN_DIV: '/'; 
TOKEN_NOT: '!';
TOKEN_NOT_EQUALS: '!=';
TOKEN_EQUALS: '=';
TOKEN_GREATER_EQUAL_THAN: '>=';
TOKEN_GREATER_THAN: '>';
TOKEN_LESS_EQUAL_THAN: '<=';
TOKEN_LESS_THAN: '<';
TOKEN_LEFT_PAREN: '(';
TOKEN_RIGHT_PAREN: ')';
TOKEN_WS: [ \t\r\n]+ -> skip;
