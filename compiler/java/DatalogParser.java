// Generated from Datalog.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DatalogParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TOKEN_EDB=1, TOKEN_IDB=2, TOKEN_RULE=3, TOKEN_MIN=4, TOKEN_MAX=5, TOKEN_SUM=6, 
		TOKEN_COUNT=7, TOKEN_ID=8, TOKEN_NUMBER=9, TOKEN_STRING=10, TOKEN_ANY=11, 
		TOKEN_COMMA=12, TOKEN_SEMICOLON=13, TOKEN_DOT=14, TOKEN_BODY_HEAD_SEP=15, 
		TOKEN_PLUS=16, TOKEN_MINUS=17, TOKEN_MULT=18, TOKEN_DIV=19, TOKEN_NOT=20, 
		TOKEN_NOT_EQUALS=21, TOKEN_EQUALS=22, TOKEN_GREATER_EQUAL_THAN=23, TOKEN_GREATER_THAN=24, 
		TOKEN_LESS_EQUAL_THAN=25, TOKEN_LESS_THAN=26, TOKEN_LEFT_PAREN=27, TOKEN_RIGHT_PAREN=28, 
		TOKEN_WS=29;
	public static final int
		RULE_datalog_program = 0, RULE_datalog_rule = 1, RULE_body = 2, RULE_head = 3, 
		RULE_negation = 4, RULE_atom = 5, RULE_assign = 6, RULE_math_expr = 7, 
		RULE_compare_expr = 8, RULE_aggregation_expr = 9, RULE_compare_op = 10, 
		RULE_aggregation_op = 11, RULE_math_op = 12, RULE_constant = 13;
	public static final String[] ruleNames = {
		"datalog_program", "datalog_rule", "body", "head", "negation", "atom", 
		"assign", "math_expr", "compare_expr", "aggregation_expr", "compare_op", 
		"aggregation_op", "math_op", "constant"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'EDB_DECL'", "'IDB_DECL'", "'RULE_DECL'", "'MIN'", "'MAX'", "'SUM'", 
		"'COUNT'", null, null, null, "'_'", "','", "';'", "'.'", "':-'", "'+'", 
		"'-'", "'*'", "'/'", "'!'", "'!='", "'='", "'>='", "'>'", "'<='", "'<'", 
		"'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "TOKEN_EDB", "TOKEN_IDB", "TOKEN_RULE", "TOKEN_MIN", "TOKEN_MAX", 
		"TOKEN_SUM", "TOKEN_COUNT", "TOKEN_ID", "TOKEN_NUMBER", "TOKEN_STRING", 
		"TOKEN_ANY", "TOKEN_COMMA", "TOKEN_SEMICOLON", "TOKEN_DOT", "TOKEN_BODY_HEAD_SEP", 
		"TOKEN_PLUS", "TOKEN_MINUS", "TOKEN_MULT", "TOKEN_DIV", "TOKEN_NOT", "TOKEN_NOT_EQUALS", 
		"TOKEN_EQUALS", "TOKEN_GREATER_EQUAL_THAN", "TOKEN_GREATER_THAN", "TOKEN_LESS_EQUAL_THAN", 
		"TOKEN_LESS_THAN", "TOKEN_LEFT_PAREN", "TOKEN_RIGHT_PAREN", "TOKEN_WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Datalog.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public DatalogParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Datalog_programContext extends ParserRuleContext {
		public List<Datalog_ruleContext> datalog_rule() {
			return getRuleContexts(Datalog_ruleContext.class);
		}
		public Datalog_ruleContext datalog_rule(int i) {
			return getRuleContext(Datalog_ruleContext.class,i);
		}
		public Datalog_programContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_datalog_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterDatalog_program(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitDatalog_program(this);
		}
	}

	public final Datalog_programContext datalog_program() throws RecognitionException {
		Datalog_programContext _localctx = new Datalog_programContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_datalog_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			datalog_rule();
			setState(32);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TOKEN_ID) {
				{
				{
				setState(29);
				datalog_rule();
				}
				}
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Datalog_ruleContext extends ParserRuleContext {
		public HeadContext head() {
			return getRuleContext(HeadContext.class,0);
		}
		public TerminalNode TOKEN_BODY_HEAD_SEP() { return getToken(DatalogParser.TOKEN_BODY_HEAD_SEP, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode TOKEN_DOT() { return getToken(DatalogParser.TOKEN_DOT, 0); }
		public Datalog_ruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_datalog_rule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterDatalog_rule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitDatalog_rule(this);
		}
	}

	public final Datalog_ruleContext datalog_rule() throws RecognitionException {
		Datalog_ruleContext _localctx = new Datalog_ruleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_datalog_rule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			head();
			setState(36);
			match(TOKEN_BODY_HEAD_SEP);
			setState(37);
			body();
			setState(38);
			match(TOKEN_DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public List<AtomContext> atom() {
			return getRuleContexts(AtomContext.class);
		}
		public AtomContext atom(int i) {
			return getRuleContext(AtomContext.class,i);
		}
		public List<TerminalNode> TOKEN_COMMA() { return getTokens(DatalogParser.TOKEN_COMMA); }
		public TerminalNode TOKEN_COMMA(int i) {
			return getToken(DatalogParser.TOKEN_COMMA, i);
		}
		public List<Compare_exprContext> compare_expr() {
			return getRuleContexts(Compare_exprContext.class);
		}
		public Compare_exprContext compare_expr(int i) {
			return getRuleContext(Compare_exprContext.class,i);
		}
		public List<AssignContext> assign() {
			return getRuleContexts(AssignContext.class);
		}
		public AssignContext assign(int i) {
			return getRuleContext(AssignContext.class,i);
		}
		public List<NegationContext> negation() {
			return getRuleContexts(NegationContext.class);
		}
		public NegationContext negation(int i) {
			return getRuleContext(NegationContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitBody(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_body);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(44);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						setState(40);
						atom();
						}
						break;
					case 2:
						{
						setState(41);
						compare_expr();
						}
						break;
					case 3:
						{
						setState(42);
						assign();
						}
						break;
					case 4:
						{
						setState(43);
						negation();
						}
						break;
					}
					setState(46);
					match(TOKEN_COMMA);
					}
					} 
				}
				setState(52);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(53);
			atom();
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TOKEN_COMMA) {
				{
				{
				setState(54);
				match(TOKEN_COMMA);
				setState(59);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
				case 1:
					{
					setState(55);
					atom();
					}
					break;
				case 2:
					{
					setState(56);
					compare_expr();
					}
					break;
				case 3:
					{
					setState(57);
					assign();
					}
					break;
				case 4:
					{
					setState(58);
					negation();
					}
					break;
				}
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HeadContext extends ParserRuleContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public HeadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_head; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterHead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitHead(this);
		}
	}

	public final HeadContext head() throws RecognitionException {
		HeadContext _localctx = new HeadContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_head);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			atom();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegationContext extends ParserRuleContext {
		public TerminalNode TOKEN_NOT() { return getToken(DatalogParser.TOKEN_NOT, 0); }
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NegationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterNegation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitNegation(this);
		}
	}

	public final NegationContext negation() throws RecognitionException {
		NegationContext _localctx = new NegationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_negation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			match(TOKEN_NOT);
			setState(69);
			atom();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public List<TerminalNode> TOKEN_ID() { return getTokens(DatalogParser.TOKEN_ID); }
		public TerminalNode TOKEN_ID(int i) {
			return getToken(DatalogParser.TOKEN_ID, i);
		}
		public TerminalNode TOKEN_LEFT_PAREN() { return getToken(DatalogParser.TOKEN_LEFT_PAREN, 0); }
		public TerminalNode TOKEN_RIGHT_PAREN() { return getToken(DatalogParser.TOKEN_RIGHT_PAREN, 0); }
		public List<Aggregation_exprContext> aggregation_expr() {
			return getRuleContexts(Aggregation_exprContext.class);
		}
		public Aggregation_exprContext aggregation_expr(int i) {
			return getRuleContext(Aggregation_exprContext.class,i);
		}
		public List<TerminalNode> TOKEN_ANY() { return getTokens(DatalogParser.TOKEN_ANY); }
		public TerminalNode TOKEN_ANY(int i) {
			return getToken(DatalogParser.TOKEN_ANY, i);
		}
		public List<ConstantContext> constant() {
			return getRuleContexts(ConstantContext.class);
		}
		public ConstantContext constant(int i) {
			return getRuleContext(ConstantContext.class,i);
		}
		public List<TerminalNode> TOKEN_COMMA() { return getTokens(DatalogParser.TOKEN_COMMA); }
		public TerminalNode TOKEN_COMMA(int i) {
			return getToken(DatalogParser.TOKEN_COMMA, i);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_atom);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			match(TOKEN_ID);
			setState(72);
			match(TOKEN_LEFT_PAREN);
			setState(77);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TOKEN_ID:
				{
				setState(73);
				match(TOKEN_ID);
				}
				break;
			case TOKEN_MIN:
			case TOKEN_MAX:
			case TOKEN_SUM:
			case TOKEN_COUNT:
				{
				setState(74);
				aggregation_expr();
				}
				break;
			case TOKEN_ANY:
				{
				setState(75);
				match(TOKEN_ANY);
				}
				break;
			case TOKEN_NUMBER:
			case TOKEN_STRING:
				{
				setState(76);
				constant();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(88);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TOKEN_COMMA) {
				{
				{
				setState(79);
				match(TOKEN_COMMA);
				setState(84);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TOKEN_ID:
					{
					setState(80);
					match(TOKEN_ID);
					}
					break;
				case TOKEN_MIN:
				case TOKEN_MAX:
				case TOKEN_SUM:
				case TOKEN_COUNT:
					{
					setState(81);
					aggregation_expr();
					}
					break;
				case TOKEN_ANY:
					{
					setState(82);
					match(TOKEN_ANY);
					}
					break;
				case TOKEN_NUMBER:
				case TOKEN_STRING:
					{
					setState(83);
					constant();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(91);
			match(TOKEN_RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public TerminalNode TOKEN_ID() { return getToken(DatalogParser.TOKEN_ID, 0); }
		public TerminalNode TOKEN_EQUALS() { return getToken(DatalogParser.TOKEN_EQUALS, 0); }
		public Math_exprContext math_expr() {
			return getRuleContext(Math_exprContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitAssign(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			match(TOKEN_ID);
			setState(94);
			match(TOKEN_EQUALS);
			setState(95);
			math_expr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Math_exprContext extends ParserRuleContext {
		public List<TerminalNode> TOKEN_ID() { return getTokens(DatalogParser.TOKEN_ID); }
		public TerminalNode TOKEN_ID(int i) {
			return getToken(DatalogParser.TOKEN_ID, i);
		}
		public Math_opContext math_op() {
			return getRuleContext(Math_opContext.class,0);
		}
		public Math_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_math_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterMath_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitMath_expr(this);
		}
	}

	public final Math_exprContext math_expr() throws RecognitionException {
		Math_exprContext _localctx = new Math_exprContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_math_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			match(TOKEN_ID);
			setState(98);
			math_op();
			setState(99);
			match(TOKEN_ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compare_exprContext extends ParserRuleContext {
		public List<TerminalNode> TOKEN_ID() { return getTokens(DatalogParser.TOKEN_ID); }
		public TerminalNode TOKEN_ID(int i) {
			return getToken(DatalogParser.TOKEN_ID, i);
		}
		public Compare_opContext compare_op() {
			return getRuleContext(Compare_opContext.class,0);
		}
		public TerminalNode TOKEN_NUMBER() { return getToken(DatalogParser.TOKEN_NUMBER, 0); }
		public Compare_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compare_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterCompare_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitCompare_expr(this);
		}
	}

	public final Compare_exprContext compare_expr() throws RecognitionException {
		Compare_exprContext _localctx = new Compare_exprContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_compare_expr);
		try {
			setState(113);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(101);
				match(TOKEN_ID);
				setState(102);
				compare_op();
				setState(103);
				match(TOKEN_ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(105);
				match(TOKEN_ID);
				setState(106);
				compare_op();
				setState(107);
				match(TOKEN_NUMBER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(109);
				match(TOKEN_NUMBER);
				setState(110);
				compare_op();
				setState(111);
				match(TOKEN_ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Aggregation_exprContext extends ParserRuleContext {
		public Aggregation_opContext aggregation_op() {
			return getRuleContext(Aggregation_opContext.class,0);
		}
		public TerminalNode TOKEN_LEFT_PAREN() { return getToken(DatalogParser.TOKEN_LEFT_PAREN, 0); }
		public TerminalNode TOKEN_ID() { return getToken(DatalogParser.TOKEN_ID, 0); }
		public TerminalNode TOKEN_RIGHT_PAREN() { return getToken(DatalogParser.TOKEN_RIGHT_PAREN, 0); }
		public Aggregation_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregation_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterAggregation_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitAggregation_expr(this);
		}
	}

	public final Aggregation_exprContext aggregation_expr() throws RecognitionException {
		Aggregation_exprContext _localctx = new Aggregation_exprContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_aggregation_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			aggregation_op();
			setState(116);
			match(TOKEN_LEFT_PAREN);
			setState(117);
			match(TOKEN_ID);
			setState(118);
			match(TOKEN_RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compare_opContext extends ParserRuleContext {
		public TerminalNode TOKEN_NOT_EQUALS() { return getToken(DatalogParser.TOKEN_NOT_EQUALS, 0); }
		public TerminalNode TOKEN_EQUALS() { return getToken(DatalogParser.TOKEN_EQUALS, 0); }
		public TerminalNode TOKEN_GREATER_THAN() { return getToken(DatalogParser.TOKEN_GREATER_THAN, 0); }
		public TerminalNode TOKEN_GREATER_EQUAL_THAN() { return getToken(DatalogParser.TOKEN_GREATER_EQUAL_THAN, 0); }
		public TerminalNode TOKEN_LESS_THAN() { return getToken(DatalogParser.TOKEN_LESS_THAN, 0); }
		public TerminalNode TOKEN_LESS_EQUAL_THAN() { return getToken(DatalogParser.TOKEN_LESS_EQUAL_THAN, 0); }
		public Compare_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compare_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterCompare_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitCompare_op(this);
		}
	}

	public final Compare_opContext compare_op() throws RecognitionException {
		Compare_opContext _localctx = new Compare_opContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_compare_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TOKEN_NOT_EQUALS) | (1L << TOKEN_EQUALS) | (1L << TOKEN_GREATER_EQUAL_THAN) | (1L << TOKEN_GREATER_THAN) | (1L << TOKEN_LESS_EQUAL_THAN) | (1L << TOKEN_LESS_THAN))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Aggregation_opContext extends ParserRuleContext {
		public TerminalNode TOKEN_MIN() { return getToken(DatalogParser.TOKEN_MIN, 0); }
		public TerminalNode TOKEN_MAX() { return getToken(DatalogParser.TOKEN_MAX, 0); }
		public TerminalNode TOKEN_SUM() { return getToken(DatalogParser.TOKEN_SUM, 0); }
		public TerminalNode TOKEN_COUNT() { return getToken(DatalogParser.TOKEN_COUNT, 0); }
		public Aggregation_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregation_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterAggregation_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitAggregation_op(this);
		}
	}

	public final Aggregation_opContext aggregation_op() throws RecognitionException {
		Aggregation_opContext _localctx = new Aggregation_opContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_aggregation_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TOKEN_MIN) | (1L << TOKEN_MAX) | (1L << TOKEN_SUM) | (1L << TOKEN_COUNT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Math_opContext extends ParserRuleContext {
		public TerminalNode TOKEN_PLUS() { return getToken(DatalogParser.TOKEN_PLUS, 0); }
		public TerminalNode TOKEN_MINUS() { return getToken(DatalogParser.TOKEN_MINUS, 0); }
		public TerminalNode TOKEN_MULT() { return getToken(DatalogParser.TOKEN_MULT, 0); }
		public TerminalNode TOKEN_DIV() { return getToken(DatalogParser.TOKEN_DIV, 0); }
		public Math_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_math_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterMath_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitMath_op(this);
		}
	}

	public final Math_opContext math_op() throws RecognitionException {
		Math_opContext _localctx = new Math_opContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_math_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TOKEN_PLUS) | (1L << TOKEN_MINUS) | (1L << TOKEN_MULT) | (1L << TOKEN_DIV))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode TOKEN_NUMBER() { return getToken(DatalogParser.TOKEN_NUMBER, 0); }
		public TerminalNode TOKEN_STRING() { return getToken(DatalogParser.TOKEN_STRING, 0); }
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DatalogListener ) ((DatalogListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_constant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			_la = _input.LA(1);
			if ( !(_la==TOKEN_NUMBER || _la==TOKEN_STRING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\37\u0083\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\7\2!\n\2\f\2\16\2$\13"+
		"\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4/\n\4\3\4\3\4\7\4\63\n\4\f\4"+
		"\16\4\66\13\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4>\n\4\7\4@\n\4\f\4\16\4C\13\4"+
		"\3\5\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7P\n\7\3\7\3\7\3\7\3\7"+
		"\3\7\5\7W\n\7\7\7Y\n\7\f\7\16\7\\\13\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t"+
		"\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\nt\n\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\2\2\20\2"+
		"\4\6\b\n\f\16\20\22\24\26\30\32\34\2\6\3\2\27\34\3\2\6\t\3\2\22\25\3\2"+
		"\13\f\2\u0086\2\36\3\2\2\2\4%\3\2\2\2\6\64\3\2\2\2\bD\3\2\2\2\nF\3\2\2"+
		"\2\fI\3\2\2\2\16_\3\2\2\2\20c\3\2\2\2\22s\3\2\2\2\24u\3\2\2\2\26z\3\2"+
		"\2\2\30|\3\2\2\2\32~\3\2\2\2\34\u0080\3\2\2\2\36\"\5\4\3\2\37!\5\4\3\2"+
		" \37\3\2\2\2!$\3\2\2\2\" \3\2\2\2\"#\3\2\2\2#\3\3\2\2\2$\"\3\2\2\2%&\5"+
		"\b\5\2&\'\7\21\2\2\'(\5\6\4\2()\7\20\2\2)\5\3\2\2\2*/\5\f\7\2+/\5\22\n"+
		"\2,/\5\16\b\2-/\5\n\6\2.*\3\2\2\2.+\3\2\2\2.,\3\2\2\2.-\3\2\2\2/\60\3"+
		"\2\2\2\60\61\7\16\2\2\61\63\3\2\2\2\62.\3\2\2\2\63\66\3\2\2\2\64\62\3"+
		"\2\2\2\64\65\3\2\2\2\65\67\3\2\2\2\66\64\3\2\2\2\67A\5\f\7\28=\7\16\2"+
		"\29>\5\f\7\2:>\5\22\n\2;>\5\16\b\2<>\5\n\6\2=9\3\2\2\2=:\3\2\2\2=;\3\2"+
		"\2\2=<\3\2\2\2>@\3\2\2\2?8\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2B\7\3"+
		"\2\2\2CA\3\2\2\2DE\5\f\7\2E\t\3\2\2\2FG\7\26\2\2GH\5\f\7\2H\13\3\2\2\2"+
		"IJ\7\n\2\2JO\7\35\2\2KP\7\n\2\2LP\5\24\13\2MP\7\r\2\2NP\5\34\17\2OK\3"+
		"\2\2\2OL\3\2\2\2OM\3\2\2\2ON\3\2\2\2PZ\3\2\2\2QV\7\16\2\2RW\7\n\2\2SW"+
		"\5\24\13\2TW\7\r\2\2UW\5\34\17\2VR\3\2\2\2VS\3\2\2\2VT\3\2\2\2VU\3\2\2"+
		"\2WY\3\2\2\2XQ\3\2\2\2Y\\\3\2\2\2ZX\3\2\2\2Z[\3\2\2\2[]\3\2\2\2\\Z\3\2"+
		"\2\2]^\7\36\2\2^\r\3\2\2\2_`\7\n\2\2`a\7\30\2\2ab\5\20\t\2b\17\3\2\2\2"+
		"cd\7\n\2\2de\5\32\16\2ef\7\n\2\2f\21\3\2\2\2gh\7\n\2\2hi\5\26\f\2ij\7"+
		"\n\2\2jt\3\2\2\2kl\7\n\2\2lm\5\26\f\2mn\7\13\2\2nt\3\2\2\2op\7\13\2\2"+
		"pq\5\26\f\2qr\7\n\2\2rt\3\2\2\2sg\3\2\2\2sk\3\2\2\2so\3\2\2\2t\23\3\2"+
		"\2\2uv\5\30\r\2vw\7\35\2\2wx\7\n\2\2xy\7\36\2\2y\25\3\2\2\2z{\t\2\2\2"+
		"{\27\3\2\2\2|}\t\3\2\2}\31\3\2\2\2~\177\t\4\2\2\177\33\3\2\2\2\u0080\u0081"+
		"\t\5\2\2\u0081\35\3\2\2\2\13\".\64=AOVZs";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}